package service.runner;

import static io.restassured.RestAssured.authentication;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.basic;
import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.reset;
import static io.restassured.RestAssured.useRelaxedHTTPSValidation;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import java.nio.charset.Charset;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import infrastructure.root.CommonLibrariesCore;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;
import net.minidev.json.JSONObject;

public class RestAssuredTCs extends CommonLibrariesCore {

	static final String BASE_URI = "http://fakerestapi.azurewebsites.net";
	static final String API_PATH = "/api/Activities/";
	static final int PORT = 641;

	static final String USER_ID = "9b5f49ab-eea9-45f4-9d66-bcf56a531b85";
	static final String USERNAME = "TOOLSQA-Test";
	static final String PASSWORD = "Test@@123";
	static final String BASE_URL = "https://bookstore.toolsqa.com";

	static String token;
	static String bookId;
	static Response response;
	static ResponseSpecification expectedStatusCodeContentType = new ResponseSpecBuilder().expectStatusCode(200)
			.expectContentType(ContentType.JSON.withCharset(Charset.defaultCharset())).build();

	@BeforeTest
	public void setBaseAPI() {

		// Specify the base URI/URL to the RESTful web service
		reset();
		baseURI = BASE_URI;

		given().config(config().logConfig(LogConfig.logConfig().enableLoggingOfRequestAndResponseIfValidationFails())).baseUri(BASE_URI)

				.when().get(BASE_URI + API_PATH)

				.then().assertThat().spec(expectedStatusCodeContentType)

				.and().log().ifValidationFails();
	}

	@Test
	public void request_GET() {

		given().request(API_PATH);
		when().get(API_PATH).then().assertThat().statusCode(200).and().body("Title", hasItem("Activity 1"));
	}

	@Test
	public void request_POST() {

		JSONObject objectParams = new JSONObject();
		objectParams.put("ID", "0");
		objectParams.put("Title", "Singh");
		objectParams.put("DueDate", "2020-07-01T07:30:07.146Z");
		objectParams.put("Completed", true);

		given().contentType(ContentType.JSON.withCharset("utf-8")).body(objectParams).when().post(API_PATH).then().assertThat().statusCode(200).and()
				.body("Title", is("Singh"));
	}

	@Test
	public void request_PUT() {

		// JSONObject is a class that represents a Simple JSON.
		// We can add Key - Value pairs using the put method
		JSONObject objectParams = new JSONObject();
		objectParams.put("ID", "1");
		objectParams.put("Title", "Singh");
		objectParams.put("DueDate", "2020-07-01T07:30:07.146Z");
		objectParams.put("Completed", false);

		given().contentType(ContentType.JSON.withCharset("utf-8")).body(objectParams).when().put(API_PATH + 1).then().assertThat().statusCode(200)
				.and().body("Title", equalTo("Singh"));
	}

	@Test
	public void request_DELETE() {

		given().header("Content-Type", ContentType.JSON);

		// Delete the request and check the response
		given().contentType(ContentType.JSON.withCharset("utf-8")).formParam("ID", "1").when().delete(API_PATH + 1).then().assertThat()
				.statusCode(200);
	}

//	@Test
	public void authenticateUser() {

		given().auth().preemptive().basic(USERNAME, PASSWORD).when().get("https://bookstore.toolsqa.com/Account/v1/GenerateToken").then().assertThat().statusCode(200);

		authentication = basic("", "");
		useRelaxedHTTPSValidation();

		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(USERNAME);
		authScheme.setPassword(USERNAME);
		authentication = authScheme;
	}

//	@Test
	public void authenticateOAuth2() {

		given().auth().oauth2("token").when().get("http://path.to/oath2/secured/api").then().assertThat().statusCode(200);
	}

//	@Test
//	public void generateTokenAuthentication() {
//
//		RequestSpecification request = RestAssured.given();
//
//		request.header("Content-Type", ContentType.JSON.withCharset("utf-8"));
//		JSONObject objectParams = new JSONObject();
//		objectParams.put("userName", USERNAME);
//		objectParams.put("password", PASSWORD);
//		request.body(objectParams.toJSONString());
//		Response response = request.when().post("/Account/v1/GenerateToken");
//		System.out.println("Body Response Of User Token: " + response.getBody().prettyPeek());
//
//		assertEquals(response.getStatusCode(), 200);
//		assertTrue(response.asString().contains("token"));
//		System.out.println("Status Code Response: " + response.getStatusCode());
//
//		token = JsonPath.from(response.asString()).get("token");
//		System.out.println("Token Generated: " + token);
//		// Get Books - No Auth is required for this.
//		response = request.get("/BookStore/v1/Books");
//		List<Map<String, String>> books = JsonPath.from(response.asString()).get("books");
//		System.out.println("Body Response Of Books: " + response.getBody().prettyPeek());
//
//		assertTrue(books.size() > 0);
//		System.out.println("Size Of Books: " + books.size());
//
//		// This bookId will be used in later requests, to add the book with respective
//		bookId = books.get(0).get("isbn");
//
//		// Add a book - with authentication
//		request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");
//		objectParams = new JSONObject();
//		objectParams.put("userID", USER_ID);
//		objectParams.put("collectionOfIsbns", bookId);
//		request.body(objectParams.toJSONString());
//		response = request.post("/BookStore/v1/Books");
//
//		assertEquals(response.getStatusCode(), 201);
//		System.out.println("Status Code Response: " + response.getStatusLine());
//
//		// Delete a book - with authentication
//		request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");
//		objectParams = new JSONObject();
//		objectParams.put("isbn", bookId);
//		objectParams.put("userID", USER_ID);
//		request.body(objectParams.toJSONString());
//		response = request.post("/BookStore/v1/Book");
//
//		assertEquals(response.getStatusCode(), 204);
//		System.out.println("Status Code Response: " + response.getStatusLine());
//
//		// Get User
//		request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");
//		response = request.get("/Account/v1/User/" + USER_ID);
//		System.out.println("Body Response of Users: " + response.getBody().prettyPeek());
//
//		assertEquals(response.getStatusCode(), 200);
//		System.out.println("Status Code Response: " + response.getStatusLine());
//
//		List<Map<String, String>> booksOfUser = JsonPath.from(response.asString()).get("books");
//
//		assertEquals(0, booksOfUser.size());
//		System.out.println("Size of Books Of User: " + booksOfUser.size());
//	}

}
