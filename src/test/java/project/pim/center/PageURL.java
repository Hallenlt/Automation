package project.pim.center;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;
import project.pim.object.HomePage;

public class PageURL extends SeleniumBaseCore {

	public static final String IP_CONFIG = "http://localhost:8080/";
//	public static final String IP_CONFIG = "http://192.168.100.4:6968";
//	public static final String IP_CONFIG = "http://142.93.171.173:6969";

	public static final String PATH_404 = "/pages/errors/error-404";
	public static final String PATH_LOGIN = "/login";
	public static final String PATH_REGISTER = "/register";
	public static final String PATH_DASHBOARD = "/apps/dashboards/analytics";
	public static final String PATH_PRODUCTS = "/apps/e-commerce/products";
	public static final String PATH_NEW_PRODUCT = "/apps/e-commerce/products/new";
	public static final String PATH_CATEGORIES = "/apps/categories";
	public static final String PATH_NEW_PRODUCT_FAMILY = "/apps/e-commerce/product-families/new";
	public static final String PATH_PRODUCT_FAMILIES = "/apps/e-commerce/product-families";
	public static final String PATH_PROFILE = "/pages/profile";
	public static final String PATH_SETTINGS_ATTRIBUTES = "/apps/settings/attributes";
	public static final String PATH_SETTINGS_NEW_ATTRIBUTES = "/apps/settings/attributes/new";
	public static final String PATH_USERS_ALL = "/apps/contacts/all";
	public static final String PATH_USERS_ADMIN = "/apps/contacts/admin";
	public static final String PATH_USERS_SUPER = "/apps/contacts/super-user";
	public static final String PATH_USERS_USER = "/apps/contacts/user";
	public static final String PATH_EXPORT = "/apps/export";
	public static final String PATH_IMPORT = "/apps/import";

	public PageURL(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public PageURL logout() {

		click("//header/div/div[2]/button/span");
		click("//li/div[2]/span");
		HomePage home = new HomePage(driver);
		home.assert_HomeTitleURL();
		return this;
	}

	// Assert title - EN

	public PageURL titleUrlAssert_404Error() {
		assertTitleURL("PIM System", IP_CONFIG + PATH_404);
		return this;
	}

	public PageURL titleUrlAssert_Register() {
		assertTitleURL("Register Page", IP_CONFIG + PATH_REGISTER);
		return this;
	}

	public PageURL titleUrlAssert_Dashboard() {
		assertTitleURL("Dashboard", IP_CONFIG + PATH_DASHBOARD);
		return this;
	}

	public PageURL titleUrlAssert_Products() {
		assertTitleURL("Products", IP_CONFIG + PATH_PRODUCTS);
		return this;
	}

	public PageURL titleUrlAssert_NewProduct() {
		assertTitleURL("Product", IP_CONFIG + PATH_NEW_PRODUCT);
		return this;
	}

	public PageURL titleUrlAssert_Categories() {
		assertTitleURL("CATEGORIES", IP_CONFIG + PATH_CATEGORIES);
		return this;
	}

	public PageURL titleUrlAssert_NewProductFamily() {
		assertTitleURL("Release Type", IP_CONFIG + PATH_NEW_PRODUCT_FAMILY);
		return this;
	}

	public PageURL titleUrlAssert_ProductFamilies() {
		assertTitleURL("product families", IP_CONFIG + PATH_PRODUCT_FAMILIES);
		return this;
	}

	public PageURL titleUrlAssert_Profile() {
		assertTitleURL("My Profile", IP_CONFIG + PATH_PROFILE);
		return this;
	}

	public PageURL titleUrlAssert_SettingsAttributes() {
		assertTitleURL("Attributes", IP_CONFIG + PATH_SETTINGS_ATTRIBUTES);
		return this;
	}

	public PageURL titleUrlAssert_UsersAll() {
		assertTitleURL("All Users", IP_CONFIG + PATH_USERS_ALL);
		return this;
	}

	public PageURL titleUrlAssert_UsersAdmin() {
		assertTitleURL("Admin Users", IP_CONFIG + PATH_USERS_ADMIN);
		return this;
	}

	public PageURL titleUrlAssert_UsersSuper() {
		assertTitleURL("Super Users", IP_CONFIG + PATH_USERS_SUPER);
		return this;
	}

	public PageURL titleUrlAssert_UsersUser() {
		assertTitleURL("Users", IP_CONFIG + PATH_USERS_USER);
		return this;
	}

	public PageURL titleUrlAssert_Import() {
		assertTitleURL("Import", IP_CONFIG + PATH_IMPORT);
		return this;
	}

	public PageURL titleUrlAssert_Export() {
		assertTitleURL("Export", IP_CONFIG + PATH_EXPORT);
		return this;
	}

	// Navigation

	public PageURL navigateTo_Register() {
		clickHref("//a[@href='" + PATH_REGISTER + "']");
		return this;
	}

	public PageURL navigateTo_Login() {
		clickHref("//a[@href='" + PATH_LOGIN + "']");
		return this;
	}

	public PageURL navigateTo_Dashboard() {
		clickHref("//a[@href='" + PATH_DASHBOARD + "']");
		return this;
	}

	public PageURL navigateTo_Products() {
		clickHref("//a[@href='" + PATH_PRODUCTS + "']");
		return this;
	}

	public PageURL navigateTo_NewProduct() {
		clickHref("//a[@href='" + PATH_NEW_PRODUCT + "']");
		return this;
	}

	public PageURL navigateTo_ProductFamilies() {
		clickHref("//a[@href='" + PATH_PRODUCT_FAMILIES + "']");
		return this;
	}

	public PageURL navigateTo_Categories() {
		clickHref("//a[@href='" + PATH_CATEGORIES + "']");
		return this;
	}

	public PageURL navigateTo_UsersAll() {
		clickHref("//a[@href='" + PATH_USERS_ALL + "']");
		return this;
	}

	public PageURL navigateTo_UsersAdmin() {
		clickHref("//a[@href='" + PATH_USERS_ADMIN + "']");
		return this;
	}

	public PageURL navigateTo_UsersSuper() {
		clickHref("//a[@href='" + PATH_USERS_SUPER + "']");
		return this;
	}

	public PageURL navigateTo_UsersUser() {
		clickHref("//a[@href='" + PATH_USERS_USER + "']");
		return this;
	}

	public PageURL expand_AccountTabOption() {
		refreshPage();
		clickHref("//header/div/div[2]/button/span");
		return this;
	}

	public PageURL navigateTo_Profile() {
		expand_AccountTabOption();
		clickHref("//a[@href='" + PATH_PROFILE + "']");
		return this;
	}

	public PageURL navigateTo_BasicInfo() {
		clickHref("//div[2]/div/button");
		return this;
	}

	public PageURL navigateTo_ProductImages() {
		clickHref("//button[2]");
		return this;
	}

	public PageURL navigateTo_Pricing() {
		clickHref("//button[3]");
		return this;
	}

	public PageURL navigateTo_Inventory() {
		clickHref("//button[4]");
		return this;
	}

	public PageURL navigateTo_ProductAttribute() {
		clickHref("//button[5]");
		return this;
	}

	public PageURL navigateTo_FamilyAttributes() {
		clickHref("//div[2]/div/button[2]");
		return this;
	}

	public PageURL navigateTo_FamilyProperties() {
		clickHref("//div[2]/div/button");
		return this;
	}

	public PageURL navigateBack_Products() {
		clickHref("//div/a");
		return this;
	}

	public PageURL navigateBack_ProductFamilies() {
		clickHref("//div/a");
		return this;
	}

	public PageURL expand_SettingsTabOption() {
		clickHref("//span[text()='Settings']");
		return this;
	}

	public PageURL navigateTo_SettingsAttributes() {
		expand_SettingsTabOption();
		clickHref("//a[@href='" + PATH_SETTINGS_ATTRIBUTES + "']");
		return this;
	}

	public PageURL navigateTo_Import() {
		clickHref("//a[@href='" + PATH_IMPORT + "']");
		return this;
	}

	public PageURL navigateTo_Export() {
		clickHref("//a[@href='" + PATH_EXPORT + "']");
		return this;
	}

	public PageURL navigateTo_ProvidersManagement() {
		clickHref("//div[2]/div/button/span");
		return this;
	}

	public PageURL navigateTo_ManualExport() {
		clickHref("//button[2]/span");
		return this;
	}

	public PageURL navigateTo_GeneralInformation() {
		clickHref("//form/div/div/div/button/span");
		return this;
	}

	public PageURL navigateTo_MappingConfiguration() {
		clickHref("//form/div/div/div/button[2]/span");
		return this;
	}

	// Redirection

	public PageURL redirectTo_Register() {
		navigateToURL(IP_CONFIG + PATH_REGISTER);
		return this;
	}

	public PageURL redirectTo_Dashboard() {
		navigateToURL(IP_CONFIG + PATH_DASHBOARD);
		return this;
	}

	public PageURL redirectTo_ProductFamilies() {
		navigateToURL(IP_CONFIG + PATH_PRODUCT_FAMILIES);
		return this;
	}

	public PageURL redirectTo_NewProductFamily() {
		navigateToURL(IP_CONFIG + PATH_NEW_PRODUCT_FAMILY);
		return this;
	}

	public PageURL redirectTo_Products() {
		navigateToURL(IP_CONFIG + PATH_PRODUCTS);
		return this;
	}

	public PageURL redirectTo_NewProduct() {
		navigateToURL(IP_CONFIG + PATH_NEW_PRODUCT);
		return this;
	}

	public PageURL redirectTo_Categories() {
		navigateToURL(IP_CONFIG + PATH_CATEGORIES);
		return this;
	}

	public PageURL redirectTo_UsersAll() {
		navigateToURL(IP_CONFIG + PATH_USERS_ALL);
		return this;
	}

	public PageURL redirectTo_UsersAdmin() {
		navigateToURL(IP_CONFIG + PATH_USERS_ADMIN);
		return this;
	}

	public PageURL redirectTo_UsersSuper() {
		navigateToURL(IP_CONFIG + PATH_USERS_SUPER);
		return this;
	}

	public PageURL redirectTo_UsersUser() {
		navigateToURL(IP_CONFIG + PATH_USERS_USER);
		return this;
	}

	public PageURL redirectTo_SettingsAttributes() {
		navigateToURL(IP_CONFIG + PATH_SETTINGS_ATTRIBUTES);
		return this;
	}

	public PageURL redirectTo_SettingsNewAttributes() {
		navigateToURL(IP_CONFIG + PATH_SETTINGS_NEW_ATTRIBUTES);
		return this;
	}

	public PageURL redirectTo_Profile() {
		navigateToURL(IP_CONFIG + PATH_PROFILE);
		return this;
	}

	public PageURL redirectTo_Import() {
		navigateToURL(IP_CONFIG + PATH_IMPORT);
		return this;
	}

	public PageURL redirectTo_Export() {
		navigateToURL(IP_CONFIG + PATH_EXPORT);
		return this;
	}

	// header section

	// footer section

}
