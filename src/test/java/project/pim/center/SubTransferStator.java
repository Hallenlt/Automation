package project.pim.center;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.BeforeGroups;

import infrastructure.interactor.BehaviorDrivenPresentor;
import infrastructure.reporter.WebEventListener;
import infrastructure.root.CommonAlgorithmCore;
import infrastructure.selenium.ClassifierClientCore;
import infrastructure.selenium.DynamicSortingCore;
import infrastructure.selenium.HttpResponseCore;
import infrastructure.selenium.ObjectMapCore;
import infrastructure.thirdparty.DiffComparisionCore;
import infrastructure.thirdparty.FTPIngestionCore;
import infrastructure.thirdparty.PDFExtractionCore;
import infrastructure.thirdparty.SMSReaderCore;
import infrastructure.thirdparty.SikuliRecognitionCore;
import infrastructure.thread.CentralBaseProcessor;
import infrastructure.validation.BoundaryProcessValidator;
import infrastructure.validation.ProcessValidator;
import project.pim.object.AttributesPage;
import project.pim.object.CategoriesPage;
import project.pim.object.HomePage;
import project.pim.object.ImportExportPage;
import project.pim.object.MyProfilePage;
import project.pim.object.ProductFamiliesPage;
import project.pim.object.ProductsPage;
import project.pim.object.RegisterPage;
import project.pim.object.UsersPage;
import project.research.factory.DatePickerPage;
import project.research.factory.SwappablePage;
import project.research.object.mobile.ApplicationPage;

public class SubTransferStator extends CentralBaseProcessor {

	public EventFiringWebDriver eventDriver;
	public WebEventListener eventListener;
	public ClassifierClientCore classifier;

	public BehaviorDrivenPresentor BDD;
	public ProcessValidator validate;
	public BoundaryProcessValidator validateBoundary;
	public FTPIngestionCore ingestion;
	public SikuliRecognitionCore sikuli;
	public DynamicSortingCore sort;
	public ObjectMapCore objmap;
	public HttpResponseCore response;
	public SMSReaderCore sms;
	public DiffComparisionCore comparision;
	public PDFExtractionCore extraction;
	public CommonAlgorithmCore algorithm;

	// web
	public PageURL url;
	public HomePage home;
	public Login login;
	public RegisterPage register;

	public ProductFamiliesPage families;
	public ProductsPage products;
	public MyProfilePage profile;
	public UsersPage users;
	public AttributesPage attributes;
	public CategoriesPage categories;
	public ImportExportPage imexport;

	public DatePickerPage datepicker;
	public SwappablePage swappable;

	// mobile
	public ApplicationPage application;

	// Initialize the process

	public @BeforeGroups(alwaysRun = true) void initial() {

//		eventDriver = new EventFiringWebDriver(eventDriver);
//		eventDriver.register(eventListener);
		eventListener = new WebEventListener();
		
		ingestion = new FTPIngestionCore(driver());
		BDD = new BehaviorDrivenPresentor(driver());
		validate = new ProcessValidator(driver());
		validateBoundary = new BoundaryProcessValidator(driver());
		sikuli = new SikuliRecognitionCore();
		sort = new DynamicSortingCore(driver());
		objmap = new ObjectMapCore(driver());
		response = new HttpResponseCore(driver());
		sms = new SMSReaderCore();
		comparision = new DiffComparisionCore();
		extraction = new PDFExtractionCore();
		algorithm = new CommonAlgorithmCore();

		// web site
		url = new PageURL(driver());
		home = new HomePage(driver());
		login = new Login(driver());
		register = new RegisterPage(driver());

		families = new ProductFamiliesPage(driver());
		products = new ProductsPage(driver());
		profile = new MyProfilePage(driver());
		users = new UsersPage(driver());
		attributes = new AttributesPage(driver());
		categories = new CategoriesPage(driver());
		imexport = new ImportExportPage(driver());

		datepicker = new DatePickerPage(driver());
		swappable = new SwappablePage(driver());
		

//		// remote
//		classifier = new ClassifierClientCore(remoteDriver());
//		// mobile
//		application = new ApplicationPage(appDriver());
	}

//		try {
//			login.validLogin();
//			Reporter.log("\t [INFO] - Executed | " + "Login Successful." + " | \n", true);
//			Thread.sleep(500);
//		} catch (WebdriverException | InterruptedException e) {
//			// TODO: handle exception
//
//			Reporter.log("\t [INFO] - Executed | " + "Login Failed." + " | \n", true);
//			fail(null, e);
//		}
	
}