package project.pim.testsuite.integration;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class AccountManagementE2E extends SubTransferStator {

	static final String PASSWORD = "012345678";

	@BeforeMethod
	public void beforeMethod() {

		clearWebStorage();
		login.validLogin();
		url.navigateTo_UsersAll();
	}

	@Test(priority = 501)
	public void Add_New_Account_InOrderTo_Verify_MyProfile() {

		users.clickOn_Create();

		String[] componentArray = users.getComponentArray();
		String email = faker.internet().emailAddress();

		users.check_StatusCheckbox();
		selenium.sendKeys(componentArray[0], email);
		users.insert_NewPassword(PASSWORD);
		users.selectList_Role(1);

		String[] integrationArray1 = users.getIntegrationArray();

		validate.getInputData(integrationArray1);
		users.clickOn_Save();

		users.successfulMessageIsDisplayed();
		url.logout();
		login.validLogin(email, PASSWORD);
		url.navigateTo_Profile();

		String[] integrationArray2 = profile.getIntegrationArray();

		validate.getOutputData(integrationArray2);
		validate.assertData();
	}

//	@Test(priority = 502)
//	public void Register_New_Account_InOrderTo_Verify_MyProfile_And_User() {
//
//		clearWebStorage();
//		home.redirectTo_HomePage();
//		url.navigateTo_Register();
//
//		String[] componentArray = register.getComponentArray();
//		String email = faker.internet().emailAddress();
//
//		selenium.sendKeys(componentArray[0], faker.leagueOfLegends().champion());
//		selenium.sendKeys(componentArray[1], faker.number().digits(10));
//		selenium.sendKeys(componentArray[2], email);
//		selenium.sendKeys(componentArray[3], PASSWORD);
//		selenium.sendKeys(componentArray[4], PASSWORD);
//
//		String[] integrationArray1 = register.getIntegrationArray();
//
//		validate.getInputData(integrationArray1);
//		register.submit();
//
//		register.successfulMessageIsDisplayed();
//		home.assert_HomeTitleURL();
//		login.validLogin(email, PASSWORD);
//		url.navigateTo_Profile();
//
//		String[] integrationArray2 = profile.getIntegrationArray();
//
//		validate.getOutputData(integrationArray2);
//		validate.assertData();
//
//		url.navigateTo_UsersAll();
//		users.clickOn_FirstItemInRowTable();
//
//		String[] integrationArray3 = users.getIntegrationArray();
//
//		validate.getOutputData(integrationArray3);
//		validate.assertData();
//	}
//
//	@Test(priority = 503)
//	public void Modify_User_Management_Then_Verify_MyProfile() {
//		
//		users.clickOn_FirstItemInRowTable();
//
//		String[] componentArray = users.getComponentArray();
//		
//		selenium.sendKeys(componentArray[1], faker.number().digits(10));
//
//		String[] integrationArray1 = users.getIntegrationArray();
//
//		validate.getInputData(integrationArray1);
//		users.clickOn_Save();
//
//		users.successfulMessageIsDisplayed();
//		url.navigateTo_Profile();
//
//		String[] integrationArray2 = profile.getIntegrationArray();
//
//		validate.getOutputData(integrationArray2);
//		validate.assertData();
//	}
//
//	@Test(priority = 504)
//	public void Modify_MyProfile_Then_Verify_User_Management() {
//
//		url.redirectTo_Profile();
//
//		String[] componentArray = profile.getComponentArray();
//		
//		selenium.sendKeys(componentArray[2],faker.number().digits(11));
//
//		String[] integrationArray1 = profile.getIntegrationArray();
//
//		validate.getInputData(integrationArray1);
//		profile.clickOn_Save();
//		
//		profile.successMessageIsDisplayed();
//		selenium.refreshPage();
//
//		validate.getOutputData(integrationArray1);
//		validate.assertData();
//
//		url.navigateTo_UsersAll();
//		users.clickOn_FirstItemInRowTable();
//
//		String[] integrationArray2 = users.getIntegrationArray();
//
//		validate.getOutputData(integrationArray2);
//		validate.assertData();
//	}

}
