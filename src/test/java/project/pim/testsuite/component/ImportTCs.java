package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import data.ProviderData;
import project.pim.center.SubTransferStator;

public class ImportTCs extends SubTransferStator {

    @BeforeMethod
    public void beforeMethod() {

	url.navigateTo_Import();
    }

    // [0]ProviderName [1]ConnectionService [2]URL [3]ExecutionInterval
    // [4]StartExecutionAt

    @Test(priority = 601)
    public void Create_New_Import_Provider_Then_Verify_General_Information() {

	imexport.clickOn_ImportCreate("RSS");

	String[] componentArray = imexport.getComponentArray();
	String name = faker.leagueOfLegends().rank();

	selenium.sendKeys(componentArray[0], name);
//		imexport.select_ConnectionService();
	imexport.selectList_ParserService(2);
	selenium.sendKeys(componentArray[2], "http://" + faker.internet().url());
	imexport.selectList_ExecutionInterval();

	validate.getInputData(componentArray);
	url.navigateTo_MappingConfiguration();

	imexport.selectList_MappingTemplate(2);
	imexport.selectList_ProductFamily();
	imexport.clickOn_Save();

	imexport.savedMessageIsDisplayed();
	imexport.assert_HasItemInListCart(name);
	imexport.clickOn_Edit(name);

	validate.getOutputData(componentArray);
	validate.assertData();
    }

    @Test(priority = 602)
    public void Edit_Current_Import_Provider_Then_Verify_General_Information() {

	imexport.clickOn_EditFirstItem();

	String[] componentArray = imexport.getComponentArray();
	String name = faker.leagueOfLegends().rank();

	selenium.sendKeys(componentArray[0], name);
	selenium.sendKeys(componentArray[2], "http://" + faker.internet().url());
	imexport.selectList_ExecutionInterval();

	validate.getInputData(componentArray);
	imexport.clickOn_Update();

	imexport.savedMessageIsDisplayed();
	imexport.assert_HasItemInListCart(name);
	imexport.clickOn_EditFirstItem();

	validate.getOutputData(componentArray);
	validate.assertData();
    }

    @Test(priority = 603)
    public void Create_New_Import_Provider_Then_Verify_Mapping_Configuration() {

	imexport.clickOn_ImportCreate("RSS");

	String[] componentArray = imexport.getComponentArray();
	String name = faker.leagueOfLegends().rank();

	selenium.sendKeys(componentArray[0], name);
//		imexport.select_ConnectionService();
	imexport.selectList_ParserService(2);
	selenium.sendKeys(componentArray[2], "http://" + faker.internet().url());
	imexport.selectList_ExecutionInterval();
	url.navigateTo_MappingConfiguration();

	String[] mappingArray = imexport.getMappingArray();

	imexport.selectList_MappingTemplate(2);
	imexport.selectList_ProductFamily();

	validate.getInputData(mappingArray);
	imexport.clickOn_Save();

	imexport.savedMessageIsDisplayed();
	imexport.assert_HasItemInListCart(name);
	imexport.clickOn_Edit(name);
	url.navigateTo_MappingConfiguration();

	validate.getOutputData(mappingArray);
	validate.assertData();
    }

    @Test(priority = 604)
    public void Edit_Current_Import_Provider_Then_Verify_Mapping_Configuration() {

	imexport.clickOn_EditFirstItem();
	url.navigateTo_MappingConfiguration();

	String[] mappingArray = imexport.getMappingArray();

	imexport.selectList_MappingTemplate(1);
	validate.getInputData(mappingArray);
	imexport.clickOn_Update();

	imexport.savedMessageIsDisplayed();
	imexport.clickOn_EditFirstItem();
	url.navigateTo_MappingConfiguration();

	validate.getOutputData(mappingArray);
	validate.assertData();
    }

    @Test(priority = 605)
    public void Delete_Current_Imimexport_Provider() {

	imexport.clickOn_EditFirstItem();

	String[] componentArray = imexport.getComponentArray();

	String name = selenium.getTextAttribute(componentArray[0]);

	imexport.clickOn_Close();
	imexport.clickOn_Delete(name);

	imexport.deletedMessageIsDisplayed();
	imexport.assert_HasNotItemInListCart(name);
    }

    @Test(priority = 606)
    public void Change_Start_Execution_At_And_Meansure_The_Time() {

	imexport.clickOn_EditFirstItem();

	String[] componentArray = imexport.getComponentArray();

	imexport.pickDatetime_StartExecutionAt();

	validate.getInputData(componentArray);
	imexport.clickOn_Update();

	imexport.savedMessageIsDisplayed();
	imexport.clickOn_EditFirstItem();

	validate.getOutputData(componentArray);
	validate.assertData();
    }

//	@Test(priority = 650, dataProvider = "getDataTypeNotNull", dataProviderClass = ProviderData.class)
    public void Create_New_Import_Provider_With_Different_DataTypes(String value) {

	imexport.clickOn_ImportCreate("RSS");

	String[] componentArray = imexport.getComponentArray();
	String name = value;

	selenium.sendKeys(componentArray[0], name);
//		imexport.select_ConnectionService();
	imexport.selectList_ParserService(2);
	selenium.sendKeys(componentArray[2], "http://" + faker.internet().url());
	imexport.selectList_ExecutionInterval();

	validate.getInputData(componentArray);
	imexport.clickOn_Save();

	imexport.savedMessageIsDisplayed();
	imexport.clickOn_Edit(name);

	validate.getOutputData(componentArray);
	validate.assertData();
    }

}