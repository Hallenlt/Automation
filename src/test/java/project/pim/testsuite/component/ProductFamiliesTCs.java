package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import data.ProviderData;
import project.pim.center.SubTransferStator;

public class ProductFamiliesTCs extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

		url.navigateTo_ProductFamilies();
	}

	// [0]Title [1]Description [2]Code [3]AttributeLabel

	@Test(priority = 401)
	public void Create_New_Product_Family() {

		String[] componentArray = families.getComponentArray();

		families.clickOn_Create();

		selenium.sendKeys(componentArray[0], faker.team().name());
		selenium.sendKeys(componentArray[1], faker.book().title());
		selenium.sendKeysRandomUUID(componentArray[2]);

		url.navigateTo_FamilyAttributes();
		families.selectMultiList_Attributes();

		url.navigateTo_FamilyProperties();
		families.selectList_AttributeLabel();

		validate.getInputData(componentArray);
		families.clickOn_Save();

		families.savedMessageIsDisplayed();
		families.clickOn_FirstItemInRowTable();
		families.statusCheckboxIsActive();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

	@Test(priority = 403)
	public void Edit_Current_Product_Family() {

		String[] componentArray = families.getComponentArray();

		families.clickOn_FirstItemInRowTable();

		selenium.sendKeys(componentArray[0], faker.team().name());
		selenium.sendKeys(componentArray[1], faker.book().title());
		families.check_StatusCheckbox();

		url.navigateTo_FamilyAttributes();
		families.selectMultiList_Attributes();
		url.navigateTo_FamilyProperties();

		validate.getInputData(componentArray);
		families.clickOn_Save();

		families.savedMessageIsDisplayed();
		families.clickOn_FirstItemInRowTable();
		families.statusCheckboxIsInactive();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

	@Test(priority = 404)
	public void Delete_Current_Product_family() {

		String[] componentArray = families.getComponentArray();

		families.clickOn_FirstItemInRowTable();

		String code = selenium.getTextAttribute(componentArray[0]);
		families.clickOn_Delete();

		families.deletedMessageIsDisplayed();
		families.assert_IsNotFirstItemInRowTable(code);
	}

//	@Test(priority = 450, dataProvider = "getDataTypeNotNull", dataProviderClass = ProviderData.class)
	public void Create_New_Properties_Tab_With_Different_DataTypes(String value) {

		String[] componentArray = families.getComponentArray();

		families.clickOn_Create();

		selenium.sendKeys(componentArray[0], value);
		selenium.sendKeys(componentArray[1], value);
		selenium.sendKeys(componentArray[2], value);

		validate.getInputData(componentArray);
		families.clickOn_Save();

		families.savedMessageIsDisplayed();
		families.clickOn_FirstItemInRowTable();
		families.statusCheckboxIsActive();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

}
