package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class VisualResponsiveTCs extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

	}

	@Test(priority = 751)
	public void Compare_Screen_UI_Design_Products_Page() {

		url.redirectTo_Products();
		sikuli.matchScreenImageRecognition("products-pattern.png", "products-finder.png");
	}

	@Test(priority = 752)
	public void Compare_Screen_UI_Design_NewProduct_Page() {

		url.redirectTo_NewProduct();
		sikuli.matchScreenImageRecognition("new-product-pattern.png", "new-product-finder.png");
	}

	@Test(priority = 753)
	public void Compare_Screen_UI_Design_ProductFamilies_Page() {

		url.redirectTo_ProductFamilies();
		sikuli.matchScreenImageRecognition("product-families-pattern.png", "product-families-finder.png");
	}

	@Test(priority = 754)
	public void Compare_Screen_UI_Design_NewProductfamily_Page() {

		url.redirectTo_NewProductFamily();
		sikuli.matchScreenImageRecognition("new-product-family-pattern.png", "new-product-family-finder.png");
	}

	@Test(priority = 755)
	public void Compare_Screen_UI_Design_Categories_Page() {

		url.redirectTo_Categories();
		sikuli.matchScreenImageRecognition("categories-pattern.png", "categories-finder.png");
	}

	@Test(priority = 756)
	public void Compare_Screen_UI_Design_RoleAll_Users_Page() {

		url.redirectTo_UsersAll();
		sikuli.matchScreenImageRecognition("contacts-all-pattern.png", "contacts-all-finder.png");
	}

	@Test(priority = 757)
	public void Compare_Screen_UI_Design_RoleAdmin_Users_Page() {

		url.redirectTo_UsersAdmin();
	}

	@Test(priority = 758)
	public void Compare_Screen_UI_Design_RoleSuper_Users_Page() {

		url.redirectTo_UsersSuper();
	}

	@Test(priority = 759)
	public void Compare_Screen_UI_Design_RoleUser_Users_Page() {

		url.redirectTo_UsersUser();
	}

	@Test(priority = 760)
	public void Compare_Screen_UI_Design_Profile_Page() {

		url.redirectTo_Profile();
		sikuli.matchScreenImageRecognition("profile-pattern.png", "profile-finder.png");
	}

	@Test(priority = 762)
	public void Compare_Screen_UI_Design_Settings_Attributes_Page() {

		url.redirectTo_SettingsAttributes();
		sikuli.matchScreenImageRecognition("settings-attributes-pattern.png", "settings-attributes-finder.png");
	}

	@Test(priority = 763)
	public void Compare_Screen_UI_Design_Import_Page() {

		url.redirectTo_Import();
		sikuli.matchScreenImageRecognition("import-pattern.png", "import-finder.png");
	}

	@Test(priority = 764)
	public void Compare_Screen_UI_Design_Export_Page() {

		url.redirectTo_Export();
		sikuli.matchScreenImageRecognition("export-pattern.png", "export-finder.png");
	}

	@Test(priority = 799)
	public void Compare_Screen_UI_Design_Login_Page() {

		url.logout();
		sikuli.matchScreenImageRecognition("login-pattern.png", "login-finder.png");
	}

	@Test(priority = 800)
	public void Compare_Screen_UI_Design_Register_Page() {

		url.redirectTo_Register();
		sikuli.matchScreenImageRecognition("register-pattern.png", "register-finder.png");
	}

}