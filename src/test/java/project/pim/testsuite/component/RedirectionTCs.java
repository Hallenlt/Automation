package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class RedirectionTCs extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

	}

	@Test(priority = 1)
	public void Redirect_Link_To_Products_Page() {

		url.redirectTo_Products();
		url.titleUrlAssert_Products();
	}

	@Test(priority = 2)
	public void Redirect_Link_To_NewProduct_Page() {

		url.redirectTo_NewProduct();
		url.titleUrlAssert_NewProduct();
	}

	@Test(priority = 3)
	public void Redirect_Link_To_ProductFamilies_Page() {

		url.redirectTo_ProductFamilies();
		url.titleUrlAssert_ProductFamilies();
	}
	
	@Test(priority = 4)
	public void Redirect_Link_To_NewProductfamily_Page() {

		url.redirectTo_NewProductFamily();
		url.titleUrlAssert_NewProductFamily();
	}

	@Test(priority = 5)
	public void Redirect_Link_To_Categories_Page() {

		url.redirectTo_Categories();
		url.titleUrlAssert_Categories();
	}

	@Test(priority = 6)
	public void Redirect_Link_To_UsersAll_Page() {

		url.redirectTo_UsersAll();
		url.titleUrlAssert_UsersAll();
	}

	@Test(priority = 7)
	public void Redirect_Link_To_Users_RoleAdmin_Page() {

		url.redirectTo_UsersAdmin();
		url.titleUrlAssert_UsersAdmin();
	}

	@Test(priority = 8)
	public void Redirect_Link_To_Users_RoleSuper_Page() {

		url.redirectTo_UsersSuper();
		url.titleUrlAssert_UsersSuper();
	}

	@Test(priority = 9)
	public void Redirect_Link_To_Users_RoleUser_Page() {

		url.redirectTo_UsersUser();
		url.titleUrlAssert_UsersUser();
	}

	@Test(priority = 10)
	public void Redirect_Link_To_Profile_Page() {

		url.redirectTo_Profile();
		url.titleUrlAssert_Profile();
	}

	@Test(priority = 12)
	public void Redirect_Link_To_Settings_Attributes_Page() {

		url.redirectTo_SettingsAttributes();
		url.titleUrlAssert_SettingsAttributes();
	}

	@Test(priority = 13)
	public void Redirect_Link_To_Import_Page() {

		url.redirectTo_Import();
		url.titleUrlAssert_Import();
	}

	@Test(priority = 14)
	public void Redirect_Link_To_Export_Page() {

		url.redirectTo_Export();
		url.titleUrlAssert_Export();
	}

	@Test(priority = 50)
	public void Redirect_Link_To_Register_Page() {

		url.logout();
		url.redirectTo_Register();
		url.titleUrlAssert_Register();
	}

}