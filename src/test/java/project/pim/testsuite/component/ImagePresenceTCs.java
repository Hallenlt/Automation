package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import data.ProviderData;
import project.pim.center.SubTransferStator;
import project.pim.center.PageURL;

public class ImagePresenceTCs extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

	}

	@Test(priority = 851, dataProvider = "getDataPathURL", dataProviderClass = ProviderData.class)
	public void Find_Invalid_Broken_Images_Many_Pages(String path) {

		selenium.navigateToURL(PageURL.IP_CONFIG + path);
		selenium.assertInvalidBrokenImage();
	}

	@Test(priority = 899)
	public void Find_Invalid_Broken_Image_Login_Page() {

		url.logout();
		selenium.navigateToURL(PageURL.IP_CONFIG + PageURL.PATH_LOGIN);
		selenium.assertInvalidBrokenImage();
	}

	@Test(priority = 900)
	public void Find_Invalid_Broken_Image_Register_Page() {

		url.redirectTo_Register();
		selenium.navigateToURL(PageURL.IP_CONFIG + PageURL.PATH_REGISTER);
		selenium.assertInvalidBrokenImage();
	}
	
	@DataProvider(name = "getDataPathURL")
	protected static Object[][] getDataPathURL() {

		Object[][] createData = new Object[17][1];

		createData[0][0] = PageURL.PATH_404;
		createData[1][0] = PageURL.PATH_CATEGORIES;
		createData[3][0] = PageURL.PATH_DASHBOARD;
		createData[4][0] = PageURL.PATH_EXPORT;
		createData[5][0] = PageURL.PATH_EXPORT;
		createData[6][0] = PageURL.PATH_IMPORT;
		createData[7][0] = PageURL.PATH_NEW_PRODUCT;
		createData[8][0] = PageURL.PATH_NEW_PRODUCT_FAMILY;
		createData[9][0] = PageURL.PATH_PRODUCTS;
		createData[10][0] = PageURL.PATH_PROFILE;
		createData[11][0] = PageURL.PATH_SETTINGS_ATTRIBUTES;
		createData[12][0] = PageURL.PATH_SETTINGS_NEW_ATTRIBUTES;
		createData[13][0] = PageURL.PATH_USERS_ADMIN;
		createData[14][0] = PageURL.PATH_USERS_SUPER;
		createData[15][0] = PageURL.PATH_USERS_USER;
		createData[16][0] = PageURL.PATH_USERS_ALL;

		return createData;
	}

}