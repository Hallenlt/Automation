package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import data.ProviderData;
import project.pim.center.SubTransferStator;

public class MyProfileTCs extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

		url.navigateTo_Profile();
	}

	// [0]DisplayName [1]Email [2]Phone [3]Address [4]Role

	@Test(priority = 151)
	public void Update_Present_MyProfile() {

		String[] componentArray = profile.getComponentArray();

		selenium.sendKeys(componentArray[0], faker.name().username());
		selenium.sendKeys(componentArray[2], faker.number().digits(10));
		selenium.sendKeys(componentArray[3], faker.address().fullAddress());

		validate.getInputData(componentArray);
		profile.clickOn_Save();

		profile.successMessageIsDisplayed();
		selenium.refreshPage();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

//	@Test(priority = 152, dataProvider = "getDataImage", dataProviderClass = ProviderData.class)
//	public void Upload_Avatar_Image_As_Multiple_TypesFormat(String filename) {
//
//		profile.upload_AvatarImage(filename);
//		profile.clickOn_Save();
//		profile.successMessageIsDisplayed();
//	}
//
//	@Test(priority = 153)
//	public void Upload_As_Changes_Avatar_Image_2_Times() {
//
//		profile.upload_AvatarImage("jpg-image.jpg");
//		profile.upload_AvatarImage("png-image.png");
//		profile.clickOn_Save();
//		profile.successMessageIsDisplayed();
//	}

//	@Test(priority = 200, dataProvider = "getDataTypeNotNull", dataProviderClass = ProviderData.class)
	public void Update_Present_MyProfile_With_Multiple_DataTypes(String value) {

		String[] componentArray = profile.getComponentArray();

		selenium.sendKeys(componentArray[0], value);
		selenium.sendKeys(componentArray[2], faker.number().digits(11));
		selenium.sendKeys(componentArray[3], value);

		validate.getInputData(componentArray);
		profile.clickOn_Save();

		profile.successMessageIsDisplayed();
		selenium.refreshPage();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

}
