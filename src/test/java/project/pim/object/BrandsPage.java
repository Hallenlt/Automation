package project.pim.object;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;
import project.pim.ElementUtils;

public class BrandsPage extends SeleniumBaseCore {

	public BrandsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public BrandsPage input_Search(String value) {
		sendKeys(ElementUtils.SEARCH_BOX_ELEMENT, value);
		click("//div/div/div/button/span/span");
		return this;
	}
}
