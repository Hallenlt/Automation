package project.pim.object;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;
import project.pim.ElementUtils;

public class CategoriesPage extends SeleniumBaseCore {

	public CategoriesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	final String ROWS_ELEMENT = "//*[@class=\"rstcustom__rowTitle\"]";

	// [0]Name [1]Description
	String componentArray[] = { "//*[@id='name']", "//textarea" };

	public String[] getComponentArray() {
		return componentArray.clone();
	}

	public CategoriesPage input_Search(String value) {
		sendKeys(ElementUtils.SEARCH_BOX_ELEMENT, value);
		click(ElementUtils.SEARCH_BUTTON_ELEMENT);
		return this;
	}

	public CategoriesPage assert_HasNotItemInTree(String expected) {
		assertHasNotDataInPageSource(ROWS_ELEMENT, expected);
		return this;
	}

	public CategoriesPage assert_HasItemInTree(String expected) {
		assertHasDataInPageSource(ROWS_ELEMENT, expected);
		return this;
	}

	public CategoriesPage clickOn_EditLastItemInTree(String value) {
		click("(.//*[normalize-space(text()) and normalize-space(.)='" + value + "'])[1]/following::button[1]");
		return this;
	}

	public CategoriesPage clickOn_AddChildOfFirstItemInTree() {
		click("//div[1]/div[2]/div/div[2]/div/div/div[2]/div[1]/button[2]");
		return this;
	}

	public CategoriesPage clickOn_EditFirstItemInTree() {
		click("//div[2]/div/button");
		return this;
	}

	public CategoriesPage statuscheckboxIsInactive() {
		checkboxIsUnselected(ElementUtils.CHECKBOX_ELEMENT);
		return this;
	}

	public CategoriesPage statusCheckboxIsActive() {
		checkboxIsSelected(ElementUtils.CHECKBOX_ELEMENT);
		return this;
	}

	public CategoriesPage check_StatusCheckbox() {
		check("//td/label");
		return this;
	}

	public CategoriesPage clickOn_DeleteAccept() {
		click("//*[text()='OK']");
		return this;
	}

	public CategoriesPage clickOn_Delete() {
		click("//*[text()='delete']");
		clickOn_DeleteAccept();
		return this;
	}

	public CategoriesPage clickOn_Save() {
		click("//*[text()='save']");
		return this;
	}

	public CategoriesPage clickOn_Create() {
		click("//*[text()='ADD NEW']");
		return this;
	}

	public CategoriesPage updatedMessageIsDisplayed() {
		checkIfElementIsDisplayed("//div[contains(.,'Category Updated Successfully')]");
		return this;
	}

	public CategoriesPage deletedMessageIsDisplayed() {
		checkIfElementIsDisplayed("//div[contains(.,'Category Deleted Successfully')]");
		return this;
	}

	public CategoriesPage savedMessageIsDisplayed() {
		checkIfElementIsDisplayed("//div[contains(.,'Category Added Successfully')]");
		return this;
	}

}