package project.pim.object;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;
import project.pim.ElementUtils;

public class ImportExportPage extends SeleniumBaseCore {

	public ImportExportPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	final String CART_ITEM_ELEMENT = "//div[@class='flex flex-row p-2']";

	// [0]ProviderName [1]ConnectionService [2]URL [3]ExecutionInterval
	// [4]StartExecutionAt
	String componentArray[] = { "//*[@name='name']", "//*[@aria-labelledby='connectionConfig']",
			"//*[@name='connectionConfig.url']", "//*[@aria-labelledby='executionInterval']",
			"//*[@name='startExecution']" };

	// [0]MappingTemplate [1]ProductFamily
	String mappingArray[] = { "//*[@aria-labelledby=\"mappingTemplateId\"]",
			"//*[@aria-labelledby=\"productFamilyId\"]" };

	public String[] getComponentArray() {
		return componentArray.clone();
	}

	public String[] getMappingArray() {
		return mappingArray.clone();
	}

	public ImportExportPage selectList_OrganizationProvider() {
		selectDropdownElective("//*[@aria-labelleby=' mui-component-select-service']",
				ElementUtils.DROPDOWN_MENU_ELEMENT, "li");
		return this;
	}

	public ImportExportPage selectList_ProductFamily() {
		selectDropdownElective(mappingArray[1], ElementUtils.DROPDOWN_MENU_ELEMENT, "li");
		return this;
	}

	public ImportExportPage selectList_MappingTemplate(int index) {
		selectDropdownButton(mappingArray[0], "li", index + 1);
		return this;
	}

	public ImportExportPage pickDatetime_StartExecutionAt() {
		return this;
	}

	public ImportExportPage selectList_ExecutionInterval() {
		selectDropdownElective(componentArray[3], ElementUtils.DROPDOWN_MENU_ELEMENT, "li");
		return this;
	}

	public ImportExportPage selectList_ParserService(int index) {
		selectDropdownButton("//*[@aria-labelledby='parserConfig']", "li", index + 1);
		return this;
	}

	public ImportExportPage insert_Account(String username, String password) {
		sendKeys("//*[@name='connectionConfig.username']", username);
		sendKeys("//*[@name='connectionConfig.password']", password);
		return this;
	}

	public ImportExportPage assert_HasNotItemInListCart(String expected) {
		assertHasNotDataInPageSource(CART_ITEM_ELEMENT, expected);
		return this;
	}

	public ImportExportPage assert_HasItemInListCart(String expected) {
		assertHasDataInPageSource(CART_ITEM_ELEMENT, expected);
		return this;
	}

	public ImportExportPage statusCheckboxIsInactive() {
//		checkboxIsUnselected(Utilities.CHECKBOX_ELEMENT);
		return this;
	}

	public ImportExportPage statusCheckboxIsActive() {
//		checkboxIsSelected(Utilities.CHECKBOX_ELEMENT);
		return this;
	}

	public ImportExportPage check_StatusCheckbox() {
		check("//form/div/div[2]/div/div[2]/div[2]/div[1]/div/div[2]/label/span[1]");
		return this;
	}

	public ImportExportPage clickOn_Close() {
		click("//span[text()='Close']");
		return this;
	}

	public ImportExportPage clickOn_Delete(String value) {
		hoverMoveOnFirstItem(CART_ITEM_ELEMENT);
		click("(.//*[normalize-space(text()) and normalize-space(.)='" + value + "'])[1]/following::button[2]");
		return this;
	}

	public ImportExportPage clickOn_Edit(String value) {
		hoverMoveOn("//*[text()='" + value + "']");
		click("(.//*[normalize-space(text()) and normalize-space(.)='" + value + "'])[1]/following::button[1]");
		return this;
	}

	public ImportExportPage clickOn_Update() {
		click("//span[text()='Update']");
		return this;
	}

	public ImportExportPage clickOn_Save() {
		click("//span[text()='Create']");
		return this;
	}

	public ImportExportPage clickOn_EditFirstItem() {
		hoverMoveOnFirstItem(CART_ITEM_ELEMENT);
		clickIgnoreScroll("//button[@aria-label='edit']");
		return this;
	}

	public ImportExportPage clickOn_DeleteFirstItem() {
		hoverMoveOnFirstItem(CART_ITEM_ELEMENT);
		click("//button[1][@aria-label='remove']");
		return this;
	}

	public ImportExportPage clickOn_Export() {
		click("//*[text()='Export']");
		return this;
	}

	public ImportExportPage clickOn_ExportCreate(String service) {
		click("//*[@class='text-16 font-extrabold text-black  whitespace-no-wrap' and text()='" + service + "']");
		return this;
	}

	public ImportExportPage clickOn_ImportCreate(String service) {
		click("//*[@class='text-16 whitespace-no-wrap' and text()='" + service + "']");
		return this;
	}

	public ImportExportPage deletedMessageIsDisplayed() {
//		checkIfElementIsDisplayed("//div[contains(.,'Provider Is Deleted')]");
		return this;
	}

	public ImportExportPage savedMessageIsDisplayed() {
//		checkIfElementIsDisplayed("//div[contains(.,'Provider Saved')]");
		return this;
	}
}