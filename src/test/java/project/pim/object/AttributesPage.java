package project.pim.object;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;
import project.pim.ElementUtils;

public class AttributesPage extends SeleniumBaseCore {

	public AttributesPage(WebDriver driver) {
		super(driver);
	}

	// [0]Code [1]Name [2]Metadata [3]Group
	String componentArray[] = { "//*[@id=\"code\"]", "//*[@id=\"name\"]", null,
			"//*[@id=\"outlined-select-currency\"]" };

	public String[] getComponentArray() {
		return componentArray.clone();
	}

	public AttributesPage input_Search(String value) {
		sendKeys(ElementUtils.SEARCH_BOX_ELEMENT, value);
		return this;
	}

	public AttributesPage assert_IsFirstItemInRowTable(String expected) {
		assertIsFirstItemInDataRowTable(ElementUtils.FIRST_ROW_ELEMENT, ElementUtils.FIRST_CELL_ELEMENT,
				expected);
		return this;
	}

	public AttributesPage assert_IsNotFirstItemInRowTable(String expected) {
		assertIsNotFirstItemInDataRowTable(ElementUtils.FIRST_ROW_ELEMENT,
				ElementUtils.FIRST_CELL_ELEMENT, expected);
		return this;
	}

	public AttributesPage clickOn_FirstItemInRowTable() {
		clickFirstItemInList(ElementUtils.FIRST_ROW_ELEMENT);
		return this;
	}

	public AttributesPage clickOn_MultiSelectType() {
		click("//div[15]/div/div[3]/button");
		return this;
	}

	public AttributesPage selectList_Group() {
		selectDropdownElective(componentArray[3], "//*[@id=\"menu-productStatus\"]/div[3]", "li");
		return this;
	}

	public AttributesPage statuscheckboxIsInactive() {
		checkboxIsUnselected(ElementUtils.CHECKBOX_ELEMENT);
		return this;
	}

	public AttributesPage statusCheckboxIsActive() {
		checkboxIsSelected(ElementUtils.CHECKBOX_ELEMENT);
		return this;
	}

	public AttributesPage check_StatusCheckbox() {
		check("//div[2]/div[2]/div[1]/div/label");
		return this;
	}

	public AttributesPage clickOn_Update() {
		click("//span[text()='Update']");
		return this;
	}

	public AttributesPage clickOn_Delete() {
		click("//*[text()='Delete']");
		return this;
	}

	public AttributesPage clickOn_Save() {
		click("//*[text()='save']");
		return this;
	}

	public AttributesPage clickOn_Create() {
		click("//*[text()='add new']");
		return this;
	}

	public AttributesPage updatedMessageIsDisplayed() {
		checkIfElementIsDisplayed("//div[contains(.,'Attribute Updated Successfully')]");
		return this;
	}

	public AttributesPage deletedMessageIsDisplayed() {
		checkIfElementIsDisplayed("//div[contains(.,'Attribute Deleted Successfully')]");
		return this;
	}

	public AttributesPage savedMessageIsDisplayed() {
		checkIfElementIsDisplayed("//div[contains(.,'Attribute Added Successfully')]");
		return this;
	}

}