package project.pim.object;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;
import project.pim.ElementUtils;

public class UsersPage extends SeleniumBaseCore {

	public UsersPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public static final String ROWS_ELEMENT = "//*[@role=\"rowgroup\"]";
	public static final String NEXTPAGE_BUTTON_ELEMENT = "//div[3]/button";
	public static final String SORT_BUTTON_ELEMENT = "//div[2]/div[2]/div[2]/div/div/div/div/div/div[3]";

	// [0]Email [1]Phone [2]First Name [3]Last Name [4]Gender [5]Role
	// [6]Country [7]City [8]Address
	public String componentArray[] = { "//*[@id=\"email\"]", "//*[@name=\"phone\"]", "//*[@id=\"firstName\"]", "//*[@id=\"lastName\"]",
			"//*[@aria-labelledby=\" mui-component-select-gender\"]", "//*[@aria-labelledby=\" mui-component-select-roleProfileModel\"]",
			"//*[@aria-labelledby=\" mui-component-select-country\"]", "//*[@id=\"city\"]", "//*[@id=\"address\"]" };

	String integrationArray[] = { componentArray[0], componentArray[1] };

	public String[] getIntegrationArray() {
		return integrationArray.clone();
	}

	public String[] getComponentArray() {
		return componentArray.clone();
	}

	public UsersPage input_Search(String value) {
		sendKeys(ElementUtils.SEARCH_BOX_ELEMENT, value);
		click(ElementUtils.SEARCH_BUTTON_ELEMENT);
		return this;
	}

	public UsersPage clickOn_ChangePassword() {
		click("//*/text()[normalize-space(.)='Change Password']/parent::*");
		return this;
	}

	public UsersPage insert_ChangePassword(String password, String confirm) {
		return this;
	}

	public UsersPage insert_NewPassword(String value) {
		sendKeys("//*[@id=\"password\"]", value);
		return this;
	}

	public UsersPage assert_IsFirstItemInRowTable(String expected) {
		assertIsFirstItemInCustomTable("//*[@class=\"rt-table\"]", "//*[@class=\"rt-tbody\"]", ROWS_ELEMENT, expected);
		return this;
	}

	public UsersPage clickOn_FirstItemInRowTable() {
		click(ROWS_ELEMENT);
		return this;
	}

	public UsersPage selectList_Country() {
		selectDropdownElective(componentArray[6], "//*[@id=\"menu-country\"]/div[3]", "li");
		return this;
	}

	public UsersPage selectList_Role(int index) {
		selectDropdownButton(componentArray[5], "li", index + 1);
		return this;
	}

	public UsersPage selectList_Gender(int index) {
		selectDropdownButton(componentArray[4], "li", index + 1);
		return this;
	}

	public UsersPage statusCheckboxIsInactive() {
		checkboxIsUnselected(ElementUtils.CHECKBOX_ELEMENT);
		return this;
	}

	public UsersPage statusCheckboxIsActive() {
		checkboxIsSelected(ElementUtils.CHECKBOX_ELEMENT);
		return this;
	}

	public UsersPage check_StatusCheckbox() {
		check("//div[2]/label/span[1]");
		return this;
	}

	public UsersPage clickOn_Delete() {
		click("//*[text()='Delete']");
		return this;
	}

	public UsersPage clickOn_Update() {
		click("//*[text()='Save']");
		return this;
	}

	public UsersPage clickOn_Save() {
		click("//*[text()='Create']");
		return this;
	}

	public UsersPage clickOn_Create() {
		click("//*[text()='person_add']");
		return this;
	}

	public UsersPage successfulMessageIsDisplayed() {
		checkIfElementIsDisplayed("//div[contains(.,'Process Successful !')]");
		return this;
	}

	public UsersPage failedMessageIsDisplayed() {
		checkIfElementIsDisplayed("");
		return this;
	}

}