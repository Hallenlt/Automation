package project.pim.object;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;

public class RegisterPage extends SeleniumBaseCore {

    public RegisterPage(WebDriver driver) {
	super(driver);
	// TODO Auto-generated constructor stub
    }

    // [0]Display Name [1]Phone [2]Email [3]Password [4]Confirm Password
    public String componentArray[] = { "//input[@name='displayName']", "//input[@name='phone']", "//input[@name='email']",
	    "//input[@name='password']", "//input[@name='password-confirm']" };

    String integrationArray[] = { componentArray[2], componentArray[1] };

    public String[] getIntegrationArray() {
	return integrationArray.clone();
    }

    public String[] getComponentArray() {
	return componentArray.clone();
    }

    public RegisterPage submit() {
	click("//*[text()='Register']");
	return this;
    }

    public RegisterPage successfulMessageIsDisplayed() {
	checkIfElementIsDisplayed("//div[contains(.,'Register Successful !!')]");
	return this;
    }

    public RegisterPage failedMessageIsDisplayed() {
	checkIfElementIsDisplayed("");
	return this;
    }

    public RegisterPage captcha(String locator, String value) {
	sendKeys(locator, value);
	return this;
    }

}