package project.pim;

public class ElementUtils {

	public static final String DROPDOWN_MENU_ELEMENT = "//div[@class=\"MuiPopover-root\"]/div[3]/ul";

	public static final String CHECKBOX_ELEMENT = "//*[@id='isActive']";

	public static final String FIRST_ROW_ELEMENT = "//table/tbody/tr[1]";

	public static final String FIRST_CELL_ELEMENT = "//table/tbody/tr[1]/th[1]";

	public static final String SEARCH_BOX_ELEMENT = "//input[@aria-label='Search']";

	public static final String SEARCH_BUTTON_ELEMENT = "//span[text()='search']";

	public static final String NEXTPAGE_BUTTON_ELEMENT = "//table/thead/tr/th[1]/span";

	public static final String FIRST_COLUMN_ELEMENT = "//table/tbody/tr/th[1]";

	public static final String SORT_BUTTON_ELEMENT = "//th[3]/span";

}