package project.belgapress.center;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;

import infrastructure.interactor.BehaviorDrivenPresentor;
import infrastructure.selenium.DatetimePickerCore;
import infrastructure.selenium.DynamicSortingCore;
import infrastructure.selenium.SeleniumBaseCore;
import infrastructure.thread.CentralBaseProcessor;
import infrastructure.validation.BoundaryProcessValidator;
import infrastructure.validation.ProcessValidator;
import project.belgapress.object.website.PublisherTextUsagePage;

public class SubTransferStator extends CentralBaseProcessor {

	final String INPUT_RECORDER_PATH = ".\\evidences\\filename.mov";
	File recordedFile = new File(INPUT_RECORDER_PATH);

	public BehaviorDrivenPresentor BDD;
	public ProcessValidator validate;
	public BoundaryProcessValidator validatebound;
	public DynamicSortingCore sorting;
	public DatetimePickerCore datetimePicker;

	// web
	public PageURL url;

	public PublisherTextUsagePage publisher;

	// Initialize the process

	public @BeforeGroups(alwaysRun = true) void initial() {

		if (driver() != null) {

			selenium = new SeleniumBaseCore(driver());
			BDD = new BehaviorDrivenPresentor(driver());
			validate = new ProcessValidator(driver());
			validatebound = new BoundaryProcessValidator(driver());
			sorting = new DynamicSortingCore(driver());
			datetimePicker = new DatetimePickerCore(driver());

			// web site
			url = new PageURL(driver());

			publisher = new PublisherTextUsagePage(driver());
		}

	}

}