package project.belgapress.object.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties("unknown")
public class POJOModel {

	// Serialization of POJO into a JSON Request Body Object

	// De-Serialization of API Response to a POJO

	List<EmployeeModel> employee;
	@JsonIgnore
	String unknown;
	Map<String, String> childKeys;
	List<Integer> codes;
	boolean isRight;
	String name;
	String batch;
	int rollNumber;

	public POJOModel(String name, String batch, int rollNumber, boolean isRight, List<Integer> codes, Map<String, String> childKey,
			List<EmployeeModel> employee) {
		this.name = name;
		this.batch = batch;
		this.rollNumber = rollNumber;
		this.isRight = isRight;
		this.codes = codes;
		this.childKeys = childKey;
	}

	public List<EmployeeModel> getEmployee() {
		return employee;
	}

	public void setEmployee(List<EmployeeModel> employee) {
		this.employee = employee;
	}

	public Map<String, String> getChildKeys() {
		return childKeys;
	}

	public void setChildKeys(Map<String, String> childKeys) {
		this.childKeys = childKeys;
	}

	public List<Integer> getCodes() {
		return codes;
	}

	public void setCodes(List<Integer> codes) {
		this.codes = codes;
	}

	public boolean getIsRight() {
		return isRight;
	}

	public void setIsRight(boolean isRight) {
		this.isRight = isRight;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Integer getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(Integer rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String output() {
		return "The Student name is: " + this.name + " | studies in Batch: " + this.batch + " | has rollNumber: " + this.rollNumber + " | then Codes: "
				+ this.codes + " | and Childkeys: " + this.childKeys + " | finally Employee: " + this.employee;
	}

}
