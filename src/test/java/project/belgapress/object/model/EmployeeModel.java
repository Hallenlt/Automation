package project.belgapress.object.model;

public class EmployeeModel {

	String fullName;
	double salary;
	
	public EmployeeModel(String fullName, double salary) {
		this.fullName = fullName;
		this.salary = salary;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

}