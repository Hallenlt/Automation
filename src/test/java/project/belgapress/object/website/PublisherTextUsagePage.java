package project.belgapress.object.website;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.DatetimePickerCore;
import infrastructure.selenium.SeleniumBaseCore;
import project.belgapress.object.ElementUtils;

public class PublisherTextUsagePage extends SeleniumBaseCore {

	public PublisherTextUsagePage(WebDriver driver) {
		super(driver);
		// g
	}

	// [0] results
	String componentArray[] = { "//form/div[2]/span[1]" };

	public String[] getComponentArray() {
		return componentArray.clone();
	}

	public PublisherTextUsagePage hide_BasicAnalysis() {
		click(ElementUtils.BASIC_ANALYSIS_BUTTON_ELEMENT);
		checkIfElementIsUndisplayed(ElementUtils.SHOW_TABLE_ELEMENT);
		return this;
	}

	public PublisherTextUsagePage show_BasicAnalysis() {
		click(ElementUtils.BASIC_ANALYSIS_BUTTON_ELEMENT);
		checkIfElementIsUndisplayed(ElementUtils.HIDE_TABLE_ELEMENT);
		return this;
	}

	public PublisherTextUsagePage noResultsMessageIsDisplayed() {
		checkIfElementIsDisplayed(ElementUtils.NO_RESULTS_TABLE_ELEMENT);
		return this;
	}

	public PublisherTextUsagePage clickOn_Search() {
		click(ElementUtils.SEARCH_BUTTON_ELEMENT);
		return this;
	}

	public PublisherTextUsagePage select_Newsbrand(String newsbrand) {
		click("//*[@id=\"root\"]/div[2]/div/form/div[1]/div/div[4]/div[1]/div[1]");
		check("//*[text()='" + newsbrand + "']");
		return this;
	}

	public PublisherTextUsagePage select_Publisher(String publisher) {
		click("//*[@id=\"root\"]/div[2]/div/form/div[1]/div/div[3]/div[1]/div[1]");
		check("//*[text()='" + publisher + "']");
		return this;
	}

	public PublisherTextUsagePage select_Language(String language) {
		click("//div[@id='root']/div[2]/div/form/div/div/div[2]/div/div");
		check("//*[text()='" + language + "']");
		return this;
	}

	public PublisherTextUsagePage select_DatetimePicker(String value) {
		click(ElementUtils.DATE_PICKER_ELEMENT);

		String fullDate = value;
		String dateArray[] = fullDate.split("/");
		String year = dateArray[2];
		String month = dateArray[1];
		String day = dateArray[0];

		datetimePicker = new DatetimePickerCore(driver);

		datetimePicker
				.selectYear("//span[@class='react-datepicker__year-read-view--selected-year']", "//div[@class='react-datepicker__year-dropdown']",
						"//div[text()='" + year + "']")

				.selectMonth("//span[@class='react-datepicker__month-read-view--selected-month'", "//div[@class='react-datepicker__month-dropdown']",
						"//span[text()='" + month + "']")

				.selectDay("//div[@class='react-datepicker__month']", "//span[text()='" + day + "']");
		return this;
	}

}