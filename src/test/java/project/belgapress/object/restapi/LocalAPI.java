package project.belgapress.object.restapi;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.reset;

import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;

public class LocalAPI {

	static ResponseSpecification expectedResponse = new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();

	static final int PORT = 8107;

	static final String BASE_URI = "http://192.168.100.4:8107/belga-ezyinsight";

	// Test Server

	static final String DOCS_API = "/v2/api-docs";

	public static final String BELGA_SOURCE_API = "/source";

	public static final String BELGA_USAGE_API = "/belga";

	public static final String LANGUAGE_API = "/languages/all";

	public static final String NEWS_OBJECT_API = "/newsobject";

	public static final String PUBLISHER_USAGE_API = "/publisher";

	public static final String STATISTIC_API = "/statistic";

	public static void configBaseURI() {

		reset();
		baseURI = StagingAPI.BASE_URI;

		given().config(config().logConfig(LogConfig.logConfig().enableLoggingOfRequestAndResponseIfValidationFails())).baseUri(BASE_URI)

				.when().get(BASE_URI + DOCS_API)

				.then().assertThat().spec(expectedResponse)

				.and().log().ifValidationFails();
	}

}