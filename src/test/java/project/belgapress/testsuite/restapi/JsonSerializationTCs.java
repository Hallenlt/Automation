package project.belgapress.testsuite.restapi;

import static io.restassured.RestAssured.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.restassured.RestAssured;
import project.belgapress.object.model.EmployeeModel;
import project.belgapress.object.model.POJOModel;
import project.belgapress.object.restapi.StagingAPI;

public class JsonSerializationTCs {

	String jsonData;

	@BeforeMethod
	public void setBaseURI() {

		StagingAPI.configBaseURI();
	}

	@Test
	public void verifySerialization() throws JsonProcessingException {

		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode parentArray = objectMapper.createArrayNode();

		ObjectNode objectNode = objectMapper.createObjectNode();
		objectNode.put("1", "dtgde");
		objectNode.put("2", true);
		objectNode.put("2", 923.45);

		ObjectNode datetime = objectMapper.createObjectNode();
		datetime.put("checkin", "2020-07-01");
		datetime.put("checkout", "2020-07-01");

		objectNode.set("datetime", datetime);
		parentArray.addAll(Arrays.asList(objectNode, objectNode));

		given().contentType("application/json").body(parentArray).when().post("").then().extract().response().toString()
				.contains("POJOModel added successfully.");

		String jsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(parentArray);
		System.out.println(jsonArray);
		System.out.println("Size of array: " + parentArray.size());
	}

	@Test
	public void testSerialization() throws JsonProcessingException {

		List<Integer> codes = new ArrayList<Integer>();
		codes.add(34235);
		codes.add(35673);

		Map<String, String> childKeys = new HashMap<>();
		childKeys.put("1", "child first");
		childKeys.put("2", "child second");

		EmployeeModel employee = new EmployeeModel("DPTL", 135.7);
		EmployeeModel employee2 = new EmployeeModel("DPTL", 135.7);
		List<EmployeeModel> allEmployees = new ArrayList<EmployeeModel>();
		allEmployees.add(employee);
		allEmployees.add(employee2);

		POJOModel pojoModel = new POJOModel("Isha Durani", "A", 101, true, codes, childKeys, allEmployees);
		POJOModel pojoModel2 = new POJOModel("Isha Durani", "A", 101, true, codes, childKeys, allEmployees);

		List<POJOModel> allPojoModels = new ArrayList<POJOModel>();
		allPojoModels.add(pojoModel);
		allPojoModels.add(pojoModel2);

		given().contentType("application/json").body(allPojoModels).when().post("").then().extract().response().toString()
				.contains("POJOModel added successfully.");

		ObjectMapper objectMapper = new ObjectMapper();
		jsonData = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(allPojoModels);
		System.out.println(jsonData);
	}

	@Test
	public void testDeSerialization() throws JsonMappingException, JsonProcessingException {

		POJOModel pojoModel = RestAssured.get("").as(POJOModel.class);
		System.out.println(pojoModel.output());

		ObjectMapper objectMapper = new ObjectMapper();
		List<POJOModel> allPojoModels = objectMapper.readValue(jsonData, new TypeReference<List<POJOModel>>() {
		});

		jsonData = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(allPojoModels);
		System.out.println(jsonData);
	}

}