package project.belgapress.testsuite.restapi;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.belgapress.object.restapi.StagingAPI;

public class JsonSchemaTCs {

	@BeforeMethod
	public void setBaseURI() {

		StagingAPI.configBaseURI();
		
		StagingAPI.configSchemaValidator();
	}

	@Test
	public void testJsonSchema() {

		given().when().get("").then().assertThat().body(matchesJsonSchemaInClasspath("json_schema.json"));
	}

}