package project.belgapress.testsuite.restapi.staging;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import project.belgapress.center.SubTransferStator;
import project.belgapress.object.restapi.LocalAPI;
import project.belgapress.object.restapi.StagingAPI;

public class ContentControllerTCs extends SubTransferStator {

//    .body("size", greaterThan(0))
//    .body("$.findAll (it.type.name = LPM}.status.id ", hasItem(statusId))
//    .body("$.findAll (it.type.name = LPM}.status.source", hasItem(sourceId))
//    .body("$.findAll (it.type.name = LPM}.status.price", hasItem(price));

//    root("find {it.type.name == '%s'}.status").
//    body("id", withArgs("LPM"), is(1)).
//    body("price", withArgs("LPM"), is(1.20f)).
//    body("source", withArgs("LPM"), is(172)).
//    body("id", withArgs("ST"), is(10));

	@BeforeTest
	public void setBaseAPI() {

		LocalAPI.configBaseURI();
	}

	// Content_BySource_Derivatives

	@Test
	public void GET_Language_FR_Content_BySource_Derivatives() {

		given().request(StagingAPI.CONTENT_DERIVATIVES_API);
		when().get(StagingAPI.CONTENT_DERIVATIVES_API + "sources=belga&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.body("data.language", anyOf(hasItem("FR"), hasItem("NL")));
	}

	@Test
	public void GET_Language_Notnull_Content_BySource_Derivatives() {

		given().request(StagingAPI.CONTENT_DERIVATIVES_API);
		when().get(StagingAPI.CONTENT_DERIVATIVES_API + "sources=belga&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.body("data.language", notNullValue());
	}

	@Test
	public void GET_Default_Offset_And_Count_Is_150_Content_BySource_Derivatives() {

		given().request(StagingAPI.CONTENT_DERIVATIVES_API);
		when().get(StagingAPI.CONTENT_DERIVATIVES_API + "sources=belga&from=2020-07-01&to=2020-08-31&count=150").then().assertThat().statusCode(200)
				.body("data.newsObjectId", hasSize(150));
	}

	@Test
	public void GET_Default_Offset_And_Count_Is_100_Content_BySource_Derivatives() {

		given().request(StagingAPI.CONTENT_DERIVATIVES_API);
		when().get(StagingAPI.CONTENT_DERIVATIVES_API + "sources=belga&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.body("data.newsObjectId", hasSize(100));
	}

	@Test
	public void GET_Default_Offset_And_Count_Is_50_Content_BySource_Derivatives() {

		given().request(StagingAPI.CONTENT_DERIVATIVES_API);
		when().get(StagingAPI.CONTENT_DERIVATIVES_API + "sources=belga&from=2020-07-01&to=2020-08-31&count=50").then().assertThat().statusCode(200)
				.body("data.newsObjectId", hasSize(50));
	}

	@Test
	public void GET_Default_Offset_And_Count_HasSize_1_Content_BySource_Derivatives() {

		given().request(StagingAPI.CONTENT_DERIVATIVES_API);
		when().get(StagingAPI.CONTENT_DERIVATIVES_API + "sources=belga&from=2020-07-01&to=2020-08-31&offset=1&count=1").then().assertThat().statusCode(200)
				.body("data.newsObjectId", hasSize(1));
	}

	@Test
	public void GET_Default_Offset_And_Count_HasSize_5_Content_BySource_Derivatives() {

		given().request(StagingAPI.CONTENT_DERIVATIVES_API);
		when().get(StagingAPI.CONTENT_DERIVATIVES_API + "sources=belga&from=2020-07-01&to=2020-08-31").then().assertThat().statusCode(200)
				.body("data.newsObjectId", hasSize(5));
	}

	// Content_Originals

	@Test
	public void GET_Multiple_Sources_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&sources=RTL info&from=2020-07-01&to=2020-08-30&count=10").then().assertThat()
				.statusCode(200).and().body("source", hasItems("La Libre Belgique", "RTL info"));
	}

	@Test
	public void GET_StoryTags_HasSize_GreaterThan_1_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("storyTags", hasItem(hasSize(greaterThan(1))));
	}

	@Test
	public void GET_StoryTags_HasArray_Empty_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("storyTags", hasItem(empty()));
	}

	@Test
	public void GET_RelatedArticles_HasArray_Empty_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("relatedArticles", hasItem(empty()));
	}

	@Test
	public void GET_RelatedArticles_HasArray_GreaterThan_2_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("relatedArticles", hasSize(greaterThan(2)));
	}

	@Test
	public void GET_Desk_Has_Nullvalue_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("desk", hasItem("No desk found"));
	}

	@Test
	public void GET_PublishDate_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("publishDate", notNullValue());
	}

	@Test
	public void GET_Title_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("title", notNullValue());
	}

	@Test
	public void GET_NewsObjectId_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("newsObjectId", notNullValue());
	}

	@Test
	public void GET_RelatedArticles_PublishDate_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=RTL info&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200).and()
				.body("relatedArticles.publishDate", notNullValue());
	}

	@Test
	public void GET_RelatedArticles_NewsObjectId_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=RTL info&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200).and()
				.body("relatedArticles.newsObjectId", notNullValue());
	}

	@Test
	public void GET_RelatedArticles_Source_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=RTL info&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200).and()
				.body("relatedArticles.source", notNullValue());
	}

	@Test
	public void GET_RelatedArticles_title_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=RTL info&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200).and()
				.body("relatedArticles.title", notNullValue());
	}

	@Test
	public void GET_RelatedArticles_Section_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=RTL info&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200).and()
				.body("relatedArticles.section", notNullValue());
	}

	@Test
	public void GET_RelatedArticles_Score_Maximum_Point_LessThan_1_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=RTL info&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200).and()
				.body("relatedArticles.score", lessThan(1));
	}

	@Test
	public void GET_RelatedArticles_Score_Array_Empty_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=RTL info&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200).and()
				.body("relatedArticles.score", hasItem(empty()));
	}

	@Test
	public void GET_Source_Notnull_hasItem_RTLinfo_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=RTL info&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200).and()
				.body("source", allOf(notNullValue(), hasItem("RTL info")));
	}

	@Test
	public void GET_MediumTypeGroup_HasItem_Online_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("mediumTypeGroup", hasItem("ONLINE"));
	}

	@Test
	public void GET_mediumTypeGroup_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("mediumTypeGroup", notNullValue());
	}

	@Test
	public void GET_Language_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("language", notNullValue());
	}

	@Test
	public void GET_Category_Has_Nullvalue_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("category", hasItem("No category found"));
	}

	@Test
	public void GET_Publisher_Has_Nullvalue_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("publisher", hasItem(nullValue()));
	}

	@Test()
	public void GET_Storytags_Notnull_Content_Originals() {

		given().request(StagingAPI.CONTENT_ORIGINALS_API);
		when().get(StagingAPI.CONTENT_ORIGINALS_API + "sources=La Libre Belgique&from=2020-07-01&to=2020-08-31&count=100").then().assertThat().statusCode(200)
				.and().body("storyTags", notNullValue());
	}

}