package project.belgapress.testsuite.website.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.belgapress.center.SubTransferStator;

public class PublisherTextUsageTCs extends SubTransferStator {

	@BeforeMethod
	public void before() {

		url.navigateTo_PublisherTextUsage();
	}

	@Test
	public void Search_By_Datetime() {

		publisher.select_DatetimePicker("2/February/2021");
	}

//	@Test
//	public void Search_By_Publisher_And_Newsbrand() {
//
//		belga.select_Publisher("RTL").select_Newsbrand("RTL info").clickOn_Search();
//	}
//
//	@Test
//	public void Search_By_Languages() {
//
//		belga.select_Language("English").clickOn_Search();
//	}

}