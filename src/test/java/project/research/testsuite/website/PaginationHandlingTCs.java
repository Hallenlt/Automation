package project.research.testsuite.website;

import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class PaginationHandlingTCs extends SubTransferStator {

	@Test
	public void handlePagination() {

		url.navigateTo_ProductFamilies();

		selenium.findItemBySpecificInDataTable("//button[@aria-label='Next Page']", "//table/tbody/tr/th[1]", "Manufacturing Group");

		selenium.findItemByAllInDataTable("//button[@aria-label='Next Page']", "dfrtgfhrtfh");
	}

}