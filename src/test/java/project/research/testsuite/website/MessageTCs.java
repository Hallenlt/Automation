package project.research.testsuite.website;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import javax.jms.Message;

import org.testng.annotations.Test;

import infrastructure.thirdparty.JavaxEmailCore;
import project.pim.center.SubTransferStator;

public class MessageTCs extends SubTransferStator {

	@Test
	public void validateEmail() {

		String email = "doanphungtriloc02@gmail.com";
		String password = "192.168.1.4";
		String server = "imap.gmail.com";

		try {
			JavaxEmailCore emails = new JavaxEmailCore(email, password, server, JavaxEmailCore.EmailFolder.INBOX);
			Message message1 = (Message) emails.getMessagesBySubject("An Education for a High-Growth Computer Science Career", false, 5)[0];

			assertTrue(emails.isTextInMessage((javax.mail.Message) message1, "Ranked in the Top 10 for Online Graduate Engineering Programs"));
			String link = emails.getUrlsFromMessage((javax.mail.Message) message1, "Learn more").get(0);
			System.out.println(link);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Test
	public void messageSMS() {

		String fromPhoneNumber = "+84793940121";
		String phoneNumber = sms.getNumber();
		String code = "";
		code = sms.getMessage(phoneNumber, fromPhoneNumber);
	}

}
