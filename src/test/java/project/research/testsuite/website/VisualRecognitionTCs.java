package project.research.testsuite.website;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.RectangleSize;
import com.applitools.eyes.StdoutLogHandler;
import com.applitools.eyes.TestResultsSummary;
import com.applitools.eyes.images.Eyes;
import com.applitools.eyes.selenium.BrowserType;
import com.applitools.eyes.selenium.Configuration;
import com.applitools.eyes.selenium.fluent.Target;
import com.applitools.eyes.visualgrid.model.DeviceName;
import com.applitools.eyes.visualgrid.model.ScreenOrientation;
import com.applitools.eyes.visualgrid.services.VisualGridRunner;

import infrastructure.utility.DirectoryUtils;
import project.pim.center.SubTransferStator;

public class VisualRecognitionTCs extends SubTransferStator {

	@Test
	public void visualImageByApplitools() throws IOException, URISyntaxException, InterruptedException {

		String STORAGE_INPUT = DirectoryUtils.RESOURCE_PATH + "\\sikuli-input\\";
		String STORAGE_OUTPUT = DirectoryUtils.RESOURCE_PATH + "\\sikuli-output\\";
		BufferedImage img;

		BatchInfo batch;
		batch = new BatchInfo("Batch App");

		// Initialize the Runner for your test.
		VisualGridRunner runner = new VisualGridRunner(1);
//		EyesRunner runner = new ClassicRunner();

		Eyes eyes = new Eyes();
		eyes.setApiKey("dFUqVGL122fgjBeKpEKAqSFaZhtBMOBDTdm8xq97chA110");

		// Set your personal Applitols API Key from your environment variables.
//		eyes.setApiKey(System.getenv("dFUqVGL122fgjBeKpEKAqSFaZhtBMOBDTdm8xq97chA110"));
		eyes.setBatch(batch);

		// Define the OS and hosting application to identify the baseline.
		eyes.setHostOS("Windows 7");
		eyes.setHostApp("Chrome Browser");

		// Define the OS and hosting application to identify the baseline
		eyes.setAppEnvironment("Windows 7", "Chrome Browser");

		// Initialize eyes Configuration
		Configuration config = new Configuration();

		// create a new batch info instance and set it to the configuration
		config.setBatch(new BatchInfo("Ultrafast Batch"));

		// Add browsers with different viewports
		config.addBrowser(800, 600, BrowserType.CHROME);
		config.addBrowser(700, 500, BrowserType.FIREFOX);
		config.addBrowser(1600, 1200, BrowserType.IE_11);
		config.addBrowser(1024, 768, BrowserType.EDGE_CHROMIUM);
		config.addBrowser(800, 600, BrowserType.SAFARI);

		// Add mobile emulation devices in Portrait mode
		config.addDeviceEmulation(DeviceName.iPhone_X, ScreenOrientation.PORTRAIT);
		config.addDeviceEmulation(DeviceName.Pixel_2, ScreenOrientation.PORTRAIT);

		// Set the configuration object to eyes
		eyes.setConfiguration(config);

		eyes.setLogHandler(new StdoutLogHandler(true));
		try {
			eyes.open("IDS WebApp", "Sanity Test", new RectangleSize(1600, 900));
			img = ImageIO.read(new File(STORAGE_INPUT + "ids-finder.PNG"));

			eyes.checkWindow(img, "Contact Us Page");
			eyes.checkImage(img, "Contact Us Page");

			eyes.check(Target.window().fully().getName(), null);

			// Finish the test
			eyes.close();
		} finally {
			eyes.abortAsync();
			// If the test was aborted before eyes.close was called, ends the test as
			// aborted.
			eyes.abortIfNotClosed();
		}
		// Wait and collect all test results
		TestResultsSummary allTestResults = runner.getAllTestResults();
		// Print results
		APP_LOGS(allTestResults.toString());
	}

	@Test
	public void recognizeIDSWebImage() throws IOException {

		selenium.navigateToURL("http://idsoftware.biz/about-us");

		String STORAGE_FOLDER = DirectoryUtils.RESOURCE_PATH + "\\sikuli-element\\";
		Screen screen = new Screen();

		Pattern captchaCheckbox = new Pattern(STORAGE_FOLDER + "captcha-checkbox.PNG");
		Pattern contactButton = new Pattern(STORAGE_FOLDER + "contact-button.PNG");
		Pattern emailTextbox = new Pattern(STORAGE_FOLDER + "email-textbox.PNG");
		Pattern nameTextbox = new Pattern(STORAGE_FOLDER + "name-textbox.PNG");
		Pattern projectTextbox = new Pattern(STORAGE_FOLDER + "project-textbox.PNG");

		Pattern windowsIcon = new Pattern(STORAGE_FOLDER + "windows-icon.PNG");
		Pattern excelIcon = new Pattern(STORAGE_FOLDER + "excel-icon.PNG");

		try {
			screen.wait(contactButton, 5);
//			sikuli.matchScreenImageRecognition("ids-pattern.PNG", "ids-finder.PNG");

			screen.click(contactButton);
			screen.type(nameTextbox, "Tai Choa Tien Sinh");
			screen.type(emailTextbox, "ngamyducphapanh@taichou.com");
			screen.type(projectTextbox, "Xin duoc cong hien mai mai cho cong ty.");
			screen.click(captchaCheckbox);

			screen.click(windowsIcon);
			screen.click(excelIcon);
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
