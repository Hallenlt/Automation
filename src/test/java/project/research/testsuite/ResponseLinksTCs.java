package project.research.testsuite;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class ResponseLinksTCs extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

	}

	@Test(priority = 0)
	public void Detect_HTTP_Status_Response_All_Links_URL() {

		String address = "http://192.168.100.4/tsuri-kahoku/";
		selenium.navigateToURL(address);
		response.detectHttpResponseAllLinkURLs(address);
	}

}