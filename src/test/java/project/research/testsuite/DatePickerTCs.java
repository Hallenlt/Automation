package project.research.testsuite;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class DatePickerTCs extends SubTransferStator {

	@Test(dataProvider = "inputDates")
	public void dateTest(String inputDate, String outputDate) {

		datepicker.goTo();
		datepicker.getDatePicker().setDate(inputDate);
		Assert.assertEquals(datepicker.getSelectedDate(), outputDate);
	}

	@DataProvider(name = "inputDates")
	public static Object[][] getDates() {

		return new Object[][] { { "15 February 2020", "02/15/2020" }, { "24 October 2020", "10/24/2020" } };
	}

}