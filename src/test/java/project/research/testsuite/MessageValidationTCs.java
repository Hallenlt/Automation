package project.research.testsuite;

import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class MessageValidationTCs extends SubTransferStator {

	@BeforeMethod
	public void before() {
		
		Reporter.log("\n [INFO] - Executed | Xác nhận tiêu chuẩn Encoding", false);

		selenium.navigateToURL("http://192.168.100.4:8107/ezreso/login");
		selenium.sendKeys("//input[@name='username']", "admin@idsolutions.com.vn");
		selenium.sendKeys("//input[@name='password']", "123456");
		selenium.click("//input[@type='submit']");
	}

	@Test
	public void testHtml5ValidationMessage() {

		selenium.click("//*[@id=\"container\"]/header/div/div/div[3]/div/ul/li[4]/a");
		selenium.click("//*[@id=\"container\"]/div[2]/div/div[1]/div[1]/div[2]/a");
		selenium.click("//*[@id=\"name\"]");
		selenium.assertHtml5ValidationMessage("//*[@id=\"name\"]", "Vui lòng nhập tên công việc");
	}

}