package project.research.object.website;

import org.openqa.selenium.remote.RemoteWebDriver;

import infrastructure.selenium.ClassifierClientCore;

public class ClassifierClientPage extends ClassifierClientCore {

	public ClassifierClientPage(RemoteWebDriver remoteDriver) {
		super(remoteDriver);
	}

	public void clickMatchingLable() {
		click("icon");
	}

}