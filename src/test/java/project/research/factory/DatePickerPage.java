package project.research.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import infrastructure.selenium.DatePickerCore;
import project.pim.center.SubTransferStator;

public class DatePickerPage extends SubTransferStator {

	public DatePickerPage(final WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@id='datepicker']")
	WebElement dateField;

	@FindBy(xpath = "//div[@id='ui-datepicker-div']")
	DatePickerCore datePicker;

	public void goTo() {
		driver.get("https://jqueryui.com/datepicker");
		this.driver.switchTo().frame(0);
		this.datePicker = new DatePickerCore(driver);
	}

	public DatePickerCore getDatePicker() {
		dateField.click();
		return datePicker;
	}

	public String getSelectedDate() {
		return this.dateField.getAttribute("value");
	}

}