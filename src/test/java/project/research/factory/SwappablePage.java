package project.research.factory;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SwappableCore;
import project.pim.center.SubTransferStator;

public class SwappablePage extends SubTransferStator {

	SwappableCore swappable;
	
	public SwappablePage(final WebDriver driver) {
		this.driver = driver;
	}

	public void goTo() {
		driver().get("https://jqueryui.com/sortable");
		this.driver().switchTo().frame(0);
		this.swappable = new SwappableCore(driver());
	}

	public SwappableCore getswappables() {
		return swappable;
	}

}