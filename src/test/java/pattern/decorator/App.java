package pattern.decorator;

public interface App {

	void developApp();
}