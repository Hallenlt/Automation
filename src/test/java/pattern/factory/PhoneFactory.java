package pattern.factory;

public class PhoneFactory {

	public Phone getPhone(PhoneType phoneType) {
		
		Phone phoneCreated = null;
		switch (phoneType) {
		case SAMSUNG:
			phoneCreated = new SamsungPhone();
			break;
		case APPLE:
			phoneCreated = new ApplePhone();
			break;
		}
		return phoneCreated;
	}

}