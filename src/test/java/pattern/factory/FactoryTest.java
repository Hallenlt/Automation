package pattern.factory;

public class FactoryTest {

	public static void main(String[] args) {
		
		PhoneFactory phoneFactory = new PhoneFactory();
		Phone phone = phoneFactory.getPhone(PhoneType.SAMSUNG);
		phone.showInfo();
	}

}