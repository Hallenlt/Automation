package pattern.factory;

public interface Phone {

	void showInfo();

}