package pattern.strategy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RoleAdminStrategy implements AuthStrategyInterface {

	WebDriver driver;
	By searchBox = By.name("q");
	By searchButton = By.name("btnG");

	public void search(String searchFor) {

		System.out.println("Searching using Text Straegy:" + searchFor);
		driver.findElement(searchBox).sendKeys(searchFor);
		driver.findElement(searchButton).click();
	}

	@Override
	public void authorize(String searchFor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDriver(WebDriver driver) {
		// TODO Auto-generated method stub
		this.driver = driver;
	}

}