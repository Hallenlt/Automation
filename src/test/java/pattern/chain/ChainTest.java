package pattern.chain;

import org.testng.annotations.Test;

public class ChainTest {

//	@Test
	public void testChain() {

		// configure ChainReposibilityInterface of Responsibility
		ChainReposibilityInterface c1 = new NegativeProcessor();
		ChainReposibilityInterface c2 = new ZeroProcessor();
		ChainReposibilityInterface c3 = new PositiveProcessor();
		c1.setNext(c2);
		c2.setNext(c3);

		// calling chain of responsibility
		c1.process(new Number(50));
		c1.process(new Number(0));
		c1.process(new Number(-50));
	}

}