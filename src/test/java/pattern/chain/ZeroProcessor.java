package pattern.chain;

class ZeroProcessor implements ChainReposibilityInterface {

	ChainReposibilityInterface nextInChain;

	public void setNext(ChainReposibilityInterface c) {
		nextInChain = c;
	}

	public void process(Number request) {

		if (request.getNumber() == 0) {
			System.out.println("ZeroProcessor : " + request.getNumber());
		} else {
			nextInChain.process(request);
		}
	}

}