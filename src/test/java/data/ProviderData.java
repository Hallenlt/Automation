package data;

import org.testng.annotations.DataProvider;

import com.github.javafaker.Faker;

import infrastructure.root.CommonRandomCore;
import project.pim.center.SubTransferStator;

public class ProviderData extends SubTransferStator {

	@DataProvider(name = "getShortLanguage")
	protected static Object[][] getDataShortLanguage() {

		return new Object[][] { { "be" }, { "en" }, { "fr" }, { "vi" } };
	}

	@DataProvider(name = "getFullLanguage")
	protected static Object[][] getDataFullLanguage() {

		return new Object[][] { { "Belgium" }, { "English" }, { "France" }, { "vi" } };
	}

	@DataProvider(name = "getDataAttachment")
	protected static Object[][] getDataAttachment() {

		Object[][] createData = new Object[4][1];

		createData[0][0] = "portable-file.pdf";
		createData[1][0] = "word-file.docx";
		createData[2][0] = "excel-file.xlsx";
		createData[3][0] = "comma-file.csv";

		return createData;
	}

	@DataProvider(name = "getDataImage")
	protected static Object[][] getDataImage() {

		Object[][] createData = new Object[5][1];

		createData[0][0] = "jpg-image.jpg";
		createData[1][0] = "png-image.png";
		createData[2][0] = "svg-image.svg";
		createData[3][0] = "webp-image.webp";
		createData[4][0] = "40mb-image.jpg";

		return createData;
	}

	@DataProvider(name = "getDataDate")
	protected static Object[][] getDate() {

		Object[][] createData = new Object[3][1];

		createData[0][0] = "yesterday";
		createData[1][0] = "today";
		createData[2][0] = "tomorrow";

		return createData;
	}

	@DataProvider(name = "getDataNumericHasNull")
	protected static Object[][] getDataNumericHasNull() {

		Object[][] createData = new Object[3][1];

		createData[0][0] = "";
		createData[1][0] = CommonRandomCore.randomNumeric(1);
		new CommonRandomCore();
		createData[2][0] = CommonRandomCore.randomNumeric(20);
		new CommonRandomCore();

		return createData;
	}

	@DataProvider(name = "getDataNumericNotNull")
	protected static Object[][] getDataNumericNotNull() {

		Object[][] createData = new Object[2][1];

		createData[1][0] = CommonRandomCore.randomNumeric(1);
		new CommonRandomCore();
		createData[2][0] = CommonRandomCore.randomNumeric(20);
		new CommonRandomCore();

		return createData;
	}

	@DataProvider(name = "getDataTypeHasNull")
	protected static Object[][] getDataTypesHasNull() {

		Object[][] createData = new Object[8][1];

//		Input [] text fields with empty value
		createData[0][0] = "";
		// Input minimum length in [] text fields with 1 chars
		createData[1][0] = CommonRandomCore.randomAlphaNumeric(1);
		new CommonRandomCore();
		// Input minimum length in [] text fields with 1 chars
		createData[2][0] = CommonRandomCore.randomAlphaNumeric(1);
		new CommonRandomCore();
		// Input [] text fields with special chars
		createData[3][0] = CommonRandomCore.randomValidSpecialChars(20);
		new CommonRandomCore();
		// Input [] text fields with UTF-8 chars
		createData[4][0] = CommonRandomCore.randomUtf8EncodingChars(20);
		new CommonRandomCore();
		// Input [] text fields with invalid special chars
		createData[5][0] = CommonRandomCore.randomInvalidSpecialChars(20);
		new CommonRandomCore();
		// Input [] text fields with upper case
		createData[6][0] = CommonRandomCore.randomAlphaNumeric(20).toUpperCase();
		new CommonRandomCore();
		// Input [] text fields with lower case
		createData[7][0] = CommonRandomCore.randomAlphaNumeric(20).toLowerCase();
		new CommonRandomCore();

		// Input [] text fields with down the line each a string
//		createData[8][0] = CommonRandom.randomDownTheLineString(20);
//		new CommonRandom();

		return createData;
	}

	@DataProvider(name = "getDataTypeNotNull")
	protected static Object[][] getDataTypesNotNull() {

		Object[][] createData = new Object[5][1];

		// Input [] text fields with special chars
		createData[0][0] = CommonRandomCore.randomValidSpecialChars(20);
		new CommonRandomCore();
		// Input [] text fields with UTF-8 chars
		createData[1][0] = CommonRandomCore.randomUtf8EncodingChars(20);
		new CommonRandomCore();
		// Input [] text fields with invalid special chars
		createData[2][0] = CommonRandomCore.randomInvalidSpecialChars(20);
		new CommonRandomCore();
		// Input [] text fields with upper case
		createData[3][0] = CommonRandomCore.randomAlphaNumeric(20).toUpperCase();
		new CommonRandomCore();
		// Input [] text fields with lower case
		createData[4][0] = CommonRandomCore.randomAlphaNumeric(20).toLowerCase();
		new CommonRandomCore();

		// Input [] text fields with down the line each a string
//		createData[6][0] = CommonRandom.randomDownTheLineString(20);
//		new CommonRandom();

		return createData;
	}

	@DataProvider(name = "getDataPassword")
	protected static Object[][] getDataPassword() {

		Object[][] createData = new Object[8][1];

		// Input [] text fields with long string
		createData[0][0] = CommonRandomCore.randomAlphaNumeric(50);
		new CommonRandomCore();
		// Input [] text fields with special chars
		createData[1][0] = CommonRandomCore.randomValidSpecialChars(20);
		new CommonRandomCore();
		// Input [] text fields with UTF-8 chars
		createData[2][0] = CommonRandomCore.randomUtf8EncodingChars(20);
		new CommonRandomCore();
		// Input [] text fields with invalid special chars
		createData[3][0] = CommonRandomCore.randomInvalidSpecialChars(20);
		new CommonRandomCore();
		// Input [] text fields with upper case
		createData[4][0] = CommonRandomCore.randomAlphaNumeric(20).toUpperCase();
		new CommonRandomCore();
		// Input [] text fields with lower case
		createData[5][0] = CommonRandomCore.randomAlphaNumeric(20).toLowerCase();
		new CommonRandomCore();
		// Input [] text fields with valid numeric
		createData[6][0] = CommonRandomCore.randomNumeric(20);
		new CommonRandomCore();
		// Input [] text fields with long numeric
		createData[7][0] = CommonRandomCore.randomNumeric(50);
		new CommonRandomCore();

		return createData;
	}

	@DataProvider(name = "getDataBoundary")
	protected static Object[][] getDataBoundary() {

		Object[][] createData = new Object[3][1];

//		Input [] text fields with boundary max length analysis
		createData[0][0] = -1;
		createData[1][0] = 0;
		createData[2][0] = 1;

		return createData;
	}

	@DataProvider(name = "getDataNested")
	protected static Object[][] getDataNested() {

		Object[][] data = new Object[3][2];

		for (int i = 0; i < 10; i++) {

			for (int j = 0; j < 2; j++) {

				data[i][j] = new Faker().beer().malt();
			}
		}
		return data;
	}

}