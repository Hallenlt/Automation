package infrastructure;

import static org.testng.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class DuplicatedValidator extends SubTransferStator {

	@Test(testName = "Trường Hợp Kiểm Thử", description = "Mô Tả Kịch Bản Kiểm Thử")
	public void test() {

		String[] sValue = new String[] { "a", "b", "c", "d", "e" };

		assertTrue(checkDuplicatedWithNormal(sValue));
		System.out.println("Check normal: value NOT duplicated! \n");

		assertTrue(checkDuplicatedWithSet(sValue));
		System.out.println("Check set: Value NOT duplicated! \n");

		APP_LOGS("Kiểm thử thành công.");
	}

	public static boolean checkDuplicatedWithNormal(String[] sValueTemp) {

		for (int i = 0; i < sValueTemp.length; i++) {

			String sValueToCheck = sValueTemp[i];
			if (sValueToCheck == null || sValueToCheck.equals(""))
				continue;

			for (int j = 0; j < sValueTemp.length; j++) {

				if (i == j)
					continue;
				String SvalueToCompare = sValueTemp[j];
				if (sValueToCheck.equals(SvalueToCompare))
					return false;
			}

		}
		return true;
	}

	protected static boolean checkDuplicatedWithSet(String[] sValueTemp) {

		Set<Object> sValueSet = new HashSet<Object>();
		for (String tempValueSet : sValueTemp) {

			if (sValueSet.contains(tempValueSet))
				return false;

			else if (!tempValueSet.equals(""))
				sValueSet.add(tempValueSet);
		}
		return true;
	}

}
