package infrastructure.root;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import infrastructure.root.CommonLibrariesCore;

public class CommonAlgorithmCore {

	public CommonAlgorithmCore() {
		// TODO Auto-generated constructor stub
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
	}

	// 1. String reverse. Write a method that will take one string as an argument
	// and will return the reverse version of this string.

	public String stringReverse(String str) {

		StringBuilder reverse = new StringBuilder();

		for (int i = str.length() - 1; i >= 0; i--) {

			reverse.append(String.valueOf(str.charAt(i)));
		}
		System.out.println("Return The Reverse Version Of String: " + str + " --> " + reverse.toString());

		return reverse.toString();
	}

	public String stringInvert(String str) {

		StringBuffer invert = new StringBuffer(str);
		invert.reverse();

		System.out.println("Invert string as an argument: " + invert.toString());

		return invert.toString();
	}

	// 2. Array reverse. Write a method that will take an array as an argument and
	// reverse it.

	public void numberReverse(int[] arrNum) {

		// revArr({1, 2, 3, 4, 5}) -> {5, 4, 3, 2, 1}
		// revArr({1}) -> {1}
		// revArr({1, 2, 3}) -> {3, 2, 1}

		// we will use two 'pointers'. One pointer will start from the beginning
		// another one from the back and we will swap their values

		// pointer that will start from the back
		int j = arrNum.length - 1;

		// our loop will go till half of our input array
		// 'i' is a pointer that will start from the beginning
		for (int i = 0; i < arrNum.length / 2; i++) {
			// swap elements using positions of i and (j - i)
			int tmp = arrNum[i];
			arrNum[i] = arrNum[j - i];
			arrNum[j - i] = tmp;
		}
		System.out.println("Reverse numbers of array as an argument: " + arrNum);
	}

	// 3. Reverse words. Write a method that will take a string as an argument. The
	// method will reverse the position of words and return it.

	public String ReverseWords(String str) {

		// revWords("apple banana kiwi") -> "kiwi banana apple"
		// revWords("I am John Doe") -> "Doe John am I"
		// revWords("orange") -> "orange"

		StringBuilder reversedWords = new StringBuilder();

		// split input string by " " space to get each word as String[]
		String[] words = str.split(" ");

		// loop over the array from back
		for (int i = words.length - 1; i >= 0; i--) {
			// add words to reversedWords with space
			reversedWords.append(words[i] + " ");
		}
		System.out.println("Reverse the position of words: " + reversedWords.toString().trim());

		// trim needed to remove last space in the end
		return reversedWords.toString().trim();
	}

	// 4. String palindrome. A palindrome is a word, phrase, number, or sequence of
	// words that reads the same backward as forward.

	public boolean stringPalindrome(String str) {

		// isPal("anna") -> true
		// isPal("civic") -> true
		// isPal("apple") -> false
		// isPal("level") -> true

		// we will use two 'pointers'. One pointer will start looking from beginning
		// another from the back. If values of pointers are not equal, we can return
		// false

		int j = str.length() - 1; // pointer for the back

		// loop will go till half because we have two pointers
		for (int i = 0; i < str.length() / 2; i++) {
			// if pointers values are not equal return false
			if (str.charAt(i) != str.charAt(j - i)) {
				return false;
			}
		}

		System.out.println("Is string palindrome: " + str);

		// if program reach here, it means all values were equal so it's palindrome
		return true;
	}

	// 5. Number palindrome. A palindrome is a word, phrase, number, or sequence of
	// words that reads the same backward as forward. The straight forward solution
	// would be to convert number to string and use the above approach. Some
	// interviewers will not allow to do it. So let’s take a look at what we can do
	// here.

	public boolean numberPalindrome(int num) {

		// isPalNum(545) -> true
		// isPalNum(1001) -> true
		// isPalNum(13) -> false
		// isPalNum(33) -> true

		// there are two most important things to remember
		// to get most right number, we can do 'num % 10'
		// to remove most right number, we can do 'num / 10'
		// both will work for any numbers

		int copyOfOriginal = num;
		int reversedNumber = 0;
		int remainder;

		while (num > 0) {
			// get most right number
			remainder = num % 10;

			// multiply by 10 to concat, not to add(plus)
			reversedNumber = (reversedNumber * 10) + remainder;

			// remove most right number from num.
			num = num / 10;
		}

		System.out.println("Is number palindrome: " + reversedNumber + " == " + copyOfOriginal);

		// if reversed version and original are equal so it's palindrome
		return reversedNumber == copyOfOriginal;
	}

	// 6. Max/min number from an array. Write a method that will accept an array of
	// int as an argument and it will return the max/min number from a given array.

	public int maxArray(int[] arrNum) {

		// max({4, 781, 8, 99, 103}) -> 781
		// max({1, 2, 3, 4, 5}) -> 5
		// max({3, 4}) -> 4
		// max({100}) -> 100

		// assume first element of array is biggest number
		int max = arrNum[0];

		// loop over the array and test our above assumption
		for (int i = 0; i < arrNum.length; i++) {
			// if max was not the biggest number, update it
			if (max < arrNum[i]) {
				max = arrNum[i];
			}
		}
		System.out.println("The smallest number from an array: " + max);

		// after the loop max variable will hold the biggest number
		return max;
		// Time Complexity: O(n)
	}

	public int minArray(int[] arrNum) {

		// min({4, 781, 8, 99, 103}) -> 4
		// min({1, 2, 3, 4, 5}) -> 1
		// min({3, 4}) -> 3
		// min({100}) -> 100

		// assume first element of array is the smallest number
		int min = arrNum[0];

		// loop over the array and test assumption
		for (int i = 0; i < arrNum.length; i++) {
			// if min was not smallest, update it
			if (min > arrNum[i]) {
				min = arrNum[i];
			}
		}
		System.out.println("The smallest number from an array: " + min);

		return min;
		// Time Complexity: O(n)
	}

	// 7. Find the second min/max number from an array. Write a method that will
	// accept int array as an argument and return second or n min/max number from
	// the given array. I will go with the sorting algorithm. Arrays.sort() is using
	// mergesort and it’s the time complexity is logarithmic (n log n). Some
	// interviewers don’t like it when sorting is used. It’s possible to do without
	// sorting, using nested loops and we will end up having O(n²).

	public int secondMax(int[] numArr) {

		// secMax({4, 781, 8, 99, 103}) -> 103
		// secMax({1, 2, 3, 4, 5}) -> 4
		// secMax({3, 4}) -> 3

		// sort an array
		Arrays.sort(numArr);

		System.out.println("The second max number from an array: " + numArr[numArr.length - 2]);

		// return one before last. Array is sorted from smallest to biggest
		return numArr[numArr.length - 2];
	}

	public int secondMin(int[] numArr) {

		// secMin({4, 781, 8, 99, 103}) -> 8
		// secMin({1, 3, 2, 4, 5}) -> 2
		// secMin({3, 4}) -> 4

		// sort an array
		Arrays.sort(numArr);

		System.out.println("The second min number from an array: " + numArr[1]);

		// return second element. Array is soreted from smallest to biggest
		return numArr[1];
		// for both methods Time Complexity: O(n log n)
	}

	// 11. Two string anagram. An anagram is when all the letters in one string
	// exist in another but the order of letters does not matter. Write a method
	// that will accept two string arguments and will return true if they are
	// anagram and false if they are not.

	public boolean twoStringAnagram(String str, String str1) {

		// isAnagram("listen", "silent") -> true
		// isAnagram("triangle", "integral") -> true
		// isAnagram("abc", "bca") -> true
		// isAnagram("abc", "ccb") -> false
		// isAnagram("aaa", "aaab") -> false

		char[] arrStr = str.toCharArray();
		char[] arrStr1 = str1.toCharArray();

		// sort both char[] arrays
		Arrays.sort(arrStr);
		Arrays.sort(arrStr1);

		System.out.println("Two string anagram: " + arrStr + " --> " + arrStr1);

		// compare sorted arrays. If sorted arrays are equal, two strings are anagram
		return Arrays.equals(arrStr, arrStr1);
	}

	// 12. Remove duplicates from a string. Write a method that will accept one
	// string argument and return it without duplicates. We will see two versions of
	// this method.

	public String removeDuplicates(String str) {

		// removeDup("hello") -> "helo"
		// removeDup("apple") -> "aple"
		// removeDup("aaaaaa") -> "a"
		// removeDup("abc") -> "abc"

		String strNoDup = "";

		// loop over string and get each char
		for (char ch : str.toCharArray()) {
			// if strNoDup does not contain char then add to it
			if (!strNoDup.contains(String.valueOf(ch))) {
				strNoDup += ch;
			}
		}
		System.out.println("Remove duplicates from a string: " + strNoDup);

		return strNoDup;
	}

	public String removeDuplicated(String str) {

		String strNoDup = "";

		// convert str to char []
		char[] letters = str.toCharArray();
		Set<Character> set = new LinkedHashSet<>();

		// add each letter to set. It will remove duplicates - Set does allow duplicates
		for (Character ch : letters) {
			set.add(ch);
		}
		System.out.println("Remove duplicated from a string: ");

		return strNoDup;
	}

	// Remove duplicates from ArrayList
	public void removeDuplicatesArray(String[] arr) {

		ArrayList<String> list = new ArrayList<>(Arrays.asList(arr));
		System.out.println("ArrayList with duplicates: " + list);

		// Creating a new ArrayList
		ArrayList<String> newList = new ArrayList<>();

		// Traversing through the first list
		for (String element : list) {
			// If this element is not present in newList, then add it
			if (!newList.contains(element)) {
				newList.add(element);
			}
		}
		System.out.println("ArrayList with duplicates removed: " + newList);
	}

	// 13. Count letters(Map). Write a method that will accept a string as an
	// argument. The method will count the number of appearances of each char and
	// return map. The key will be a letter and the value will be a number of
	// appearances in the string. See input and output in the example.

	public Map<Character, Integer> countLetters(String str) {

		// countLetters("hello") -> {{'h', 1}, {'e', 1}, {'l', 2}, {'o', 1}}
		// countLetters("aauuchhh") -> {{'a', 2}, {'u', 2}, {'c', 1}, {'h', 3}}
		// countLetters("aaaaaa") -> {{'a', 6}}
		// countLetters("abc") -> {{'a', 1}, {'b', 1}, {'c', 1}}

		Map<Character, Integer> letters = new HashMap<>();

		for (int i = 0; i < str.length(); i++) {

			char ch = str.charAt(i);

			// if map already contains the key, get the value and put back with +1
			if (letters.containsKey(ch)) {
				letters.put(ch, letters.get(ch) + 1);
			} else {
				// if does not contains char as key, new letter put with value 1
				letters.put(ch, 1);
			}
		}
		System.out.println("Count the number of appearances of each char: " + letters.toString());

		return letters;
	}

	// 14. FizzBuzz. Print numbers from 1 to 100
	// - if a number is divisible by 3 print Fizz
	// - if a number is divisible by 5 print Buzz
	// - if a number is divisible by both 3 and 5 print FizzBuzz

	public void fizzBuzz() {

		for (int i = 1; i <= 100; i++) {

			// first check if number is divisible by 3 and 5
			if (i % 3 == 0 && i % 5 == 0) {
				System.out.println("Fizz");
			} else if (i % 5 == 0) {
				System.out.println("Buzz");
			} else if (i % 3 == 0) {
				System.out.println("FizzBuzz");
			} else {
				System.out.println(i);
			}
		}
		// Time Complexity: O(1)
	}

	// 15. Even or Odd. Write a method that will accept one int as an argument. The
	// method will print Even if the number is even and Odd if the number is odd.

	public void evenOrOdd(int num) {

		// evenOrOdd(5) -> Odd
		// evenOrOdd(2) -> Even
		// evenOrOdd(100) -> Even
		// evenOrOdd(101) -> Odd

		if (num % 2 == 0) {
			System.out.println("Even");
		} else {
			System.out.println("Odd");
		}
		// Time Complexity: O(1)
	}

	// 16. Sum. Write a method that accepts int[] array and an int number, find 2
	// elements in the array that sum is equal to the given int. Assume that an
	// input array will have only one pair of numbers that sum equal to our given
	// number. It will always have this pair. See input and output examples. I will
	// use the Brute Force algorithm. If you have a better algorithm, please put it
	// in the comments. I will be happy to see it.

	public int[] sumOfOnePair(int numArr[], int num) {

		// sum({1, 2, 3, 5}, 4) -> {1, 3}
		// sum({7, 7, 4, 3, 8}, 7) -> {4, 3}
		// sum({13, 43, 2, 71}, 84) -> {13, 71}

		int[] sumNumbers = new int[2];

		for (int i = 0; i < numArr.length; i++) {

			for (int j = i + 1; j < numArr.length; j++) {

				if (numArr[i] + numArr[j] == num) {
					sumNumbers[0] = numArr[i];
					sumNumbers[1] = numArr[j];
				}
			}
		}
		System.out.println("One pair of numbers that sum equal to the given number: " + sumNumbers.toString());

		return sumNumbers;
	}

	// 20. Print all the permutations of a string
	public void permutation(String s, String result) {

		// If string is empty
		if (s.length() == 0) {

			System.out.println(result + " ");
			return;
		}

		for (int i = 0; i < s.length(); i++) {

			// ith character of string
			char ch = s.charAt(i);

			// remaining string after excluding the ith character
			String remainStr = s.substring(0, i) + s.substring(i + 1);

			// recursive call
			permutation(remainStr, result + ch);
		}
	}

	public void moveZerosToEnd(int[] arrNum) {

		int n = arrNum.length;
		// Count of non-zero elements
		int count = 0;

		// Traverse the array. If element encountered is
		// non-zero, then replace the element at index 'count'
		// with this element
		for (int i = 0; i < n; i++)

			if (arrNum[i] != 0)
				arrNum[count++] = arrNum[i];

		// Now all non-zero elements have been shifted to
		// front and 'count' is set as index of first 0.
		// Make all elements 0 from count to end.
		while (count < n)
			arrNum[count++] = 0;

		System.out.println("Array after pushing zeros to the end: ");

		for (int i = 0; i < n; i++)

			System.out.println(arrNum[i] + " ");
	}

	// Count occurrences of each character in a string
	public void countCharacterInString(int maxChar, String str) {

		// Create an array of size 256 i.e. ASCII_SIZE
		int count[] = new int[maxChar];
		int len = str.length();

		// Initialize count array index
		for (int i = 0; i < len; i++)
			count[str.charAt(i)]++;

		// Create an array of given String size
		char ch[] = new char[str.length()];

		for (int i = 0; i < len; i++) {

			ch[i] = str.charAt(i);
			int find = 0;

			for (int j = 0; j <= i; j++) {

				// If any matches found
				if (str.charAt(i) == ch[j])
					find++;
			}

			if (find == 1)
				System.out.println("Number of Occurrence of " + str.charAt(i) + " is:" + count[str.charAt(i)]);
		}
	}

	// Replace the substring with another string
	public void replaceSubstring(String str, String tobeReplaced, String toReplacedwith) {

		String[] astr = str.split(tobeReplaced);
		StringBuffer strb = new StringBuffer();

		for (int i = 0; i <= astr.length - 1; i++) {

			strb = strb.append(astr[i]);

			if (i != astr.length - 1) {
				strb = strb.append(toReplacedwith);
			}
		}
		System.out.println(strb.toString());
	}

	// Find the largest value from the given array
	public void largestValueArray(int[] arrNum) {

		int val = arrNum[0];

		for (int i = 0; i < arrNum.length; i++) {

			if (arrNum[i] > val) {
				val = arrNum[i];
			}
		}
		System.out.println("Largest value in the Given Array is " + val);
	}

	// Sort strings in dictionary order
	public void dictionaryOrder(String[] arr) {

		for (int i = 0; i < 3; ++i) {

			for (int j = i + 1; j < 4; ++j) {

				if (arr[i].compareTo(arr[j]) > 0) {
					String temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		System.out.println("In lexicographical order:");

		for (int i = 0; i < 4; i++) {

			System.out.println(arr[i]);
		}
	}

	// Find the frequency of character in a string
	public void frequencyOfCharacter(String str, char ch) {

		int frequency = 0;

		for (int i = 0; i < str.length(); i++) {

			if (ch == str.charAt(i)) {
				++frequency;
			}
		}
		System.out.println("Frequency of " + ch + " = " + frequency);
	}

}
