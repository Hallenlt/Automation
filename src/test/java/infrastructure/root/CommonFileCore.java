package infrastructure.root;

import static org.testng.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.testng.Reporter;

import com.google.common.io.Files;

public class CommonFileCore extends CommonLibrariesCore {

	public static void copyFile(String frompath, String targetpath, String filename) {

		Reporter.log("\n [INFO] - Executed | Copy File: " + "From Path: " + frompath + " --> Target Path: " + " --> File Name: " + filename + " |", true);
		File fromfile = new File(frompath + filename);
		File tofile = new File(targetpath + filename);
		try {
			Files.copy(fromfile, tofile);
			APP_LOGS("Copy File: " + "From Path: " + frompath + " --> Target Path: " + " --> File Name: " + filename + " |");
		} catch (IOException e) {
			// TODO Auto-generated catch block

			fail(null, e);
		}
	}

	public static String moveFile(String frompath, String targetpath, String filename) {

		Reporter.log("\n [INFO] - Executed | Moving File: " + "From Path: " + frompath + " --> Target Path: " + " --> File Name: " + targetpath + filename + " |", true);
		File fromfile = new File(frompath);
		File tofile = new File(targetpath + filename);
		try {
			fromfile.renameTo(tofile);
		} catch (Exception e) {
			// TODO: handle exception

			fail(null, e);
		}
		return filename;
	}

}
