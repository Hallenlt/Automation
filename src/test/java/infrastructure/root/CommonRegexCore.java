package infrastructure.root;

import static org.testng.Assert.assertTrue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonRegexCore extends CommonLibrariesCore {

	static final Pattern EMAIL_PATTERN = Pattern.compile("^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");

	static final Pattern IPV4_PATTERN = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.(?!$)|$)){4}$");

	static final Pattern IMAGE_PATTERN = Pattern.compile("([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)");

	static final Pattern USERNAME_PATTERN = Pattern.compile("^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$");

	static final Pattern PASSWORD_PATTERN = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})");

	static final String DATE_PATTERN = "uuuu-M-d";

	// (?=.{1,64}@) # local-part min 1 max 64
	//
	// [A-Za-z0-9_-]+ # Start with chars in the bracket [ ], one or more (+)
	// # dot (.) not in the bracket[], it can't start with a dot (.)
	//
	// (\\.[A-Za-z0-9_-]+)* # follow by a dot (.), then chars in the bracket [ ] one
	// or more (+)
	// # * means this is optional
	// # this rule for two dots (.)
	//
	// @ # must contains a @ symbol
	//
	// [^-] # domain can't start with a hyphen (-)
	//
	// [A-Za-z0-9-]+ # Start with chars in the bracket [ ], one or more (+)
	//
	// (\\.[A-Za-z0-9-]+)* # follow by a dot (.), optional
	//
	// (\\.[A-Za-z]{2,}) # the last tld, chars in the bracket [ ], min 2

	public static boolean isValidSimpleDate(final String input) {
		boolean valid = false;
		try {
			dateformat.parse(input);
			dateformat.setLenient(false);
			valid = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			valid = false;
		}
		return valid;
	}

	public void isValidSimpleDateMatcher(final String input) {
		assertTrue(isValidSimpleDate(input));
	}

	public static boolean isValidDate(final String input) {
		boolean valid = false;
		try {
			// ResolverStyle.STRICT for 30, 31 days checking, and also leap year.
			LocalDate.parse(input, DateTimeFormatter.ofPattern(DATE_PATTERN).withResolverStyle(ResolverStyle.STRICT));
			valid = true;

		} catch (DateTimeParseException e) {
			// TODO: handle exception
			e.printStackTrace();
			valid = false;
		}
		return valid;
	}

	public void isValidDateMatcher(final String input) {
		assertTrue(isValidDate(input));

	}

	public void isValidPasswordMatcher(final String input) {
		Matcher matcher = PASSWORD_PATTERN.matcher(input);
		assertTrue(matcher.matches());
	}

	public void isValidUsernameMatcher(final String input) {
		Matcher matcher = USERNAME_PATTERN.matcher(input);
		assertTrue(matcher.matches());
	}

	public void isValidImageMatcher(final String input) {
		Matcher matcher = IMAGE_PATTERN.matcher(input);
		assertTrue(matcher.matches());
	}

	public void isValidIPV4Matcher(final String input) {
		Matcher matcher = EMAIL_PATTERN.matcher(input);
		assertTrue(matcher.matches());
	}

	public void isValidEmailMatcher(final String input) {
		Matcher matcher = EMAIL_PATTERN.matcher(input);
		assertTrue(matcher.matches());
	}

}