package infrastructure.interactor;

import io.github.bonigarcia.wdm.config.WebDriverManagerException;

public enum Type {

	SCENARIO, GIVEN, WHEN, THEN, AND;

	@Override
	public String toString() {
		switch (this) {
		case SCENARIO:
			return "Scenario_ Description";
		case GIVEN:
			return "Given_ Description";
		case WHEN:
			return "When_ Description";
		case THEN:
			return "Then_ Description";
		case AND:
			return "And_ Description";
		default:
			throw new WebDriverManagerException("Invalid Page Object Type: " + this.name());
		}
	}

}