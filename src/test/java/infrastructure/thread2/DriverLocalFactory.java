package infrastructure.thread2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import infrastructure.thread.OptionsManager;

public class DriverLocalFactory {

	static WebDriver createInstance(String environment, String width, String height, String agent) {

		WebDriver driver = null;

		if (environment.toLowerCase().contains("firefox")) {
			driver = new FirefoxDriver(OptionsManager.getFirefoxOptions());
			return driver;
		}

		if (environment.toLowerCase().contains("chrome")) {
//			driver = new ChromeDriver(OptionsManager.getChromeOptions(width, height, agent));
			return driver;
		}

		return driver;
	}

}