package infrastructure.thread2;

import org.testng.annotations.Test;

public class ThreadTest {

	@Test
	public void testMethod1() {
		invokeBrowser("http://www.ndtv.com");
	}

	@Test
	public void testMethod2() {
		invokeBrowser("http://www.facebook.com");

	}

	private void invokeBrowser(String url) {
		System.out.println("Thread id = " + Thread.currentThread().getId());
		System.out.println("Hashcode of webDriver instance = " + DriverLocalManager.getDriver().hashCode());
		DriverLocalManager.getDriver().get(url);
	}

}