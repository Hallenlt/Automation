package infrastructure.thread2;

import org.openqa.selenium.WebDriver;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class DriverListener implements IInvokedMethodListener {

	@Override
	public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {

		if (method.isTestMethod()) {
			String environment = method.getTestMethod().getXmlTest().getLocalParameters().get("environment");
			String width = method.getTestMethod().getXmlTest().getLocalParameters().get("width");
			String height = method.getTestMethod().getXmlTest().getLocalParameters().get("height");
			String agent = method.getTestMethod().getXmlTest().getLocalParameters().get("environment");
			WebDriver driver = DriverLocalFactory.createInstance(environment, width, height, agent);
			DriverLocalManager.setWebDriver(driver);
		}
	}

	@Override
	public void afterInvocation(IInvokedMethod method, ITestResult testResult) {

		if (method.isTestMethod()) {
			WebDriver driver = DriverLocalManager.getDriver();
			if (driver != null) {
				driver.quit();
			}
		}
	}

}