package infrastructure.appium;

import org.openqa.selenium.support.PageFactory;

import infrastructure.thread.CentralBaseProcessor;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class AndroidBaseCore extends CentralBaseProcessor {

	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) appDriver();

	public AndroidBaseCore(AndroidDriver<MobileElement> driver) {
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		// TODO Auto-generated constructor stub
	}

	protected void openNotifications() {
		driver.openNotifications();
	}

	protected void getLogcatClient() {
		driver.getLogcatClient();
	}

	protected void endTestCoverage(String intent, String path) {
		driver.endTestCoverage(intent, path);
	}

	protected void toggleLocationServices() {
		driver.toggleLocationServices();
	}

	protected void currentActivity() {
		driver.currentActivity();
	}

	protected void getBatteryInfo() {
		driver.getBatteryInfo();
	}

}