package infrastructure.reporter;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

public class WebEventListener implements WebDriverEventListener {

	@Override
	public void beforeAlertAccept(WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Alert Accept " + " \t | " + driver);
	}

	@Override
	public void afterAlertAccept(WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Alert Accept " + " \t | " + driver);
	}

	@Override
	public void afterAlertDismiss(WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Alert Dismiss " + " \t | " + driver);
	}

	@Override
	public void beforeAlertDismiss(WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Alert Dismiss " + " \t | " + driver);
	}

	@Override
	public void beforeNavigateRefresh(WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Navigate Refresh " + " \t | " + driver);
	}

	@Override
	public void afterNavigateRefresh(WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Navigate Refresh " + " \t | " + driver);
	}

	@Override
	public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Change Value Of \t | " + element + " | " + keysToSend + " \t | " + driver);
	}

	@Override
	public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Change Value Of \t | " + element + " | " + keysToSend + " \t | " + driver);
	}

	@Override
	public void beforeSwitchToWindow(String windowName, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Switch To Window \t | " + windowName + " \t | " + driver);
	}

	@Override
	public void afterSwitchToWindow(String windowName, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Switch To Window \t | " + windowName + " \t | " + driver);
	}

	@Override
	public <X> void beforeGetScreenshotAs(OutputType<X> target) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Get Screenshot As \t | " + target + " | ");
	}

	@Override
	public <X> void afterGetScreenshotAs(OutputType<X> target, X screenshot) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Get Screenshot As \t | " + target + " \t | " + screenshot);
	}

	@Override
	public void beforeGetText(WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Get Text \t | " + element + " \t | " + driver);
	}

	@Override
	public void afterGetText(WebElement element, WebDriver driver, String text) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Get Text \t | " + element + " | " + text + " \t | " + driver);
	}

	@Override
	public void beforeNavigateTo(String url, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Navigate To \t | " + url + " \t | " + driver);
	}

	@Override
	public void afterNavigateTo(String url, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Navigate To \t | " + url + " \t | " + driver);
	}

	@Override
	public void beforeNavigateBack(WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Navigate Back \t | " + driver + " \t | " + driver);
	}

	@Override
	public void afterNavigateBack(WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Navigate Back " + " \t | " + driver);
	}

	@Override
	public void beforeNavigateForward(WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Navigate Forward " + " \t | " + driver);
	}

	@Override
	public void afterNavigateForward(WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Navigate Forward " + " \t | " + driver);
	}

	@Override
	public void beforeFindBy(By by, WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Find By \t | " + by + " | " + element + " \t | " + driver);
	}

	@Override
	public void afterFindBy(By by, WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Find By \t | " + by + " | " + element + " \t | " + driver);
	}

	@Override
	public void beforeClickOn(WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Click On \t | " + element + " \t | " + driver);
	}

	@Override
	public void afterClickOn(WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Click On \t | " + element + " \t | " + driver);
	}

	@Override
	public void beforeScript(String script, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t Before Script \t | " + script + " \t | " + driver);
	}

	@Override
	public void afterScript(String script, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t After Script \t | " + script + " \t | " + driver);
	}

	@Override
	public void onException(Throwable throwable, WebDriver driver) {
		// TODO Auto-generated method stub
		System.out.println("[INFO] - Executed | \t On Exception \t | " + throwable + " \t | " + driver);
	}

}