package infrastructure.reporter;

import java.util.HashMap;
import java.util.Map;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import infrastructure.root.CommonLibrariesCore;

public class ExtentTestManager extends CommonLibrariesCore {

	static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();
	static ExtentReports extentReports = ExtentManager.getInstance();

	public static synchronized ExtentTest get() {
		return (ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId()));
	}

	public static synchronized void endTest() {
		extentReports.flush();
	}

	public static synchronized ExtentTest startTest(String testName) {
		return startTest(testName, "Description");
	}

	public static synchronized ExtentTest startTest(String testName, String desc) {

		ExtentTest extentTest = extentReports.createTest(testName);
		extentTestMap.put((int) (long) (Thread.currentThread().getId()), extentTest);
		return extentTest;
	}

}