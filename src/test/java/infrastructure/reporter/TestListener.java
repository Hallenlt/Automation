package infrastructure.reporter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.io.FileHandler;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.Markup;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;
import infrastructure.root.CommonFileCore;
import infrastructure.thread.CentralBaseProcessor;

public class TestListener extends CentralBaseProcessor implements ITestListener {

	@Override
	public void onStart(ITestContext context) {

		System.out.println("*** Test Suite " + context.getName() + " started ***");

//		try {
//			FileUtils.deleteDirectory(new File(EVIDENCES_REPORT_PATH));
//			Path path = Paths.get(EVIDENCES_REPORT_PATH);
//			Files.createDirectories(path);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	@Override
	public void onTestStart(ITestResult result) {

		System.out.println(("*** Running test method " + result.getMethod().getMethodName() + "..."));

		ExtentTestManager.startTest(result.getMethod().getMethodName());

//		try {
//			recorder = new ATUTestRecorder(EVIDENCES_REPORT_PATH, "filename", false);
//			if (recorder != null)
//				recorder.start();
//		} catch (ATUTestRecorderException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	@Override
	public void onTestSuccess(ITestResult result) {

		System.out.println("*** Executed " + result.getMethod().getMethodName() + " test successfully...");
		ExtentTestManager.get().log(Status.PASS, "Test passed");

//		if (recordedFile != null)
//			recordedFile.delete();
	}

	@Override
	public void onFinish(ITestContext context) {

		System.out.println(("*** Test Suite " + context.getName() + " ending ***"));

		ExtentTestManager.endTest();
		ExtentManager.getInstance().flush();

		try {
			if (recorder != null)
				recorder.stop();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onTestFailure(ITestResult result) {

		System.out.println("*** Test execution " + result.getMethod().getMethodName() + " failed...");
		ExtentTestManager.get().log(Status.FAIL, "Test Failed: " + result.getThrowable());

		// screenshot tag in the report

		String targetLocation = null;
		String testClassName = result.getInstanceName().trim();
		String screenShotName = "FAILURE_" + result.getName().toString().trim() + "_" + datetimeformat.format(calendar.getTime()) + ".png";
		String fileSeperator = System.getProperty("file.separator");
		String reportsPath = System.getProperty("user.dir") + fileSeperator + "TestReport" + fileSeperator + "evidences";

		ExtentTestManager.get().log(Status.INFO, "Screen shots reports path - " + reportsPath);

		File file = new File(reportsPath + fileSeperator + testClassName);
		if (!file.exists()) {

			if (file.mkdirs())
				ExtentTestManager.get().log(Status.INFO, "Directory: " + file.getAbsolutePath() + " is created!");
			else
				ExtentTestManager.get().log(Status.INFO, "Failed to create directory: " + file.getAbsolutePath());
		}

		File screenshotFile = ((TakesScreenshot) driver()).getScreenshotAs(OutputType.FILE);
		targetLocation = reportsPath + fileSeperator + testClassName + fileSeperator + screenShotName;

		File targetFile = new File(targetLocation);
		ExtentTestManager.get().log(Status.INFO, "Screen shot file location - " + screenshotFile.getAbsolutePath());
		ExtentTestManager.get().log(Status.INFO, "Target File location - " + targetFile.getAbsolutePath());

		try {
			FileHandler.copy(screenshotFile, targetFile);
			ExtentTestManager.get().error("Screenshot", MediaEntityBuilder.createScreenCaptureFromPath(targetLocation).build());
//			ExtentTestManager.get().log(Status.ERROR, (Markup) ExtentTestManager.get().addScreenCaptureFromPath(targetLocation));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		if (result.getStatus() == ITestResult.FAILURE) {
//
//			String failedResult = "FAILURE_" + result.getName() + "_" + datetimeformat.format(calendar.getTime());
//			String outputRecorderPath = null;
//
//			if (appDriver != null) {
//
//				String screenShotPath = appium.capture(failedResult);
//				logger.log(LogStatus.WARNING, logger.addScreenCapture(screenShotPath));
//				outputRecorderPath = CommonFileCore.moveFile(INPUT_RECORDER_PATH, ".\\evidences\\", failedResult + ".mov");
//
//			} else if (driver != null) {
//
//				String screenShotPath = selenium.capture(failedResult);
//				logger.log(LogStatus.WARNING, logger.addScreenCapture(screenShotPath));
//				outputRecorderPath = CommonFileCore.moveFile(INPUT_RECORDER_PATH, ".\\evidences\\", failedResult + ".mov");
//			}
//
//			if (outputRecorderPath != null) {
//
//				logger.log(LogStatus.WARNING, "<a href='" + ".\\evidences\\" + outputRecorderPath + "'><span class='label info'>Download Video Log</span></a>");
//				logger.log(LogStatus.WARNING, logger.addScreencast(outputRecorderPath));
//			}
//			logger.log(LogStatus.FAIL, result.getThrowable());
//
//		} else if (result.getStatus() == ITestResult.SKIP) {
//
//			if (recordedFile != null) {
//				recordedFile.delete();
//			}
//			logger.log(LogStatus.SKIP, result.getThrowable());
//
//		} else {
//
//			if (recordedFile != null)
//				recordedFile.delete();
//			else if (result.getStatus() == ITestResult.SUCCESS) {
//
//			}
//		}

		// video tag in the report

//		String failedResult = "FAILURE_" + result.getName().toString().trim() + "_" + datetimeformat.format(calendar.getTime());
//		String outputRecorderPath = null;
//
//		outputRecorderPath = CommonFileCore.moveFile(INPUT_RECORDER_PATH, EVIDENCES_REPORT_PATH, failedResult + ".mov");
//
//		if (outputRecorderPath != null) {
//
//			ExtentTestManager.get().log(Status.ERROR, "<a href='" + EVIDENCES_REPORT_PATH + outputRecorderPath + "'><span class='label info'>Download Video Log</span></a>");
//			try {
//				ExtentTestManager.get().log(Status.ERROR, (Markup) ExtentTestManager.get().addScreencastFromPath(outputRecorderPath));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
	}

	@Override
	public void onTestSkipped(ITestResult result) {

		System.out.println("*** Test " + result.getMethod().getMethodName() + " skipped...");
		ExtentTestManager.get().log(Status.SKIP, "Test Skipped: " + result.getThrowable());

		if (recordedFile != null)
			recordedFile.delete();

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

		System.out.println("*** Test failed but within percentage % " + result.getMethod().getMethodName());
	}

}
