package infrastructure.performance;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.collections.HashTree;

public class JmeterJMX {

	public void main() throws Exception {
		
		// Initialize Properties, logging, locale, etc.
		String jmeterProperties = "C:\\apache-jmeter-5.2.1\\bin\\jmeter.properties";
		String jmeterHome = "C:\\apache-jmeter-5.2.1";
				
		// JMeter Engine
		StandardJMeterEngine jmeter = new StandardJMeterEngine();

		// Initialize Properties, logging, locale, etc.
		JMeterUtils.loadJMeterProperties(jmeterProperties);
		JMeterUtils.setJMeterHome(jmeterHome);
		JMeterUtils.initLogging();// you can comment this line out to see extra log messages of i.e. DEBUG level
		JMeterUtils.initLocale();

		// Initialize JMeter SaveService
		SaveService.loadProperties();

		// Load existing .jmx Test Plan
		File file = new File("C:\\apache-jmeter-5.2.1\\example.jmx");
		HashTree testPlanTree = SaveService.loadTree(file);

		// Run JMeter Test
		jmeter.configure(testPlanTree);
		jmeter.run();

		System.out.println("Jmeter Report.");
	}

}
