package infrastructure.thirdparty;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.SocketException;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import infrastructure.root.CommonDatetimeCore;
import infrastructure.selenium.SeleniumBaseCore;
import infrastructure.thread.CentralBaseProcessor;

public class FTPIngestionCore extends CentralBaseProcessor {

	// Creating FTPClient instance
	FTPClient ftp = null;
	
	public FTPIngestionCore(WebDriver driver) {
		this.driver = driver;
	}

	public FTPIngestionCore(String host, int port, String username, String password) {

		Reporter.log("\n [INFO] - Executed | Connecting To The FTP Server: " + host + " :: " + port + " |", true);
		selenium = new SeleniumBaseCore(driver());
		CommonDatetimeCore.getDatetime("today");
		ftp = new FTPClient();
		ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
		int reply;
		try {
			ftp.connect(host, port);
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
			}
			ftp.login(username, password);
			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			ftp.enterLocalPassiveMode();
			APP_LOGS("Connecting To The FTP Server: " + host + " :: " + port);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			APP_LOGS_FAIL(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			APP_LOGS_FAIL(e.getMessage());
		}
	}

	public void ingestFTPFile(String departure, String fileName, String directory) {

		InputStream input = null;
		try {
			input = new FileInputStream(new File(departure));
			this.ftp.storeFile(directory + fileName, input);
			APP_LOGS("FTP File Uploaded Successfully: " + fileName);
			if (input != null) {
				input.close();
			}
		} catch (IOException e) {
			APP_LOGS_FAIL(e.getMessage());
		}
	}

	public void assertListFTPFiles(String directory, String fileName) {

		FTPFile[] files;
		try {
			files = this.ftp.listFiles(directory);
			for (FTPFile file : files) {
				String details = file.getName();
				APP_LOGS("Get File Name: " + details);
				if (details.equals(fileName)) {
					APP_LOGS("Asserted List FTP Files: " + directory + " --> Contains: " + fileName);
				} else {
					APP_LOGS_FAIL("Asserted List FTP Files: " + directory + " --> Contains: " + fileName);
					assertTrue(details.equals(fileName));
				}
			}
		} catch (IOException e) {
			APP_LOGS_FAIL(e.getMessage());
		}
	}

	public FTPIngestionCore disconnect() {

		if (this.ftp.isConnected()) {
			try {
				this.ftp.logout();
				this.ftp.disconnect();
			} catch (IOException e) {
				APP_LOGS_FAIL(e.getMessage());
			}
		}
		return this;
	}

}