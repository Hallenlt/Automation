package infrastructure.thirdparty;

import static org.testng.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import infrastructure.thread.CentralBaseProcessor;

public class PDFExtractionCore extends CentralBaseProcessor {

	PDFTextStripper pdfStripper = null;
	PDDocument pdDoc = null;
	COSDocument cosDoc = null;
	String parsedText = null;

	// extractPDFContent(driver().getCurrentUrl(), ""));
	// extractPDFContent("D:/Paynetsbicardbill.pdf", ""));

	public void extractPDFContent(String path, String reqText) {

		try {
//	    File file = new File(path);
//	    PDFParser parser = new PDFParser((RandomAccessRead) new FileInputStream(file));
			URL url = new URL(path);
			BufferedInputStream file = new BufferedInputStream(url.openStream());
			PDFParser parser = new PDFParser((RandomAccessRead) file);
			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);
			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			APP_LOGS_FAIL(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			APP_LOGS_FAIL(e.getMessage());
			try {
				if (cosDoc != null) {
					cosDoc.close();
				}
				if (pdDoc != null) {
					pdDoc.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
				e2.printStackTrace();
			}
		}
		if (parsedText.contains(reqText)) {
			APP_LOGS("PDF Contents Are The Same: " + parsedText + " :: " + reqText);
		} else {
			APP_LOGS_FAIL("PDF Contents Are Not The Same: " + parsedText + " :: " + reqText);
			assertTrue(parsedText.contains(reqText));
		}
	}

}
