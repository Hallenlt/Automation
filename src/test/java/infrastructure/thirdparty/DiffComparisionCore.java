package infrastructure.thirdparty;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.text.diff.StringsComparator;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.CommandVisitorImpl;

public class DiffComparisionCore extends CommonLibrariesCore {

	public void compareFileDocument(String departure1, String departure2) {

		try {
			File file1 = new File(departure1);
			File file2 = new File(departure2);

			if (FileUtils.contentEquals(file1, file2)) {
				APP_LOGS("Files Document Are The Same: " + departure1 + " :: " + departure2);
			} else {
				APP_LOGS_FAIL("Files Document Are Not The Same: " + departure1 + " :: " + departure2);
				assertTrue(FileUtils.contentEquals(file1, file2));
			}
		} catch (IOException e) {
			// TODO: handle exception
			APP_LOGS_FAIL(e.getMessage());
		}
	}

	public void compareVisuallyFileDocument(String parture1, String parture2) {

		LineIterator file1 = null;
		LineIterator file2 = null;
		try {
			file1 = FileUtils.lineIterator(new File(parture1), "utf-8");
			file2 = FileUtils.lineIterator(new File(parture2), "utf-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			APP_LOGS_FAIL(e.getMessage());
		}

//		// Initialize visitor.
//		CommandVisitorImplement fileCommandsVisitor = new CommandVisitorImplement();
//
//		// Read file line by line so that comparison can be done line by line.
//		while (file1.hasNext() || file2.hasNext()) {
//			/*
//			 * In case both files have different number of lines, fill in with empty
//			 * strings. Also append newline char at end so next line comparison moves to
//			 * next line.
//			 */
//			String left = (file1.hasNext() ? file1.nextLine() : "") + "\n";
//			String right = (file2.hasNext() ? file2.nextLine() : "") + "\n";
//
//			// Prepare diff comparator with lines from both files.
//			StringsComparator comparator = new StringsComparator(left, right);
//
//			if (comparator.getScript().getLCSLength() > (Integer.max(left.length(), right.length()) * 0.4)) {
//				/*
//				 * If both lines have at least 40% commonality then only compare with each other
//				 * so that they are aligned with each other in final diff HTML.
//				 */
//				comparator.getScript().visit(fileCommandsVisitor);
//			} else {
//				/*
//				 * If both lines do not have 40% commanlity then compare each with empty line so
//				 * that they are not aligned to each other in final diff instead they show up on
//				 * separate lines.
//				 */
//				StringsComparator leftComparator = new StringsComparator(left, "\n");
//				leftComparator.getScript().visit(fileCommandsVisitor);
//				StringsComparator rightComparator = new StringsComparator("\n", right);
//				rightComparator.getScript().visit(fileCommandsVisitor);
//			}
//		}
//		fileCommandsVisitor.generateHTML();
	}

}