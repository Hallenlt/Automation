package infrastructure.thirdparty;

import java.net.URI;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.BasicVotes;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;

public class JiraClientManager {

	JiraRestClient restClient;
	String username;
	String password;
	String url;

	public JiraClientManager(String username, String password, String url) {

		this.username = username;
		this.password = password;
		this.url = url;
		this.restClient = getJiraRestClient();
	}

	private JiraRestClient getJiraRestClient() {
		return new AsynchronousJiraRestClientFactory().createWithBasicHttpAuthentication(getJiraURI(), this.username, this.password);
	}

	private URI getJiraURI() {
		return URI.create(this.url);
	}

	public String createIssue(String projectKey, Long issueType, String issueSummary) {

		IssueRestClient issueClient = restClient.getIssueClient();
		IssueInput issueInput = new IssueInputBuilder(projectKey, issueType, issueSummary).build();
		return issueClient.createIssue(issueInput).claim().getKey();
	}

	public JiraClientManager addCommentIssue(Issue issue, String body) {
		restClient.getIssueClient().addComment(issue.getCommentsUri(), Comment.valueOf(body));
		return this;
	}

	public JiraClientManager updateIssue(String issueKey, String summary) {
		IssueInput input = new IssueInputBuilder().setDescription(summary).build();
		restClient.getIssueClient().updateIssue(issueKey, input).claim();
		return this;
	}

	public Issue getIssue(String issueKey) {
		return restClient.getIssueClient().getIssue(issueKey).claim();
	}

	public JiraClientManager voteIssue(Issue issue) {
		restClient.getIssueClient().vote(issue.getVotesUri()).claim();
		return this;
	}

	public int getTotalVotesCount(String issueKey) {
		BasicVotes votes = getIssue(issueKey).getVotes();
		return votes == null ? 0 : votes.getVotes();
	}

	public JiraClientManager deleteIssue(String issueKey, boolean subtasks) {
		restClient.getIssueClient().deleteIssue(issueKey, subtasks).claim();
		return this;
	}

}