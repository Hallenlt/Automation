package infrastructure.thirdparty;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.RectangleSize;
import com.applitools.eyes.StdoutLogHandler;
import com.applitools.eyes.TestResultsSummary;
import com.applitools.eyes.images.Eyes;
import com.applitools.eyes.selenium.BrowserType;
import com.applitools.eyes.selenium.Configuration;
import com.applitools.eyes.selenium.fluent.Target;
import com.applitools.eyes.visualgrid.model.DeviceName;
import com.applitools.eyes.visualgrid.model.ScreenOrientation;
import com.applitools.eyes.visualgrid.services.VisualGridRunner;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.thread.CentralBaseProcessor;
import infrastructure.utility.DirectoryUtils;

public class ApplitoolsVisualEyesCore extends CentralBaseProcessor {

	String STORAGE_INPUT = DirectoryUtils.RESOURCE_PATH + "\\sikuli-input\\";
	String STORAGE_OUTPUT = DirectoryUtils.RESOURCE_PATH + "\\sikuli-output\\";

	public void visualImageByApplitools(String batchInfo, String apiKey, String os, String browser, String appName, String testName, int width, int height, String windowTag,
			String imageTag) {

		BufferedImage img;
		BatchInfo batch;
		batch = new BatchInfo(batchInfo);

		// Initialize the Runner for your test.
		VisualGridRunner runner = new VisualGridRunner(1);
//		EyesRunner runner = new ClassicRunner();

		Eyes eyes = new Eyes();
		eyes.setApiKey(apiKey);

		// Set your personal Applitols API Key from your environment variables.
//		eyes.setApiKey(System.getenv("dFUqVGL122fgjBeKpEKAqSFaZhtBMOBDTdm8xq97chA110"));
		eyes.setBatch(batch);

		// Define the OS and hosting application to identify the baseline.
		eyes.setHostOS(os);
		eyes.setHostApp(browser);

		// Define the OS and hosting application to identify the baseline
		eyes.setAppEnvironment(os, browser);

		// Initialize eyes Configuration
		Configuration config = new Configuration();

		// create a new batch info instance and set it to the configuration
		config.setBatch(new BatchInfo("Ultrafast Batch"));

		// Add browsers with different viewports
		config.addBrowser(800, 600, BrowserType.CHROME);
		config.addBrowser(700, 500, BrowserType.FIREFOX);
		config.addBrowser(1600, 1200, BrowserType.IE_11);
		config.addBrowser(1024, 768, BrowserType.EDGE_CHROMIUM);
		config.addBrowser(800, 600, BrowserType.SAFARI);

		// Add mobile emulation devices in Portrait mode
		config.addDeviceEmulation(DeviceName.iPhone_X, ScreenOrientation.PORTRAIT);
		config.addDeviceEmulation(DeviceName.Pixel_2, ScreenOrientation.PORTRAIT);

		// Set the configuration object to eyes
		eyes.setConfiguration(config);

		eyes.setLogHandler(new StdoutLogHandler(true));

		try {
			eyes.open(appName, testName, new RectangleSize(width, height));
			img = ImageIO.read(new File(STORAGE_INPUT + "ids-finder.PNG"));

			eyes.checkWindow(img, windowTag);
			eyes.checkImage(img, imageTag);

			eyes.check(Target.window().fully().getName(), null);

			// Finish the test
			eyes.close();

		} catch (IOException e) {
			e.printStackTrace();

		} finally {
			eyes.abortAsync();
			// If the test was aborted before eyes.close was called, ends the test as
			// aborted.
			eyes.abortIfNotClosed();
		}
		// Wait and collect all test results
		TestResultsSummary allTestResults = runner.getAllTestResults();
		// Print results
		APP_LOGS(allTestResults.toString());
	}

}
