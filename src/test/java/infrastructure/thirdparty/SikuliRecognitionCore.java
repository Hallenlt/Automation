package infrastructure.thirdparty;

import static org.testng.Assert.fail;

import java.io.IOException;

import org.sikuli.script.Finder;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.testng.Reporter;

import infrastructure.selenium.SeleniumBaseCore;
import infrastructure.thread.CentralBaseProcessor;
import infrastructure.utility.DirectoryUtils;

public class SikuliRecognitionCore extends CentralBaseProcessor {

	static float RATIO = (float) 0.01;

	public void matchScreenImageRecognition(String patternImage, String finderImage) {

		Reporter.log("\n [INFO] - Executed | Screen Image Recognition Matching: " + patternImage + " |", true);
		selenium = new SeleniumBaseCore(driver());

		selenium.capture(DirectoryUtils.RESOURCE_PATH + "\\sikuli-input\\", finderImage);
		Pattern pattern = new Pattern(DirectoryUtils.RESOURCE_PATH + "\\sikuli-output\\" + patternImage);
		try {
			Thread.sleep(1000);
			Finder finder = new Finder(DirectoryUtils.RESOURCE_PATH + "\\sikuli-input\\" + finderImage);
			finder.find(pattern.similar(RATIO));

			if (finder.hasNext()) {

				Match match = new Match(finder.next());
				APP_LOGS("Screen Image Recognition Matching: " + (match.getScore()) * 100 + " % --> " + patternImage);
				finder.destroy();

			} else {
				APP_LOGS_FAIL("No Screen Image Recognition Matching Found " + patternImage);
				fail();
			}
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}