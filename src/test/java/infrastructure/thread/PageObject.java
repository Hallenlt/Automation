package infrastructure.thread;

import org.openqa.selenium.WebDriver;

import framework.dataDriven.ExcelConfigReader;
import infrastructure.selenium.SeleniumBaseCore;
import infrastructure.utility.DirectoryUtils;

public class PageObject extends SeleniumBaseCore {

	public PageObject(WebDriver driver) {
		super(driver);
	}
	
	final int MONDAY = 4;
	final int TUESDAY = 5;
	final int WEDNESDAY = 6;

	String[] expectedList = { "dfhfgh", "hsfhdgf", "dfghewr", "ahdgdfg", "dfhfgh", "hsfhdgf", "dfghewr", "ahdgdfg", "dfhfgh", "hsfhdgf", "dfghewr", "ahdgdfg", "dfhfgh", "hsfhdgf",
			"dfghewr", "ahdgdfg", "dfhfgh", "hsfhdgf" };

	public String[] getExpectedList() {
		return expectedList.clone();
	}

	public PageObject assertListData_Newspapers() {
		assertIsListItemInDataGrid("//*[@class='MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-2']", ".//p[1]", getExpectedList());
		return this;
	}

	public PageObject navigateTo_Publications() {
		clickHref("//*[@href='/explore/kiosk/publications']");
		return this;
	}

	public PageObject insert_Password(String value) {
//		sendKeys("//*[@id=\"password\"]", "Belga2020");
		sendKeys("//*[@id=\"password\"]", value);
		return this;
	}

	public PageObject clickOn_Login() {
		click("//*[@id=\"kc-login\"]");
		return this;
	}

	public PageObject insert_Email(String value) {
//		sendKeys("*//input[@name='username']", "helpdesk@idsolutions.com.vn");
		sendKeys("//*[@id=\"username\"]", value);
		return this;
	}

	public PageObject navigateTo_Login() {
		getURL("https://web.belga.press/authentication");
		return this;
	}

}