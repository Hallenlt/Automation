package infrastructure.thread;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;

import com.automation.remarks.testng.GridInfoExtractor;

import infrastructure.utility.EnvironmentUtils;

public class CapabilityFactory {

	public static DesiredCapabilities capabilities;

	public static Capabilities getChromeCapabilities(ChromeOptions options) {
		
		Reporter.log("\t [INFO] - Executed | " + "EXECUTING ON CHROME" + " |", true);

		capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		capabilities.setBrowserName(BrowserType.CHROME);
		capabilities.setPlatform(Platform.WINDOWS);
		capabilities.setJavascriptEnabled(true);

		return capabilities;
	}

	public static Capabilities getFirefoxCapabilities(FirefoxOptions options) {

		Reporter.log("\t [INFO] - Executed | " + "EXECUTING ON FIREFOX" + " |", true);
		
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setBrowserName(BrowserType.FIREFOX);
		capabilities.setPlatform(Platform.WINDOWS);
		capabilities.setJavascriptEnabled(true);
		capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);

		return capabilities;
	}
	
	public static Capabilities getSafariCapabilities() {
		
		Reporter.log("\t" + "[INFO] - Executed | " + "EXECUTING ON SAFARI" + " |", true);
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("build", "v1.0");
		capabilities.setCapability("name", "PIM");
		capabilities.setCapability("platform", "macOS Mojave");
		capabilities.setCapability("browserName", "Safari");
		capabilities.setCapability("version", "12.0");
		capabilities.setCapability("resolution", "1920x1080");
		capabilities.setCapability("console", true);
		capabilities.setCapability("network", true);
		capabilities.setCapability("visual", true);
		capabilities.setCapability("timezone", "UTC+07:00");
		capabilities.setCapability("geoLocation", "VN");
		capabilities.setCapability("safari.popups", true);
		capabilities.setCapability("safari.cookies", true);
		capabilities.setCapability("video", true);
		capabilities.setCapability("recordVideo", true);
		
		return capabilities;
	}
	
	public static Capabilities getAndroidCapabilities(String device, int udid, String platform, String version) {
		
		Reporter.log("\t [INFO] - Executed | " + "EXECUTING ON ANDROID" + " |", true);
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", device);
		capabilities.setCapability("udid", udid);
		capabilities.setCapability("platformName", platform);
		capabilities.setCapability("platformVersion", version);
		capabilities.setCapability("noReset", "true");
		capabilities.setCapability("appPackage", EnvironmentUtils.ANDROID_APP_PACKAGE);
		capabilities.setCapability("appActivity", EnvironmentUtils.ANDROID_APP_ACTIVITY);
		
		return capabilities;
	}

}