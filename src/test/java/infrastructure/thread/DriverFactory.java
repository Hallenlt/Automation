package infrastructure.thread;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ThreadGuard;

import infrastructure.root.CommonLibrariesCore;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public class DriverFactory extends CommonLibrariesCore {

	protected final String EVIDENCES_REPORT_PATH = System.getProperty("user.dir") + "\\TestReport\\evidences\\";
	protected final String INPUT_RECORDER_PATH = EVIDENCES_REPORT_PATH + "filename.mov";
	protected File recordedFile = new File(INPUT_RECORDER_PATH);

	String browser;
	protected WebDriver driver;
	protected RemoteWebDriver remoteDriver;
	protected AppiumDriver<MobileElement> appDriver;
	protected AppiumDriverLocalService appService = AppiumDriverLocalService.buildDefaultService();
	protected AppiumDriverLocalService appLocalService = AppiumDriverLocalService.buildDefaultService();

	public static ThreadLocal<WebDriver> threadDriver = new ThreadLocal<WebDriver>();
	public static ThreadLocal<RemoteWebDriver> rmThreadDriver = new ThreadLocal<RemoteWebDriver>();
	public static ThreadLocal<AppiumDriver<MobileElement>> appThreadDriver = new ThreadLocal<AppiumDriver<MobileElement>>();

	public RemoteWebDriver remoteDriver() {
		return (RemoteWebDriver) ThreadGuard.protect(rmThreadDriver.get());
	}

	public AppiumDriver<MobileElement> appDriver() {
		return (AppiumDriver<MobileElement>) ThreadGuard.protect(appThreadDriver.get());
	}

	public WebDriver driver() {
		return ThreadGuard.protect(threadDriver.get());
	}

	public void refreshDriver() {

		if (driver != null)
			driver.navigate().refresh();

		if (appDriver != null)
			appDriver.resetApp();

		if (remoteDriver != null)
			remoteDriver.navigate().refresh();
	}

	public void closeDriver() {

		if (driver != null)
			driver.close();

		if (appDriver != null)
			appDriver.launchApp();

		if (remoteDriver != null)
			remoteDriver.quit();

		if (appLocalService != null)
			appLocalService.stop();

//				if (clsfClient != null)
//					clsfClient.shutdown(); 
	}

	public void quitDriver() {
		
		driver().close();
		
//		if (driver != null) {
//			driver.quit();
//			driver = null;
//		}
//		if (appDriver != null) {
//			appDriver.closeApp();
//			appDriver = null;
//		}
//		if (remoteDriver != null) {
//			remoteDriver.quit();
//			remoteDriver = null;
//		}
//		if (appLocalService != null) {
//			appLocalService.stop();
//			appLocalService = null;
//		}

//			if (clsfClient != null)
//				clsfClient.shutdown(); 
	}

}