package infrastructure.thread;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import infrastructure.utility.DirectoryUtils;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;

public class OptionsManager extends CapabilityFactory {

	public static ChromeOptions getChromeOptions(int width, int height, String agent) {

		System.setProperty("webdriver.chrome.driver", DirectoryUtils.RESOURCE_PATH + "\\libs\\driver\\chromedriver.exe");
		
		Map<String, Object> deviceMetrics = new HashMap<>();
		deviceMetrics.put("width", width);
		deviceMetrics.put("height", height);
//		deviceMetrics.put("pixelRatio", 2);

		Map<String, Object> responsiveEmulator = new HashMap<>();
		responsiveEmulator.put("deviceMetrics", deviceMetrics);
		responsiveEmulator.put("userAgent", agent);

		Map<String, Object> chromeOptions = new HashMap<>();
		chromeOptions.put("mobileEmulation", responsiveEmulator);

		ChromeOptions options = new ChromeOptions();
		options.setHeadless(false);
		options.setAcceptInsecureCerts(true);
		
//		options.addArguments("--start-maximized");
//		options.addArguments("--ignore-certificate-errors");
//		options.addArguments("--disable-popup-blocking");
//		options.addArguments("--lang=locale-of-choice");
		options.addArguments("--incognito");
		options.setExperimentalOption("mobileEmulation", responsiveEmulator);
		
		// Automatically download pdf file
//		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
//		chromePrefs.put("plugins.always_open_pdf_externally", true);
//		options.setExperimentalOption("prefs", chromePrefs);
		
		getChromeCapabilities(options);

		WebDriverManager.getInstance(DriverManagerType.CHROME).setup();

		return options;
	}

	public static FirefoxOptions getFirefoxOptions() {

		System.setProperty("webdriver.gecko.driver", DirectoryUtils.RESOURCE_PATH + "\\libs\\driver\\geckodriver.exe");

		FirefoxProfile profile = new FirefoxProfile();
		profile.setAcceptUntrustedCertificates(true);
		profile.setAssumeUntrustedCertificateIssuer(false);
		profile.setPreference("network.proxy.type", 0);
		profile.setPreference("intl.accept_languages", "locale-of-choice");
		
		FirefoxOptions options = new FirefoxOptions();
		options.setHeadless(false);
		options.setCapability(FirefoxDriver.PROFILE, profile);
		options.addArguments("--start-maximized");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--disable-popup-blocking");
		options.addArguments("--incognito");
		options.addArguments("--lang=locale-of-choice");
		options.setProfile(profile);
		
		getFirefoxCapabilities(options);

		WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();

		return options;
	}
	
}
