package infrastructure.thread;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.automation.remarks.testng.UniversalVideoListener;
import com.automation.remarks.video.annotations.Video;

import framework.dataDriven.ExcelConfigReader;
import infrastructure.utility.DirectoryUtils;
import project.pim.center.SubTransferStator;

//@Listeners(UniversalVideoListener.class)
public class BelgaPress extends SubTransferStator {

//	@Video
	@Test(dataProvider = "printSchema")
//	@Test
	public void Belgapress(String data1, String data2) {

		object.navigateTo_Login().insert_Email("minh.vo@idsolutions.com.vn")
		
				.clickOn_Login().insert_Password("slsZ1XmB")

				.clickOn_Login().navigateTo_Publications().assertListData_Newspapers();
		
		System.out.println(data1 + " | " + data2);
	}

	@DataProvider(name = "printSchema")
	public Object[][] inputData() {

		ExcelConfigReader excel = new ExcelConfigReader(DirectoryUtils.DATA_DRIVEN_PATH + "Print Schema.xlsx");
		int rows = excel.getRowCount(0);

		Object[][] data = new Object[rows][2];

		for (int i = 0; i < rows; i++) {

			data[i][0] = excel.getData(0, i, 4);
//			data[i][1] = excel.getData(0, i, 4);
		}
		return data;
	}

}