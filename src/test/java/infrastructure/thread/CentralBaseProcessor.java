package infrastructure.thread;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.automation.remarks.testng.GridInfoExtractor;

import atu.testrecorder.exceptions.ATUTestRecorderException;
import infrastructure.reporter.ExtentManager;
import infrastructure.reporter.ExtentTestManager;
import infrastructure.selenium.SeleniumBaseCore;
import infrastructure.utility.EnvironmentUtils;
import infrastructure.validation.ProcessValidator;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class CentralBaseProcessor extends DriverFactory {

	public static List<DriverFactory> poolFactory = Collections.synchronizedList(new ArrayList<DriverFactory>());
	public static ThreadLocal<DriverFactory> threadFactory = new ThreadLocal<DriverFactory>();

	public SeleniumBaseCore selenium;
	public PageObject object;

	public @BeforeSuite(alwaysRun = true) void instantiate() throws IOException {

		threadFactory = new ThreadLocal<DriverFactory>() {
			@Override
			protected DriverFactory initialValue() {
				DriverFactory factory = new DriverFactory();
				poolFactory.add(factory);
				return factory;
			}
		};

		ExtentTestManager.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());

		Reporter.log("\t <--------------> TESTSUITE STARTING__ <--------------> \n", true);
	}

	public @BeforeMethod(alwaysRun = true) void Pretreatment(Method method) throws ATUTestRecorderException {

//		String suiteName = method.getAnnotation(Test.class).suiteName();
//		String testName = method.getAnnotation(Test.class).testName();
//		String description = method.getAnnotation(Test.class).description();
//		extentTest = ExtentTestManager.startTest(suiteName + " - " + testName + " - " + description);
	}

	@Parameters({ "environment", "width", "height", "agent", "port", "device", "udid", "platform", "version" })
	public @BeforeMethod(alwaysRun = true) void Setup(@Optional String environment, @Optional int width, @Optional int height, @Optional String agent, @Optional int port,
			@Optional String device, @Optional int udid, @Optional String platform, @Optional String version) throws IOException {

		if (environment.equalsIgnoreCase("unit")) {

			Reporter.log("\t [INFO] - Executed | " + "EXECUTING ON UNIT TEST" + " |", true);

		} else if (environment.equalsIgnoreCase("chrome")) {

			threadDriver.set(new ChromeDriver(OptionsManager.getChromeOptions(width, height, agent)));

//			DesiredCapabilities dc = new DesiredCapabilities();
//			dc.setCapability(CapabilityType.BROWSER_NAME, BrowserType.CHROME);
//			dc.setCapability(CapabilityType.PLATFORM_NAME, Platform.LINUX);
//			dc.setCapability("name", "AutomationTest");
//			dc.setCapability("build", "AutomationBuild");
//			dc.setCapability("recordVideo", true);
//
//			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//			ChromeOptions options = new ChromeOptions();
//			options.addArguments("lang=en_GB");
//			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//
//			threadDriver.set(new RemoteWebDriver(new URL(EnvironmentUtils.GRID_SERVER + port + "/wd/hub"), dc));
			
		} else if (environment.equalsIgnoreCase("firefox")) {

			threadDriver.set(new FirefoxDriver(OptionsManager.getFirefoxOptions()));
//			threadDriver.set(new RemoteWebDriver(new URL(EnvironmentUtils.GRID_SERVER + port + "/wd/hub"), OptionsManager.getFirefoxOptions()));

		} else if (environment.equalsIgnoreCase("safari")) {

			String username = "doanphungtriloc";
			String accesskey = "oBaW0WS7AK2a3PJ3V1IQboSQn11AYU7GtYGmrOCXnY28U1V5vc";
			String lambdaServer = "@hub.lambdatest.com/wd/hub";

			threadDriver.set(new RemoteWebDriver(new URL("https://" + username + ":" + accesskey + lambdaServer), CapabilityFactory.getSafariCapabilities()));
			String nodeIP = GridInfoExtractor.getNodeIp(new URL("https://" + username + ":" + accesskey + lambdaServer), rmThreadDriver.get().getSessionId().toString());
			System.setProperty("video.remote", "true");
			System.setProperty("remote.video.hub", nodeIP);

		} else if (environment.equalsIgnoreCase("Android")) {

			appThreadDriver.set(new AppiumDriver<MobileElement>(new URL(EnvironmentUtils.GRID_SERVER + port + "/wd/hub"),
					CapabilityFactory.getAndroidCapabilities(device, udid, platform, version)));
			
		} else {
			throw new IllegalArgumentException("\n\t\t <--------------> The environment Type Is Undefined! <-------------->");
		}

		driver().manage().window().maximize();
		driver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		driver().manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

		selenium = new SeleniumBaseCore(driver());
		object = new PageObject(driver());
	}

	@Parameters("environment")
	public @BeforeTest(alwaysRun = true) void StartExtentReport(@Optional String environment) {

//		ExtentTestManager.get().assignAuthor("QA / QC");
//		ExtentTestManager.get().assignCategory(getClass().getName());

//		extent = new ExtentReports(System.getProperty("user.dir") + ".\\ExtentReport.html", true);
//		extent.addSystemInfo("Project", "Product Information Management [PIM]");
//		extent.addSystemInfo("Host Name", PageURL.IP_CONFIG);
//		extent.addSystemInfo("Browser", environment);
//		extent.addSystemInfo("Time Zone", System.getProperty("user.timezone"));
//		extent.addSystemInfo("User Location", System.getProperty("user.country"));
//		extent.addSystemInfo("Role", "Administrator");
//		extent.loadConfig(new File(DirectoryUtils.RESOURCE_PATH + "\\config\\extent-config.xml"));
//		extent.getProjectName();
//		extent.getReportId();
	}

	public @AfterTest(alwaysRun = true) void EndExtentReport() {

		ExtentTestManager.endTest();
	}

	public @AfterMethod(alwaysRun = true) void PostProcessing(ITestResult result) throws IOException, NoSuchWindowException, InterruptedException {

		try {
//			for (DriverFactory factory : poolFactory) {
//				factory.refreshDriver();
//			}
		} finally {
			// TODO: handle finally clause
			Thread.sleep(500);
		}
		Reporter.log("\t <--------------> TESTCASE FINISHED! <--------------> \n", true);
	}

	public @AfterClass(alwaysRun = true) void teardown() {

		try {
//			clearWebStorage();

//			for (DriverFactory factory : poolFactory) {
//				factory.closeDriver();
//			}
		} catch (Exception e) {
			// TODO: handle exception
		}
//		process = runtime.exec("taskkill /F /IM chromedriver.exe /T");
//		process = runtime.exec("taskkill /F /IM chrome.exe /T");
//		process = runtime.exec("taskkill /F /IM firefox.exe /T");

		Reporter.log("\t <--------------> TESTCLASS FINISHED! <--------------> \n", true);
	}

	public @AfterSuite(alwaysRun = true) void terminate() {

		for (DriverFactory factory : poolFactory) {
			factory.quitDriver();
		}

		threadFactory.remove();
		threadDriver.remove();
//		appThreadDriver.remove();
//		rmThreadDriver.remove();

		ExtentManager.getInstance().flush();

		Reporter.log("\t <--------------> TESTSUITE FINISHING___ <--------------> \n", true);
	}

	public void clearWebStorage() {

		try {
			WebStorage webStorage = null;
			webStorage = (WebStorage) new Augmenter().augment(driver());

			Reporter.log("\n [INFO] - Executed | " + "Deleting Local Storage" + " |");
			LocalStorage localStorage = webStorage.getLocalStorage();
			localStorage.clear();

//			Reporter.log("\n [INFO] - Executed | " + "Deleting Session Storage" + " |");
//			SessionStorage sessionStorage = webStorage.getSessionStorage();
//			sessionStorage.clear();
//			
//			Reporter.log("\n [INFO] - Executed | " + "Deleting All Cookies" + " |");
//			driver().manage().deleteAllCookies();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}