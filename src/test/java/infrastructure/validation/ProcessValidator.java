package infrastructure.validation;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.selenium.SeleniumBaseCore;

public class ProcessValidator extends SeleniumBaseCore {

	public ProcessValidator(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	List<Object> inputData = new ArrayList<>();
	List<Object> outputData = new ArrayList<>();

	public List<Object> getInputData(String[] elementInputArray) {

		inputData.clear();

		String[] inputArr = elementInputArray;

		for (int i = 0; i < inputArr.length; i++) {

			if (inputArr[i] != null) {

				inputData.add(getTextAttribute(inputArr[i]));
				CommonLibrariesCore.APP_LOGS("Get Input Text Attribute Value: " + getTextAttribute(inputArr[i]) + " --> In Locator: " + inputArr[i] + " |");
			}
		}
		Reporter.log("\n [INFO] - Executed | ==================================== \n", true);

		return inputData;
	}

	public List<Object> getOutputData(String[] elementOutputArray) {

		outputData.clear();

		String[] outputArr = elementOutputArray;

		for (int i = 0; i < outputArr.length; i++) {

			if (outputArr[i] != null) {

				outputData.add(getTextAttribute(outputArr[i]));
				CommonLibrariesCore.APP_LOGS("Get Output Text Attribute Value: " + getTextAttribute(outputArr[i]) + " --> In Locator: " + outputArr[i] + " |");
			}
		}
		Reporter.log("\n [INFO] - Executed | ==================================== \n", true);

		return outputData;
	}

	public void assertData() {

		status = inputData.equals(outputData);
		if (status == true) {

			CommonLibrariesCore.APP_LOGS("\t TEXT STRING INPUT / OUTPUT DATA: \n\t" + inputData + " == \n\t" + outputData);
			CommonLibrariesCore.APP_LOGS("\t TOTAL LENGTH INPUT / OUTPUT DATA: " + inputData.toString().length() + " == " + outputData.toString().length());

		} else if (status == false) {

			CommonLibrariesCore.APP_LOGS_FAIL("\t TEXT STRING INPUT / OUTPUT DATA: \n\t" + inputData + " == \n\t" + outputData);
			CommonLibrariesCore.APP_LOGS_FAIL("\t TOTAL LENGTH INPUT / OUTPUT DATA: " + inputData.toString().length() + " == " + outputData.toString().length());
		}

		assertEquals(inputData, outputData);
		assertEquals(inputData.toString().length(), outputData.toString().length());
	}

}
