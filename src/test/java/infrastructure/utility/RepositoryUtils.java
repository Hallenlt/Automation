package infrastructure.utility;

public class RepositoryUtils {

	// String yesterday = common.getDatetime("yesterday");
	// String tomorrow = common.getDatetime("tomorrow");
	//
	// newage.input_startDate(yesterday);
	// newage.input_endDate(tomorrow);

	protected String ELEMENT_DEFINATION1 = "//button[starts-with(@id, 'save') and contains(@class,'publish')]";

	protected String ELEMENT_DEFINATION2 = "//span[contains(text(),'CONTENT')]";

	protected String ELEMENT_DEFINATION3 = "//*[text()='CONTENT']";

	protected String ELEMENT_DEFINATION4 = "//h2[contains(string(),'CONTENT')]";

	protected String ELEMENT_DEFINATION5 = "//div[contains(.,'CONTENT')]";

	protected String ELEMENT_DEFINATION6 = "(.//*[normalize-space(text()) and normalize-space(.)='CULTURE'])[1]/following::button[1]";

	protected String ELEMENT_DEFINATION7 = "//input[starts-with(@id, 'submit')]";

	protected String ELEMENT_DEFINATION8 = "//input[contains(@id, 'subscribe')]";

	protected String ELEMENT_DEFINATION9 = "//td[contains(text(),'CONTENT')]/following-sibling::td/descendant::div/label[contains(text(),'CONTENT')]/following-sibling::input";

}