package infrastructure.selenium;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.thread.CentralBaseProcessor;

public class ObjectMapCore extends CentralBaseProcessor {

	Properties properties = null;

	public ObjectMapCore(WebDriver driver) {
		this.driver = driver;
		// TODO Auto-generated constructor stub
	}

	public ObjectMapCore(String mapFile) {

		properties = new Properties();
		try {
			FileInputStream stream = new FileInputStream(mapFile);
			properties.load(stream);
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public By getLocator(String elementName) {
		// Read value using the logical name as Key
		String locator = properties.getProperty(elementName);
		// Split the value which contains locator type and locator value
		String locatorType = "";
		locatorType = locator.split(":")[0];
		String locatorValue = "";
		locatorValue = locator.split(":")[1];
		// Return a instance of By class based on type of locator

		if (locatorType.toLowerCase().equals("id"))
			return By.id(locatorValue);

		else if (locatorType.toLowerCase().equals("name") || (locatorValue.equals(elementName)))
			return By.name(locatorValue);

		else if ((locatorType.toLowerCase().equals("classname")) || (locatorValue.contains("-")))
			return By.className(locatorValue);

		else if ((locatorType.toLowerCase().equals("tagname"))
				|| (locatorValue.startsWith("ul") || (locatorValue.startsWith("li") || (locatorValue.startsWith("tr") || (locatorValue.startsWith("td")
						|| (locatorValue.startsWith("table")) || (locatorValue.startsWith("img")) || (locatorValue.startsWith("a")))))))
			return By.tagName(locatorValue);

		else if ((locatorType.toLowerCase().equals("xpath")) || (locatorValue.startsWith("//")))
			return By.xpath(locatorValue);

		else if ((locatorType.toLowerCase().equals("linktext")))
			return By.linkText(locatorValue);

		else if (locatorType.toLowerCase().equals("partiallinktext"))
			return By.partialLinkText(locatorValue);

		else if ((locatorType.toLowerCase().equals("cssselector")) || (locatorValue.contains(".") || (locatorValue.startsWith("#"))))
			return By.cssSelector(locatorValue);

		else
			try {
				throw new WebDriverException("Locator Type [" + locatorType + "] Not Defined.");
			} catch (WebDriverException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return null;
	}

}