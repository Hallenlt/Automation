package infrastructure.selenium;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ThreadGuard;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.root.CommonRandomCore;
import infrastructure.thread.CentralBaseProcessor;
import infrastructure.thread.DriverFactory;
import infrastructure.validation.ProcessValidator;

public class SeleniumBaseCore extends CentralBaseProcessor {

	public SeleniumBaseCore(WebDriver driver) {
		this.driver = driver();
	}

	public void achieveCoordinatesDimension(String locator) {

		WebElement logo = driver().findElement(By.xpath(locator));
		Reporter.log("Achieve Height Coordinates Dimension: " + logo.getRect().getDimension().getHeight());
		Reporter.log("Achieve Width Corrdinates Dimension: " + logo.getRect().getDimension().getWidth());
	}

	// ===================================================
	// Code part used to get HTML5 validation message
	// ===================================================

	public void assertHtml5ValidationMessage(String locator, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert HTML5 Validation Message: " + expected + " --> At Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);

		WebElement element = driver().findElement(By.xpath(locator));
		JavascriptExecutor jse = (JavascriptExecutor) driver();
		String message = (String) jse.executeScript("return arguments[0].validationMessage", element);
		try {
			assertEquals(message, expected);
			APP_LOGS("Passes To Assert HTML5 Validation Message: " + message + " == " + expected + " --> At Locator: " + locator);
		} catch (AssertionError e) {
			// TODO: handle exception
			APP_LOGS_FAIL("Fails To Assert HTML5 Validation Message: " + message + " == " + expected + " --> At Locator: " + locator);
			APP_LOGS_FAIL(e.getMessage());
			assertEquals(message, expected);
		}
	}

	// ===================================================
	// Code part used to handle searching below
	// ===================================================

	public void maxValueInDataTable(String rows_tagname, String column_tagname) throws ParseException {

		String max;
		double m = 0, r = 0;

		List<WebElement> rows = driver().findElements(By.tagName(rows_tagname));
		for (int i = 0; i < rows.size(); i++) {

			max = driver().findElement(By.tagName("tr/[" + (i + 1) + "]/" + column_tagname)).getText();
			NumberFormat f = NumberFormat.getNumberInstance();
			Number num = f.parse(max);
			max = num.toString();
			m = Double.parseDouble(max);
			if (m > r) {
				r = m;
			}
		}
		APP_LOGS("Maximum current price is : " + r);
	}

	public void countTotalNoOfRecordsInDataTable(String tagname) {

		List<WebElement> rows = driver().findElements(By.tagName(tagname));
		APP_LOGS("Total No Of Records In Data Table: " + rows.size());
	}

	public void isDesignatedItemInDataTable(String locator, String expected) {

		waitForPresenceOfElement(locator);
		try {
			WebElement col = driver().findElement(By.xpath(locator));
			assertEquals(col.getText(), expected);
			APP_LOGS("Value Found: " + col.getText());

		} catch (NoSuchElementException e) {
			APP_LOGS_FAIL(e.getMessage());
			fail(null, e);
		}
	}

	public void findItemByAllInDataTable(String nextbutton, String expected) {

		Reporter.log("\n [INFO] - Executed | Finding Item By All In Data Table: " + expected + " |", true);

		int pageindex = 0;

		loop: while (true) {
			waitForPresenceOfElement(By.tagName("table"));
			waitForPresenceOfElement(By.tagName("tr"));

			WebElement tbl = driver().findElement(By.tagName("table"));
			List<WebElement> rows = tbl.findElements(By.tagName("tr"));
			try {
				for (int i = 0; i < rows.size(); i++) {

					JavascriptExecutor jse = (JavascriptExecutor) driver();
					jse.executeScript("arguments[0].scrollIntoView();", rows.get(i));
					List<WebElement> cols = rows.get(i).findElements(By.tagName("th"));

					for (int j = 0; j < cols.size(); j++) {

						jse.executeScript("arguments[0].scrollIntoView();", cols.get(j));
						String actual = cols.get(j).getText();

						if (actual.contains(expected)) {
							APP_LOGS("Item By All Was Found In Data Table At The Position: [" + i + "] " + actual);
							break loop;
						}
					}
				}
			} catch (WebDriverException e) {
				// TODO: handle exception

				APP_LOGS_FAIL(e.getMessage());
				fail(null, e);
			}
			WebElement nextButton = driver().findElement(By.xpath(nextbutton));

			if (nextButton.isEnabled()) {
				nextButton.click();
				APP_LOGS("Went To The Next Page Index: " + (pageindex + 1));

			} else {
				APP_LOGS("Went to The Lastest Page Index: " + (pageindex));
				APP_LOGS_FAIL("Item By All Not Found In Data Table: " + expected);
				break loop;
			}
			pageindex++;
		}
	}

	public void findItemBySpecificInDataTable(String nextbutton, String column, String expected) {

		Reporter.log("\n [INFO] - Executed | Finding Item By Specific On Data Table: " + expected + " |", true);

		int pageindex = 0;

		loop: while (true) {
			waitForPresenceOfElement(column);
			List<WebElement> cols = driver().findElements(By.xpath(column));
			try {
				Thread.sleep(500);
				for (int i = 0; i < cols.size(); i++) {

					JavascriptExecutor jse = (JavascriptExecutor) driver();
					jse.executeScript("arguments[0].scrollIntoView();", cols.get(i));
					String actual = cols.get(i).getText();

					if (actual.contains(expected)) {
						APP_LOGS("Item By Specific Was Found On Data Table At The Position: [" + i + "] " + actual);
						break loop;
					}
				}
			} catch (WebDriverException | InterruptedException e) {
				// TODO: handle exception

				APP_LOGS_FAIL(e.getMessage());
				fail(null, e);
			}
			WebElement nextButton = driver().findElement(By.xpath(nextbutton));

			if (nextButton.isEnabled()) {
				nextButton.click();
				APP_LOGS("Went To The Next Page: " + (pageindex + 1));

			} else {
				APP_LOGS("Went to The Lastest Page Index: " + pageindex);
				APP_LOGS_FAIL("Item By Specific Not Found On Data Table: " + expected);
				break loop;
			}
			pageindex++;
		}
	}

	// ===================================================
	// Code part used to handle file attachment below
	// ===================================================

	public void assertInvalidBrokenImage() {

		Reporter.log("\n [INFO] - Executed | Assert Invalid Broken Image" + " |", true);
		int invalidImageCount = 0;
		List<WebElement> imagesList = driver().findElements(By.tagName("img"));
		APP_LOGS("Total No. Of Available Images: " + imagesList.size());

		for (WebElement imgElement : imagesList) {

			if (imgElement != null) {
				try {
					HttpClient client = HttpClientBuilder.create().build();
					HttpGet request = new HttpGet(imgElement.getAttribute("src"));
					HttpResponse response = client.execute(request);

					if (response.getStatusLine().getStatusCode() != 200)
						invalidImageCount++;

				} catch (IOException e) {
					Reporter.log(e.getMessage(), true);
				}
			}
		}
		if (invalidImageCount > 0)
			APP_LOGS_FAIL("Total No. Of Invalid Broken Images: " + invalidImageCount);
		else
			APP_LOGS("Total No. Of Invalid Broken Images: " + invalidImageCount);
	}

	// ===================================================
	// Code part used to handle file attachment below
	// ===================================================

	protected void uploadFile(String locator, String filepath) {

		Reporter.log("\n [INFO] - Executed | File Uploading" + " |", true);
		driver().findElement(By.xpath(locator)).sendKeys(System.getProperty("user.dir") + filepath);
		APP_LOGS("File Is Uploaded" + " |");
	}

	protected void uploadFile2(String locator, String filepath, String filename) {

		Reporter.log("\n [INFO] - Executed | File Uploading" + " |", true);
		try {
			click(locator);
			Runtime.getRuntime().exec("Script.exe");
		} catch (IOException e) {

			APP_LOGS_FAIL("File Is Uploaded" + " |");
			fail(null, e);
		}
		StringSelection select;
		select = new StringSelection(filepath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			// TODO Auto-generated catch block

			fail(null, e);
		}
		isFileExisting(filepath, filename);
		APP_LOGS("File Is Uploaded" + " |");
	}

	public void downloadFile(String locator, String filepath, String filename) {

		Reporter.log("\n [INFO] - Executed | File Downloading" + " |", true);
		try {
			click(locator);
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyPress(KeyEvent.KEY_RELEASED);
		} catch (AWTException e) {
			// TODO Auto-generated catch block

			fail(null, e);
		}
		isFileExisting(filepath, filename);
		APP_LOGS("File Is Downloaded" + " |");
	}

	public boolean isFileExisting(String filepath, String filename) {

		boolean flag = false;
		File dir = new File(filepath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {

			if (dir_contents[i].equals(filename))
				return flag = true;
		}
		return flag;
	}

	public String getDownloadedDocumentName(String downloadDir, String fileExtension) throws IOException, InterruptedException {

		String downloadedFileName = null;
		boolean valid = true;
		boolean found = false;

		// default timeout in seconds
		long timeOut = 20;
		Path downloadFolderPath = Paths.get(downloadDir);
		WatchService watchService = FileSystems.getDefault().newWatchService();
		downloadFolderPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
		long startTime = System.currentTimeMillis();

		do {
			WatchKey watchKey;
			watchKey = watchService.poll(timeOut, TimeUnit.SECONDS);
			long currentTime = (System.currentTimeMillis() - startTime) / 1000;

			if (currentTime > timeOut) {
				Reporter.log("Download operation timed out.. Expected file was not downloaded", true);
				return downloadedFileName;
			}

			for (WatchEvent<?> event : watchKey.pollEvents()) {

				WatchEvent.Kind<?> kind = event.kind();
				if (kind.equals(StandardWatchEventKinds.ENTRY_CREATE)) {
					String fileName = event.context().toString();
					Reporter.log("New File Created:" + fileName);

					if (fileName.endsWith(fileExtension)) {
						downloadedFileName = fileName;
						Reporter.log("Downloaded file found with extension " + fileExtension + ". File name is " + fileName, true);
						Thread.sleep(500);
						found = true;
						break;
					}
				}
			}

			if (found) {
				return downloadedFileName;
			} else {
				currentTime = (System.currentTimeMillis() - startTime) / 1000;

				if (currentTime > timeOut) {
					Reporter.log("Failed to download expected file", true);
					return downloadedFileName;
				}
				valid = watchKey.reset();
			}
		} while (valid);
		return downloadedFileName;
	}

	// ===================================================
	// Code part used to declaring variables above
	// ===================================================

	public void refreshPage() {

		Reporter.log("\n [INFO] - Executed | Refreshing Present Page |", true);
		APP_LOGS("Refreshed Present Page |");
		try {
			Thread.sleep(1000);
			driver().navigate().refresh();
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void switchingHandleTabs2(Method method) {

		try {
			(new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS)).until(ExpectedConditions.numberOfWindowsToBe(2));
			String winHandleBefore = driver().getWindowHandle();
			for (String winHandle : driver().getWindowHandles()) {
				driver().switchTo().window(winHandle);
			}
			String getTitle = "";
			getTitle = driver().getTitle();
			Reporter.log("\n [INFO] - Executed | Switching Handle New Tab For: " + getTitle + " |", true);

			method.getName();

			driver().close();
			driver().switchTo().window(winHandleBefore);
			APP_LOGS("Switching Handle New Tab For: " + getTitle + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Switching Handle New Tab |");
			fail(null, e);
		}
	}

	public void switchingHandleTabs(Method method) {

		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver();
			jse.executeScript("window.open()");
			List<String> browserTabs = new ArrayList<String>(driver().getWindowHandles());
			driver().switchTo().window(browserTabs.get(1));
			String getTitle = "";
			getTitle = driver().getTitle();
			Reporter.log("\n [INFO] - Executed | Switching Handle New Tab For: " + getTitle + " |", true);

			method.getName();

			driver().close();
			driver().switchTo().window(browserTabs.get(0));
			APP_LOGS("Switching Handle New Tab For: " + getTitle + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Switching Handle New Tab |");
			fail(null, e);
		}
	}

	protected void switchingHandleWindows(Method method) {

		try {
			(new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS)).until(ExpectedConditions.numberOfWindowsToBe(2));
			String MainWindow = driver().getWindowHandle();
			Set<String> set = driver().getWindowHandles();
			Iterator<String> iterator = set.iterator();
			while (iterator.hasNext()) {
				String ChildWindow = iterator.next();
				if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
					driver().switchTo().window(ChildWindow);
					String getTitle = "";
					getTitle = driver().getTitle();
					Reporter.log("\n [INFO] - Executed | Switching And Handling All Opened Window For: " + getTitle + " |", true);

					method.getName();

					APP_LOGS("Switching And Handling All Opened Window For: " + getTitle + " |");
				}
			}
			driver().switchTo().window(MainWindow);
		} catch (WebDriverException e) {

			APP_LOGS_FAIL("Switching And Handling All Opened Window |");
			fail(null, e);
		}
	}

//	public void switchTabs() {
//
//		selenium.switchingHandleTabs(override_switchingHandleTabs());
//	}
//	public Method override_switchingHandleTabs() {
//
//		return null;
//	}

	protected void mediaPlayer(String tagname) {

		Reporter.log("\n [INFO] - Executed | Playing Media: " + tagname + " |", true);
		waitForPresenceOfElement(By.tagName(tagname));
		try {
			WebElement element = driver().findElement(By.tagName(tagname));
			JavascriptExecutor jse = (JavascriptExecutor) driver();
			jse.executeScript("arguments[0].play();", element);
//			jse.executeScript("arguments[0].setVolume(50);", element);
//			jse.executeScript("arguments[0].setMute(true);", element);
//			jse.executeScript("arguments[0].setMute(false);", element);
			jse.executeScript("arguments[0].pause()", element);
			APP_LOGS("Played Media: " + tagname + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Played Media: " + tagname + " |");
			fail(null, e);
		}
	}

	// =========================================================
	// Code part used to indicate screenshot / capture below
	// =========================================================

	public String captureElement(String locator, String destination, String name) {

		WebElement element = driver().findElement(By.xpath(locator));
		String dest = destination + name;
		File file = new File(dest);
		try {
			File elementShot = element.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(elementShot, file);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			fail(null, e);
		}
		return dest;
	}

	public String capture(String name) {

		TakesScreenshot ts = (TakesScreenshot) driver();
		String dest = ".\\evidences\\" + name + ".png";
		File file = new File(dest);
		try {
			File screenshot = ts.getScreenshotAs(OutputType.FILE);
			FileHandler.copy(screenshot, file);
//			FileUtils.copyFile(screenshot, file);
		} catch (Exception e) {
			// TODO: handle exception
			fail(null, e);
		}
		return dest;
	}

	public String capture(String destination, String name) {

		TakesScreenshot ts = (TakesScreenshot) driver();
		File screenshot = null;
		String dest = destination + name;
		File file = null;
		file = new File(dest);
		try {
			screenshot = ts.getScreenshotAs(OutputType.FILE);
			FileHandler.copy(screenshot, file);
//			FileUtils.copyFile(screenshot, file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			fail(null, e);
		}
		return dest;
	}

	protected void captureScreenshot() {

		Reporter.log("\n [INFO] - Executed | Screenshot Capture Entire Screen: " + driver().getTitle() + " |", true);
		BufferedImage image = null;
		try {
			image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
		} catch (HeadlessException | AWTException e) {
			// TODO Auto-generated catch block
		}
		String title = driver().getTitle().replaceAll("\\W", "");
		Random random = new Random();
		try {
			status = ImageIO.write(image, "jpg", new File(".\\evidences\\" + title + random.nextInt() + ".jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Reporter.log(e.getMessage(), true);
		}
		Reporter.log("\n [INFO] - Executed | Screenshot Captured Entire Screen: " + driver().getTitle() + " == " + status + " |", true);
	}

	// ===================================================
	// Code part used to get text attribute below
	// ===================================================

	public String getTextAttribute(String locator) {

		Reporter.log("\n [INFO] - Executed | Getting Text / Attribute In Attribute: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			WebElement element = driver().findElement(By.xpath(locator));
			String attribute = "";
			// innerText | innerHTML |
			attribute = element.getAttribute("textContent").toString().trim();
			return attribute;

		} catch (NullPointerException e) {
			// TODO: handle exception
			WebElement element = driver().findElement(By.xpath(locator));
			String text = "";
			text = element.getText().toString().trim();
			System.out.println("=======>>>>>>>>>" + text);
			return text;
		} catch (WebDriverException e) {
			// TODO: handle exception
			APP_LOGS_FAIL("Get Text / Attribute In Locator: " + locator + " |");
		}
		return locator;
	}

	protected void alertDismiss() {

		Reporter.log("\n [INFO] - Executed | Dissmissing Alert |", true);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			alert = wait.until(ExpectedConditions.alertIsPresent());
			alert = driver().switchTo().alert();
			alert.dismiss();
			APP_LOGS("Dismissed Alert" + alert.getText() + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Dismissed Alert" + alert.getText() + " |");
			fail(null, e);
		}
	}

	protected void alertAccept() {

		Reporter.log("\n [INFO] - Executed | Accepting Alert |", true);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			alert = wait.until(ExpectedConditions.alertIsPresent());
			alert = driver().switchTo().alert();
			alert.accept();
			APP_LOGS("Accepted Alert: " + alert.getText() + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Accepted Alert" + alert.getText() + " |");
			fail(null, e);
		}
	}

	public void getURL(String url) {

		Reporter.log("\n [INFO] - Executed | Opening URL Path Navigation To: " + url + " |", true);
		APP_LOGS("Opening URL Path Navigation To: " + url + " |");
		try {
			driver().get(url);
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Opened URL Path Navigation To: " + url + " |");
			fail(null, e);
		}
	}

	public void navigateToURL(String url) {

		Reporter.log("\n [INFO] - Executed | Opening URL Path Navigation To: " + url + " |", true);
		APP_LOGS("Opening URL Path Navigation To: " + url + " |");
		try {
			driver().navigate().to(url);
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Opening URL Path Navigation To: " + url + " |");
			fail(null, e);
		}
	}

	protected void selectAllRadioButton(String locator) {

		Reporter.log("\n [INFO] - Executed | Selecting Radio Button: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			List<WebElement> list = driver().findElements(By.xpath(locator));
			wait.until(ExpectedConditions.elementToBeClickable((By) list));
			status = list.get(0).isSelected();
			if (status == true) {
				list.get(1).click();
				APP_LOGS("The First Radio Button Is Selected ==> Select The Second: " + locator + " |");
			} else {
				list.get(0).click();
				APP_LOGS("The Second Radio Button Is Selected ==> Select The First: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Selecting Radio Button: " + locator + " |");
			fail(null, e);
		}
	}

	// ===================================================
	// Code part used to handle drop-down below
	// ===================================================

	protected void selectDropdownElective(String locator, String parent_locator, String child_tagname) {

		Reporter.log("\n [INFO] - Executed | Selecting Dropdown In Locator: " + locator + " --> " + child_tagname + " |", true);
		waitForPresenceOfElement(locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			Thread.sleep(500);
			WebElement menu;
			wait.until(ExpectedConditions.elementToBeClickable(menu = driver().findElement(By.xpath(locator))));
			if (menu.isEnabled()) {
				menu.click();
			}
			Thread.sleep(500);
			WebElement dropdown = driver().findElement(By.xpath(parent_locator));
			List<WebElement> list = dropdown.findElements(By.tagName(child_tagname));
			int size = list.size();
			int randomValue = ThreadLocalRandom.current().nextInt(size - 1) + 1;
			if (list.get(randomValue).isEnabled()) {
				list.get(randomValue).click();
			}
			APP_LOGS("Selected Dropdown In Locator: " + parent_locator + " --> " + child_tagname + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Selected Dropdown In Locator: " + parent_locator + " --> " + child_tagname + " |");
			fail(null, e);
		}
	}

	protected void selectDropdownButton(String locator, String tagname, int index) {

		Reporter.log("\n [INFO] - Executed | Selected Dropdown In Index [" + index + "] At Locator: " + locator + " --> " + tagname + " |", true);
		waitForPresenceOfElement(locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			Thread.sleep(500);
			WebElement menu;
			wait.until(ExpectedConditions.elementToBeClickable(menu = driver().findElement(By.xpath(locator))));
			if (menu.isEnabled()) {
				menu.click();
			}
			waitForPresenceOfElement(By.tagName(tagname));
			List<WebElement> list = driver().findElements(By.tagName(tagname));
			list.get(index).click();
			APP_LOGS("Selected Dropdown In Index [" + index + "] At Locator: " + locator + " --> " + tagname + " |");
		} catch (NoSuchElementException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Selected Dropdown In Index [" + index + "] At Locator: " + locator + " --> " + tagname + " |");
			fail(null, e);
		}
	}

	protected void selectDropdownButton(String parent_locator, String child_locator) {

		Reporter.log("\n [INFO] - Executed | Selecting Dropdown In Locator: " + parent_locator + " --> " + child_locator + " |", true);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			WebElement element;
			wait.until(ExpectedConditions.elementToBeClickable(element = driver().findElement(By.xpath(parent_locator))));
			if (element.isEnabled()) {
				element.click();
			}
			Thread.sleep(1000);
			waitForPresenceOfElement(parent_locator);
			WebElement option;
			wait.until(ExpectedConditions.elementToBeClickable(option = element.findElement(By.xpath(child_locator))));
			if (option.isEnabled()) {
				option.click();
				APP_LOGS("Selected Dropdown In Locator: " + parent_locator + " --> " + child_locator + " |");
			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Selected Dropdown In Locator: " + parent_locator + " --> " + child_locator + " |");
			fail(null, e);
		}
	}

	protected void selectDropdown(String locator, int index) {

		Reporter.log("\n [INFO] - Executed | Selecting Dropdown In Index [" + index + "] At Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			wait.until(ExpectedConditions.elementSelectionStateToBe(By.xpath(locator), false));
			Select dropDown = new Select(driver().findElement(By.xpath(locator)));
			dropDown.selectByIndex(index);
			APP_LOGS("Selected Dropdown In Index [" + index + "] At Locator: " + locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("[INFO] - Executed | Selected Dropdown In Index [" + index + "] At Locator: " + locator + " |");
			fail(null, e);
		}
	}

	// ===================================================
	// Code part used to handle check-box below
	// ===================================================

	protected void check(String locator) {

		Reporter.log("\n [INFO] - Executed | Selecting Checkbox: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			WebElement element;
			wait.until(ExpectedConditions.elementToBeClickable(element = driver().findElement(By.xpath(locator))));
			if (element.isEnabled()) {
				element.click();
			}
			APP_LOGS("Checkbox Is selected: " + locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Checkbox Is selected: " + locator + " |");
			fail(null, e);
		}
	}

	protected void checkAllCheckbox(String parent_locator, String child_tagname) {

		Reporter.log("\n [INFO] - Executed | Selecting All Checkbox In Locator: " + parent_locator + " |", true);
		waitForPresenceOfElement(parent_locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			WebElement element;
			wait.until(ExpectedConditions.elementToBeClickable(element = driver().findElement(By.xpath(parent_locator))));
			List<WebElement> elements = element.findElements(By.tagName(child_tagname));
			for (WebElement el : elements) {
				el.click();
			}
			APP_LOGS("All Checkbox Is Selected: " + parent_locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("All Checkbox Is Selected: " + parent_locator + " |");
			fail(null, e);
		}
	}

	protected void checkboxIsUnselected(String locator) {

		Reporter.log("\n [INFO] - Executed | Checking If Checkbox Is Inactive: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		WebElement element = driver().findElement(By.xpath(locator));
		String attribute = "";
		attribute = element.getAttribute("checked");
		if (attribute != null) {
			APP_LOGS_FAIL("Expect Checkbox Is Inactive [Unselected] But Found The Opposite: " + locator + " |");
			fail();
		} else {
			APP_LOGS("Expect Checkbox Is Inactive [UnSelected]: " + locator + " |");
		}
	}

	protected void checkboxIsSelected(String locator) {

		Reporter.log("\n [INFO] - Executed | Checking If Checkbox Is Active: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		WebElement element = driver().findElement(By.xpath(locator));
		String attribute = "";
		attribute = element.getAttribute("checked");
		if (attribute != null) {
			APP_LOGS("Expect Checkbox Is Active [Selected]: " + locator + " |");
		} else {
			APP_LOGS_FAIL("Expect Checkbox Is Active [Selected] But Found The Opposite: " + locator + " |");
			fail();
		}
	}

	// ===================================================
	// Code part used to handle event on click below
	// ===================================================

	public void doubleClick(String locator) {

		Reporter.log("\n [INFO] - Executed | Double Clicking On Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			WebElement element = driver().findElement(By.xpath(locator));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			if (element.isEnabled()) {
				Actions action = new Actions(driver()).doubleClick(element);
				action.build().perform();
				APP_LOGS("Double Clicked On Locator: " + locator + " |");
			}

		} catch (WebDriverException e) {
			// TODO: handle exception
			APP_LOGS_FAIL(e.getMessage());
			fail(null, e);
		}
	}

	public void rightClick(String locator) {

		Reporter.log("\n [INFO] - Executed | Right Clicking On Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			WebElement element = driver().findElement(By.xpath(locator));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			if (element.isEnabled()) {
				Actions action = new Actions(driver()).contextClick(element);
				action.build().perform();
				APP_LOGS("Right Clicked On Locator: " + locator + " |");
			}

		} catch (WebDriverException e) {
			// TODO: handle exception
			APP_LOGS_FAIL(e.getMessage());
			fail(null, e);
		}
	}

	public void submit(String locator) {

		Reporter.log("\n [INFO] - Executed | Submitting On Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			WebElement element;
			wait.until(ExpectedConditions.elementToBeClickable(element = driver().findElement(By.xpath(locator))));
			if (element.isEnabled()) {
				element.submit();
				APP_LOGS("Submitted On Locator: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Submitted On Locator: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickAndHold(String locator) {

		Reporter.log("\n [INFO] - Executed | Clicking And Holding On Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			WebElement element;
			wait.until(ExpectedConditions.elementToBeClickable(element = driver().findElement(By.xpath(locator))));
			actions = new Actions(driver());
			if (element.isEnabled()) {
				actions.moveToElement(element).clickAndHold().build().perform();
				APP_LOGS("Clicked And Holded On Locator: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked And Holded On Locator: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickInvisibleHidden(String locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On Invisible Hidden Locator: " + locator + " |", true);
		try {
			WebElement element = driver().findElement(By.xpath(locator));
//	    if (element.isEnabled()) {
			JavascriptExecutor jse = (JavascriptExecutor) driver();
			jse.executeScript("return arguments[0].click();", element);
			element.click();
			APP_LOGS("Clicked On Invisible Hidden Locator: " + locator + " |");
//	    }
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On Invisible Hidden Locator: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickHref(String locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On Href Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			Thread.sleep(500);
			WebElement element = driver().findElement(By.xpath(locator));
			element.click();
			APP_LOGS("Clicked On Href Locator: " + locator + " |");
		} catch (ElementClickInterceptedException | InterruptedException e) {
			// TODO: handle exception

			// Ignore Element Click Intercepted Exception
		}
	}

	public void clickIgnoreScroll(String locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On Locator: " + locator + " |", true);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			Thread.sleep(500);
			WebElement element = driver().findElement(By.xpath(locator));
			wait.until(ExpectedConditions.elementToBeClickable(element = driver().findElement(By.xpath(locator))));
			if (element.isEnabled()) {
				element.click();
				APP_LOGS("Clicked On Locator: " + locator + " |");
			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On Locator: " + locator + " |");
			fail(null, e);
		}
	}

	public void click(String locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			Thread.sleep(500);
			WebElement element = driver().findElement(By.xpath(locator));
			wait.until(ExpectedConditions.elementToBeClickable(element = driver().findElement(By.xpath(locator))));
			if (element.isEnabled()) {
				element.click();
				APP_LOGS("Clicked On Locator: " + locator + " |");
			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On Locator: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickItemInList(String locator, int index) {

		Reporter.log("\n [INFO] - Executed | Clicking On Item In List: " + locator + " |", true);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		waitForPresenceOfElement(locator);
		try {
			List<WebElement> list = driver().findElements(By.xpath(locator));
			if (list.get(index).isEnabled()) {
				list.get(index).click();
				APP_LOGS("Clicked On Item In List: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On Item In List: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickLastItemInList(String locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On Last Item In List: " + locator + " |", true);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		waitForPresenceOfElement(locator);
		try {
			List<WebElement> list = driver().findElements(By.xpath(locator));
			int size = list.size();
			if (list.get(size - 1).isEnabled()) {
				list.get(size - 1).click();
				APP_LOGS("Clicked On Last Item In List: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On Last Item In List: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickFirstItemInList(String locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On First Item In List: " + locator + " |", true);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		waitForPresenceOfElement(locator);
		try {
			List<WebElement> list = driver().findElements(By.xpath(locator));
			if (list.get(0).isEnabled()) {
				list.get(0).click();
				APP_LOGS("Clicked On First Item List: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On First Item In Data Row Table: " + locator + " |");
			fail(null, e);
		}
	}

	// ===================================================
	// Code part used to move on element below
	// ===================================================

	public void scrollIntoViewTop(By by) {

		JavascriptExecutor jse = (JavascriptExecutor) driver();
		WebElement element = driver().findElement(by);
		jse.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public void scrollIntoViewTop(String locator) {

		JavascriptExecutor jse = (JavascriptExecutor) driver();
		WebElement element = driver().findElement(By.xpath(locator));
		jse.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	protected void scrollIntoViewBottom(By by) {

		JavascriptExecutor jse = (JavascriptExecutor) driver();
		WebElement element = driver().findElement(by);
		jse.executeScript("arguments[0].scrollIntoView(false);", element);
	}

	protected void scrollIntoViewBottom(String locator) {

		JavascriptExecutor jse = (JavascriptExecutor) driver();
		WebElement element = driver().findElement(By.xpath(locator));
		jse.executeScript("arguments[0].scrollIntoView(false);", element);
	}

	protected void dragVerticalSlider(String locator, int yOffset) {

		Reporter.log("\n [INFO] - Executed | Dragging The Horizontal Slider: " + locator + " --> With xOffset: " + yOffset + " |", true);
		waitForPresenceOfElement(By.xpath(locator));
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			WebElement element;
			wait.until(ExpectedConditions.elementToBeClickable(element = driver().findElement(By.xpath(locator))));
			driver().switchTo().frame(0);
			actions = new Actions(driver());
			actions.dragAndDropBy(element, 0, yOffset).build().perform();
			actions.release();
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Dragged The Horizontal Slider: " + locator + " --> With xOffset: " + yOffset + " |");
			fail(null, e);
		}
		APP_LOGS("Dragged The Horizontal Slider: " + locator + " --> With xOffset: " + yOffset + " |");
	}

	protected void dragHorizontalSlider(String locator, int xOffset) {

		Reporter.log("\n [INFO] - Executed | Dragging The Horizontal Slider: " + locator + " --> With xOffset: " + xOffset + " |", true);
		waitForPresenceOfElement(By.xpath(locator));
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			WebElement element;
			wait.until(ExpectedConditions.elementToBeClickable(element = driver().findElement(By.xpath(locator))));
			driver().switchTo().frame(0);
			actions = new Actions(driver());
			actions.dragAndDropBy(element, xOffset, 0).build().perform();
			actions.release();
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Dragged The Horizontal Slider: " + locator + " --> With xOffset: " + xOffset + " |");
			fail(null, e);
		}
		APP_LOGS("Dragged The Horizontal Slider: " + locator + " --> With xOffset: " + xOffset + " |");
	}

	protected void dragAndDrop(String locator, int xOffset, int yOffset) {

		Reporter.log("\n [INFO] - Executed | Dragging And Dropping: " + locator + " |", true);
		waitForPresenceOfElement(By.xpath(locator));
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			WebElement element;
			wait.until(ExpectedConditions.elementToBeClickable(element = driver().findElement(By.xpath(locator))));
			actions = new Actions(driver());
			actions.clickAndHold(element).moveByOffset(xOffset, yOffset).release().build().perform();
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Dragged And Dropped: " + locator + " |");
			fail(null, e);
		}
		APP_LOGS("Dragged And Dropped: " + locator + " |");
	}

	public void hoverMoveOn(int xOffset, int yOffset) {

		Reporter.log("\n [INFO] - Executed | Hover Move On: " + xOffset + " - " + yOffset + " |", true);
		try {
			Thread.sleep(500);
			actions = new Actions(driver());
			actions.moveByOffset(xOffset, yOffset).build().perform();
			APP_LOGS("Hover Move On: " + xOffset + "," + yOffset + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Hover Move On: " + xOffset + "," + yOffset + " |");
			fail(null, e);
		}
	}

	protected void hoverMoveOnLastItem(String locator) {

		Reporter.log("\n [INFO] - Executed | Hover Move On: " + locator + " |", true);
		try {
			Thread.sleep(500);
			List<WebElement> elements = driver().findElements(By.xpath(locator));
			int size = elements.size();
			actions = new Actions(driver());
			if (elements.get(size - 1).isEnabled()) {
				actions.moveToElement(elements.get(size - 1)).build().perform();
				APP_LOGS("Hove Move On: " + locator + " |");
			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Hover Move On: " + locator + " |");
			fail(null, e);
		}
	}

	protected void hoverMoveOnFirstItem(String locator) {

		Reporter.log("\n [INFO] - Executed | Hover Move On: " + locator + " |", true);
		try {
			Thread.sleep(500);
			List<WebElement> elements = driver().findElements(By.xpath(locator));
			actions = new Actions(driver());
			if (elements.get(0).isEnabled()) {
				actions.moveToElement(elements.get(0)).build().perform();
				APP_LOGS("Hove Move On: " + locator + " |");
			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Hover Move On: " + locator + " |");
			fail(null, e);
		}
	}

	public void hoverMoveOn(String locator) {

		Reporter.log("\n [INFO] - Executed | Hover Move On: " + locator + " |", true);
		try {
			Thread.sleep(500);
			WebElement element = driver().findElement(By.xpath(locator));
			actions = new Actions(driver());
//			if (element.isEnabled()) {
			actions.moveToElement(element).build().perform();
			APP_LOGS("Hove Move On: " + locator + " |");
//			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Hover Move On: " + locator + " |");
			fail(null, e);
		}
	}

	// ===================================================
	// Code part used to handle checking component below
	// ===================================================

	protected void checkIfElementIsDisabled(String locator, String cssValue, String expected) {

		Reporter.log("\n [INFO] - Executed | Checking If Element Is Disabled: " + locator + " --> Via CSS Value Attribute: " + cssValue + " |", true);
		try {
			Thread.sleep(500);
			WebElement element = driver().findElement(By.xpath(locator));
			String value = "";
			value = element.getCssValue(cssValue);
			Reporter.log("Get CSS Value: " + value, true);
			if (value.contains(expected)) {
				APP_LOGS("CSS Attribute Is Disabled: " + cssValue + " --> With Value: " + value + " == " + expected + " |");
			} else {
				APP_LOGS_FAIL("CSS Attribute Is Enabled: " + cssValue + " --> With Value: " + value + " == " + expected + " |");
				assertEquals(value, expected);
			}
			APP_LOGS("Checked Element Is Disabled: " + locator + " --> Via CSS Value Attribute: " + cssValue + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Checked Element Is Disabled: " + locator + " --> Via CSS Value Attribute: " + cssValue + " |");
			fail(null, e);
		}
	}

	protected void checkIfButtonCursorIsDisabled(String locator) {

		Reporter.log("\n [INFO] - Executed | Checking If Element Is Disabled: " + locator + " --> Via CSS Value Attribute: cursor" + " |", true);
		try {
			Thread.sleep(500);
			WebElement element = driver().findElement(By.xpath(locator));
			String value = "";
			value = element.getCssValue("cursor");
			if (value.contains("not-allowed")) {
				APP_LOGS("CSS Attribute Is Disabled: cursor" + " --> With Value: " + value + " |");
			} else {
				APP_LOGS_FAIL("[INFO] - Executed | CSS Attribute Is Enabled: cursor" + " --> With Value: " + value + " |");
				assertTrue(value.contains("not-allowed"));
			}
			APP_LOGS("Checked Element Is Disabled: " + locator + " --> Via CSS Value Attribute: cursor" + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Checked Element Is Disabled: " + locator + " --> Via CSS Value Attribute: cursor" + " |");
			fail(null, e);
		}
	}

	protected void checkIfElementColourIsDisabled(String locator, String expected) {

		Reporter.log("\n [INFO] - Executed | Checking If Element Is Disabled By Colour: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			Thread.sleep(500);
			WebElement element = driver().findElement(By.xpath(locator));
			String color = "";
			color = element.getCssValue("background-color");
			APP_LOGS_FAIL("CSS Value By Background Color: " + color + " |");
			String hex = Color.fromString(color).asHex();
			if (hex.contains(expected)) {
				APP_LOGS("Asserted / Validated CSS Value As Hex. Element Is Disabled By Colour: \n" + hex + " == " + expected + " |");
			} else {
				APP_LOGS_FAIL("[INFO] - Executed | Asserted / Validated CSS Value As Hex. Element Is Enabled By Colour: \n" + hex + " == " + expected + " |");
				assertEquals(hex, expected);
			}
			APP_LOGS_FAIL("Checked Element Is Disabled By Colour: " + locator + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Checked Element Is Disabled By Colour: " + locator + " |");
			fail(null, e);
		}
	}

	protected void checkIfElementIsUndisplayed(String locator) {

		Reporter.log("\n [INFO] - Executed | Checking If Element Is Undisplayed: " + locator + " |", true);
		String returnActual = "Element Is Displayed (False): ";
		String returnExpected = "Element Is Not Displayed (True): ";

		new JSWaitCore(driver()).waitAllRequest();
		try {
//			Thread.sleep(500);
			status = driver().findElement(By.xpath(locator)).isDisplayed() && driver().findElements(By.xpath(locator)).size() > 0;
			if (status != null && status == true) {
				APP_LOGS_FAIL("Checked If Element Is Undisplayed: " + locator + " |");
				assertEquals(returnActual, returnExpected);
			}
		} catch (NoSuchElementException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("" + "Checked If Element Is Undisplayed: " + locator + " |");
		}
//		APP_LOGS("" + returnExpected + locator + " |");
	}

	protected void checkIfElementIsDisplayed(String locator) {

		Reporter.log("\n [INFO] - Executed | Checking If Element Is Displayed: " + locator + " |", true);
		String returnExpected = "Element Is Displayed (False): ";
		String returnActual = "Element Is Not Displayed (True): ";

//		new JSWaitCore(driver()).waitAllRequest();
		try {
			Thread.sleep(500);
			status = driver().findElement(By.xpath(locator)).isDisplayed() && driver().findElements(By.xpath(locator)).size() != 0;
			if (status != null && status == true) {
				APP_LOGS("Checked If Element Is Displayed: " + locator + " |");
			}
		} catch (NoSuchElementException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("" + "Checked If Element Is Displayed: " + locator + " |");
			assertEquals(returnActual, returnExpected);
		}
	}

	// ===============================================================
	// Code part used to handle assertion / validation data below
	// ===============================================================

	protected void assertIsFirstItemInCustomTable(String table, String body, String rows, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is First Item In Data Row Table: " + rows + " |", true);
		waitForPresenceOfElement(table);
		waitForPresenceOfElement(body);
		try {
			WebElement tableName = driver().findElement(By.xpath(table));
			WebElement tableBody = tableName.findElement(By.xpath(body));
			List<WebElement> tableRows = tableBody.findElements(By.xpath(rows));
			String actual = "";
			actual = tableRows.get(0).getText();
			if (actual.contains(expected) == true) {
				APP_LOGS("Asserted Is First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + rows + " |");
			} else {
				APP_LOGS_FAIL("Asserted Is First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + rows + " |");
				assertEquals(actual, expected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void assertIsNotFirstItemInCustomTable(String table, String body, String rows, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is Not First Item In Data Row Table: " + rows + " |", true);
		String returnExpected = "Item Was Not The First In Data Table (True): ";
		String returnActual = "Item Was The First In Data Table (False): ";
		waitForPresenceOfElement(table);
		waitForPresenceOfElement(body);
		try {
			WebElement tableName = driver().findElement(By.xpath(table));
			WebElement tableBody = tableName.findElement(By.xpath(body));
			List<WebElement> tableRows = tableBody.findElements(By.xpath(rows));
			String actual = "";
			actual = tableRows.get(0).getText();
			if (!actual.contains(expected) == true) {
				APP_LOGS("Asserted Is Not First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + rows + " |");
			} else {
				APP_LOGS_FAIL("Asserted Is Not First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + rows + " |");
				assertEquals(returnActual, returnExpected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void assertIsNotFirstItemInDataRowTable(String parent_row, String child_column, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is Not First Item In Data Row Table: " + child_column + " |", true);
		String returnExpected = "Item Was Not The First In Data Table (True): ";
		String returnActual = "Item Was The First In Data Table (False): ";
		waitForPresenceOfElement(parent_row);
		waitForPresenceOfElement(child_column);
		try {
			WebElement element = driver().findElement(By.xpath(parent_row));
			WebElement row_list = element.findElement(By.xpath(child_column));
			String actual = "";
			actual = row_list.getText();
			if (!actual.contains(expected) == true) {
				APP_LOGS("Asserted Is Not First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + child_column + " |");
			} else {
				APP_LOGS_FAIL("Asserted Is Not First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + child_column + " |");
				assertEquals(returnActual, returnExpected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void assertIsFirstItemInDataRowTable(String parent_row, String child_column, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is First Item In Data Row Table: " + child_column + " |", true);
		waitForPresenceOfElement(parent_row);
		waitForPresenceOfElement(child_column);
		try {
			WebElement element = driver().findElement(By.xpath(parent_row));
			WebElement row_list = element.findElement(By.xpath(child_column));
			String actual = "";
			actual = row_list.getText();
			if (actual.contains(expected)) {
				APP_LOGS("Asserted Is First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + child_column + " |");
			} else {
				APP_LOGS_FAIL("Asserted Is First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + child_column + " |");
				assertEquals(actual, expected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void assertIsNotItemInListData(String locator, String tagname, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is Not List Items In Data Table: " + locator + " |", true);
		String returnExpected = "Item Was Not The First In Data Table (True): ";
		String returnActual = "Item Was The First In Data Table (False): ";
		waitForPresenceOfElement(locator);
		waitForPresenceOfElement(By.tagName(tagname));
		try {
			WebElement element = driver().findElement(By.xpath(locator));
			List<WebElement> rows_list = element.findElements(By.tagName(tagname));
			int rows_count = rows_list.size();

//			ArrayList<Object> listArray = new ArrayList<>();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < rows_count; i++) {
				sb.append("\n\t" + rows_list.get(i).getText());
//				listArray.add(rows_list.get(i).getText());
			}
			String actual = "";
			actual = sb.toString();
			if (!actual.contains(expected)) {
				APP_LOGS("Asserted Is Not List Item In Data Table: \n" + actual + " == " + expected + " --> At Locator: " + locator + " |");
			} else {
				APP_LOGS_FAIL("Asserted Is Not List Item In Data Table: \n" + actual + " == " + expected + " --> At Locator: " + locator + " |");
				assertEquals(returnActual, returnExpected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void assertIsItemInListData(String parent, String children, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is List Items In Data Table: " + parent + " |", true);
		waitForPresenceOfElement(parent);
		waitForPresenceOfElement(children);
		try {
			WebElement parentElement = driver().findElement(By.xpath(parent));
			List<WebElement> rows_list = parentElement.findElements(By.xpath(children));
			int rows_count = rows_list.size();

//			ArrayList<Object> listArray = new ArrayList<>();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < rows_count; i++) {
				sb.append("\n\t" + rows_list.get(i).getAttribute("textContent"));
//				listArray.add(rows_list.get(i).getText());
			}
			String actual = "";
			actual = sb.toString();
			if (actual.contains(expected)) {
				APP_LOGS("Assert Is List Items In Data Table: \n" + actual + " == " + expected + " --> At Locator: " + parent + " |");
			} else {
				APP_LOGS_FAIL("Assert Is List Items In Data Table: \n" + actual + " == " + expected + " --> At Locator: " + parent + " |");
				assertEquals(actual, expected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception
			APP_LOGS_FAIL(e.getMessage());
			fail(null, e);
		}
	}

	protected void assertIsListItemInDataGrid(String parent, String children, String[] expectedList) {

		Reporter.log("\n [INFO] - Executed | Assert Is List Items In Data Table: " + parent + " |", true);
		waitForPresenceOfElement(parent);
		waitForPresenceOfElement(children);
		try {
			WebElement parentElement = driver().findElement(By.xpath(parent));
			List<WebElement> rows_list = parentElement.findElements(By.xpath(children));
			int rows_count = rows_list.size();

			List<Object> actualData = new ArrayList<>();
			for (int i = 0; i < rows_count; i++) {
				actualData.add(rows_list.get(i).getAttribute("textContent"));
			}
			
			List<Object> expectedData = new ArrayList<>();
			for (int i = 0; i < expectedList.length; i++) {
				expectedData.add(expectedList[i]);
			}

			assertEquals(actualData, expectedData);

//			if (actual.contains(expected)) {
//				APP_LOGS("Assert Is List Items In Data Table: \n" + actual + " == " + expected + " --> At Locator: " + parent + " |");
//			} else {
//				APP_LOGS_FAIL("Assert Is List Items In Data Table: \n" + actual + " == " + expected + " --> At Locator: " + parent + " |");
//				assertEquals(actual, expected);
//			}
		} catch (WebDriverException e) {
			// TODO: handle exception
			APP_LOGS_FAIL(e.getMessage());
			fail(null, e);
		}
	}

	protected void assertTitleURL(String expectTitle, String expectUrl) {

		Reporter.log("\n [INFO] - Executed | Asserting Page Title: " + expectTitle + " |", true);
		Reporter.log("\n [INFO] - Executed | Asserting URL Navigation: " + expectUrl + " |", true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			fail(null, e);
		}
		(new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS)).until(ExpectedConditions.titleIs(expectTitle));
		String actualTitle = "";
		actualTitle = driver().getTitle();
		status = expectTitle.equals(actualTitle);
		if (status == true) {
			APP_LOGS("Asserted Page Title: " + actualTitle + " == " + expectTitle + " |");
		}
		(new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS)).until(ExpectedConditions.urlToBe(expectUrl));
		String actualUrl = "";
		actualUrl = driver().getCurrentUrl();
		status = expectUrl.equals(actualUrl);
		if (status == true) {
			APP_LOGS("Asserted URL Navigation: " + actualUrl + " == " + expectUrl + " |");
		} else {
			APP_LOGS_FAIL("Asserted Page Title: " + actualTitle + " == " + expectTitle + " |");
			APP_LOGS_FAIL("Asserted URL Navigation: " + actualUrl + " == " + expectUrl + " |");
			assertEquals(actualTitle, expectTitle);
			assertEquals(actualUrl, expectUrl);
		}
	}

	protected void assertHasNotDataInPageSource(String locator, String expected) {

		Reporter.log("\n [INFO] - Executed | Asserting Has Not Data: " + expected + " --> In Page Source: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		String pageSrc = "";
		pageSrc = driver().getPageSource();
		if (!pageSrc.contains(expected)) {
			APP_LOGS("Asserted List: " + locator + " --> There Is Not Contains: " + expected);
		} else {
			APP_LOGS_FAIL("Asserted Data List: " + locator + " --> There Is Not Contains: " + expected);
			assertFalse(pageSrc.contains(expected));
		}
	}

	protected void assertHasDataInPageSource(String locator, String expected) {

		Reporter.log("\n [INFO] - Executed | Asserting Has Data: " + expected + " --> In Page Source: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		String pageSrc = "";
		pageSrc = driver().getPageSource();
		if (pageSrc.contains(expected)) {
			APP_LOGS("Asserted Data List: " + locator + " --> There Is Contains: " + expected);
		} else {
			APP_LOGS_FAIL("Asserted Data List: " + locator + " --> There Is Contains: " + expected);
			assertTrue(pageSrc.contains(expected));
		}
	}

	// ===================================================
	// Code part used to handle waiting below
	// ===================================================

	public void waitForPresenceOfElement(String locator) {

		scrollIntoViewBottom(locator);
		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		status = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(locator)))) != null;
//			status = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator))) != null;
		if (status == true) {

		} else
			APP_LOGS_FAIL("Can Not To Determine Presence Of Element Located: " + locator + " |");
	}

	protected void waitForPresenceOfElement(By by) {

		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		status = wait.until(ExpectedConditions.presenceOfElementLocated(by)) != null;
//			status = wait.until(ExpectedConditions.visibilityOfElementLocated(by)) != null;
		if (status == true) {

		} else
			APP_LOGS_FAIL("Can Not To Determine Presence Of Element Located: " + by + " |");
	}

	protected boolean waitForElementToInvisible(String locator) {

		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
			return true;
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Can Not Determine Invisibility Of Element Located: " + locator + " |");
			fail(null, e);
			return false;
		}
	}

	protected void waitForElementToInvisible(By by) {

		wait = new WebDriverWait(driver(), WAIT_TIMEOUT_IN_SECONDS);
		try {
//			status = wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return (WebElement) ExpectedConditions.invisibilityOfElementLocated(by);
				}
			});
			if (status == true) {

			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Can Not Determine Invisibility Of Element Located: " + by + " |");
			fail(null, e);
		}
	}

	// =========================================================
	// Code part used to handle text field component below
	// =========================================================

	protected int sendKeysNumericBoundaryMaxLength(String locator, int boundary) {

		Reporter.log("\n [INFO] - Executed | Boundary Max Length Analysis: " + boundary + " |", true);
		try {
			WebElement element = driver().findElement(By.xpath(locator));
			String limit = "";
			limit = element.getAttribute("maxlength").toString();
			int maxLength = Integer.parseInt(limit);
			sendKeys(locator, CommonRandomCore.randomNumeric(maxLength + boundary));
			APP_LOGS("Boundary Max Length Analysed: " + boundary + "--> Get Value: " + maxLength + " |");
			return maxLength;
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
		return boundary;
	}

	public int sendKeysAlphaBoundaryMaxLength(String locator, int boundary) {

		Reporter.log("\n [INFO] - Executed | Boundary Max Length Analysis: " + boundary + " |", true);
		try {
			WebElement element = driver().findElement(By.xpath(locator));
			String limit = "";
			limit = element.getAttribute("maxlength").toString();
			int maxLength = Integer.parseInt(limit);
			sendKeys(locator, CommonRandomCore.randomAlphaNumeric(maxLength + boundary));
			APP_LOGS("Boundary Max Length Analysed: " + boundary + "--> Get Value: " + maxLength + " |");
			return maxLength;
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
		return boundary;
	}

	public SeleniumBaseCore sendKeysRandomUUID(String locator) {

		String uuid = UUID.randomUUID().toString();
		sendKeys(locator, uuid);
		return this;
	}

	public SeleniumBaseCore sendKeys(String locator, String value) {

		Reporter.log("\n [INFO] - Executed | Entering Input Text: " + value + " --> At Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			WebElement element = driver().findElement(By.xpath(locator));
			if (!element.isEnabled()) {
				APP_LOGS_FAIL("Textbox Field Is Disabled Instead Of Enabled: " + locator);
			}
			element.sendKeys(Keys.chord(Keys.CONTROL + "a"));
			element.sendKeys(Keys.chord(Keys.DELETE));
			element.sendKeys(value);
			APP_LOGS("Entered Input Text: " + value + " --> At Locator: " + locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Entered Input Text: " + value + " --> At Locator: " + locator + " |");
			fail(null, e);
		}
		return this;
	}

	public String sendKeysSelectInput(String locator, String value) {

		Reporter.log("\n [INFO] - Executed | Entering Input Text: " + value + " --> At Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			WebElement element = driver().findElement(By.xpath(locator));
			if (!element.isEnabled()) {
				APP_LOGS_FAIL("Textbox Field Is Disabled Instead Of Enabled: " + locator);
			}
			actions = new Actions(driver());
			actions.moveToElement(element).click();
			actions.sendKeys(value).build().perform();
			try {
				robot = new Robot();
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
			} catch (AWTException e) {
				// TODO Auto-generated catch block

				fail(null, e);
			}
			APP_LOGS("Entered Input Text: " + value + " --> At Locator: " + locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Entered Input Text: " + value + " --> At Locator: " + locator + " |");
			fail(null, e);
		}
		return value;
	}

}
