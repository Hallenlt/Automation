package framework.dataDriven;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelConfigReader {

	Workbook workbook;
	XSSFSheet sheet;

	public ExcelConfigReader(String path) {

		try {
			File src = new File(path);
			
			FileInputStream inputStream = new FileInputStream(src);
//			workbook = new XSSFWorkbook(inputStream);
			String extension = path.substring(path.lastIndexOf("."));

			if (extension.equals(".xlsx")) {
				workbook = new XSSFWorkbook(inputStream);
			}

			else if (extension.equals(".xls")) {
				workbook = new HSSFWorkbook(inputStream);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public String getData(int sheetNumber, int row, int column) {
		
		sheet = (XSSFSheet) workbook.getSheetAt(sheetNumber);
		
		String data = new DataFormatter().formatCellValue(sheet.getRow(row).getCell(column));
		
		return data;
	}
	
	public int getRowCount(int sheetIndex) {
		
		int row = workbook.getSheetAt(sheetIndex).getLastRowNum();
		row = row + 1;
		
		return row;
	}

}