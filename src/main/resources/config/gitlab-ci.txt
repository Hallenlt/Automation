e2e:chrome:
  stage: e2e-test
  services:
    - selenium/standalone-chrome
  script:
    - npm run e2e-test --host=selenium__standalone-chrome
    