package pattern.strategy;

import org.openqa.selenium.WebDriver;

public interface AuthStrategyInterface {

	void setDriver(WebDriver driver);

	void authorize(String searchFor);
}