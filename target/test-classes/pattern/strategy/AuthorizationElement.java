package pattern.strategy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import project.pim.center.SubTransferStator;

public class AuthorizationElement extends SubTransferStator {

	AuthStrategyInterface strategy;
	By result = By.className("rc");

	public AuthorizationElement(WebDriver driver, AuthStrategyInterface strategy) {

		this.driver = driver;
		this.strategy = strategy;
	}

	public void authorize(String txt) {
		this.strategy.authorize(txt);
	}

	public int getResultsCount() {
		return this.driver.findElements(result).size();
	}

}