package pattern.strategy;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class AuthStrategyTest extends SubTransferStator {

//	@Test(dataProvider = "getDataStrategy")
	public void googleSearchStrategy(AuthStrategyInterface strategy, String searchString, int resultCount) {

		AuthorizationElement auth = new AuthorizationElement(driver, strategy);
		auth.authorize(searchString);
		assertEquals(resultCount, auth.getResultsCount());
	}

	@DataProvider
	public Object[][] getDataStrategy() {
		return new Object[][] { new Object[] { new RoleAdminStrategy(), "Test Automation Guru", 7 } };
//				new Object[] { new VoiceStrategy(), "weather today", 9 }, new Object[] { new TextStrategy(), "Selenium WebDriver", 14 }, };
	}

}