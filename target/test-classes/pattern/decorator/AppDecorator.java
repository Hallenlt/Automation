package pattern.decorator;

public abstract class AppDecorator implements App {
    /**
     * This is the delegate to decorate. It is package-private so the subclasses can access it so we do not need
     * getters/setters and a constructor. However I would add one if I would write this decorator in my daily job.
     */
    App delegate;
}