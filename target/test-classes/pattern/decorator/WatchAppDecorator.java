package pattern.decorator;

public class WatchAppDecorator extends AppDecorator {

	/**
	 * A required constructor to set the delegate for this app.
	 *
	 * @param delegate the delegate which should be decorated.
	 */
	public WatchAppDecorator(App delegate) {
		this.delegate = delegate;
	}

	@Override
	public void developApp() {
		this.delegate.developApp();
		System.out.println("Adding Watch extension...");
	}
}