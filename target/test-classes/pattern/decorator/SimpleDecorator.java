package pattern.decorator;

public class SimpleDecorator implements App {

	final App delegate;

	/**
	 * A required constructor to set the delegate for this app.
	 *
	 * @param delegate the delegate which should be decorated.
	 */
	public SimpleDecorator(App delegate) {
		this.delegate = delegate;
	}

	@Override
	public void developApp() {
		System.out.println("Preparing extra content...");
		this.delegate.developApp();
		System.out.println("Fine-tuning the app to be more perfect...");
	}

}