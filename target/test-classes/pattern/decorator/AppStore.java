package pattern.decorator;

public class AppStore {

	public static void main(String... args) {

		final App tvApp = new TVAppDecorator(new IOSApp());
		tvApp.developApp();
		System.out.println("----- Adding TV Extension ----- \n");

		final App watchApp = new WatchAppDecorator(new IOSApp());
		watchApp.developApp();
		System.out.println("----- Adding Watch Extension ----- \n");

		final App perfectApp = new SimpleDecorator(new IOSApp());
		perfectApp.developApp();
		System.out.println("----- Adding Perfect Extension ----- \n");
	}
	
}