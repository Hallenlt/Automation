package pattern.chain;

interface ChainReposibilityInterface {

	public abstract void setNext(ChainReposibilityInterface nextInChain);

	public abstract void process(Number request);
}