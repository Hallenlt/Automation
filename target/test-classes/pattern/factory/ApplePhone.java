package pattern.factory;

public class ApplePhone implements Phone {

	@Override
	public void showInfo() {
		System.out.printf("Đây là điện thoại Apple");
	}

}
