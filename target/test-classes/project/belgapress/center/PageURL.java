package project.belgapress.center;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;

public class PageURL extends SeleniumBaseCore {

	public static final String IP_CONFIG = "http://192.168.100.4:6969";
//	public static final String IP_CONFIG = "http://10.10.10.102:8181";

	public static final String PATH_404 = "/pages/errors/error-404";
	public static final String PATH_BELGA_TEXT_USAGE = "/belga-text-usage";
	public static final String PATH_PUBLISHER_TEXT_USAGE = "/publisher";
	public static final String PATH_STATISTICS = "/statistics";

	public PageURL(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	// Assert title - EN
	
	public void clickOn() {
		click("//*[@id=\"root\"]/header/nav/div/div[2]/div[2]/a");
	}

	public PageURL titleUrlAssert_404Error() {
		assertTitleURL("Error Page 404", IP_CONFIG + PATH_404);
		return this;
	}

	public PageURL titleUrlAssert_BelgaTextUsage() {
		assertTitleURL("Belga Press Monitoring - Text Usage", IP_CONFIG + PATH_BELGA_TEXT_USAGE);
		return this;
	}
	
	public PageURL titleUrlAssert_PublisherTextUsage() {
		assertTitleURL("Belga Press Monitoring - Publisher", IP_CONFIG + PATH_BELGA_TEXT_USAGE);
		return this;
	}
	
	public PageURL titleUrlAssert_Statistics() {
		assertTitleURL("Belga Press Monitoring - Statistics", IP_CONFIG + PATH_BELGA_TEXT_USAGE);
		return this;
	}

	// Redirection

	public PageURL redirectTo_BelgaTextUsage() {
		clickHref("//a[@href='" + PATH_BELGA_TEXT_USAGE + "']");
		return this;
	}

	public PageURL redirectTo_PublisherTextUsage() {
		clickHref("//a[@href='" + PATH_PUBLISHER_TEXT_USAGE + "']");
		return this;
	}

	public PageURL redirectTo_Statistics() {
		clickHref("//a[@href='" + PATH_STATISTICS + "']");
		return this;
	}

	// Navigation

	public PageURL navigateTo_BelgaTextUsage() {
		navigateToURL(IP_CONFIG + PATH_BELGA_TEXT_USAGE);
		return this;
	}

	public PageURL navigateTo_PublisherTextUsage() {
		navigateToURL(IP_CONFIG + PATH_PUBLISHER_TEXT_USAGE);
		return this;
	}

	public PageURL navigateTo_Statistics() {
		navigateToURL(IP_CONFIG + PATH_STATISTICS);
		return this;
	}

	// header section

	// footer section

}
