package project.belgapress.object;

public class ElementUtils {

	public static final String SEARCH_BUTTON_ELEMENT = "//input[@type='submit']";

	public static final String NO_RESULTS_TABLE_ELEMENT = "//span[@class='row justify-content-center table-no-result']";

	public static final String ALL_CHECKBOX_ELEMENT = "//*[text()='All']";

	public static final String DATE_PICKER_ELEMENT = "//i[@class='fa  fa-calendar icon-date-picker mr-1']";

	public static final String AMOUNT_RESULTS_ELEMENT = "//form/div[2]/span[1]";

	public static final String BASIC_ANALYSIS_BUTTON_ELEMENT = "//section[2]/div/button";
	
	public static final String SHOW_TABLE_ELEMENT = "//div[@class='showTable section']";
	
	public static final String HIDE_TABLE_ELEMENT = "//div[@class='hideTable section']";

}