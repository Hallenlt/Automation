package project.belgapress.object.restapi;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.reset;

import com.github.fge.jsonschema.SchemaVersion;
import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.module.jsv.JsonSchemaValidatorSettings;
import io.restassured.specification.ResponseSpecification;

public class StagingAPI {

	static ResponseSpecification expectedResponse = new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();

	static final int PORT = 8080;

	static final String BASE_URI = "http://belga-monitorings-01.staging.belga.be:8080/belgamonitoring/api";

	// Staging Server

	static final String DOCS_API = "/v3/api-docs";

	public static final String ALIVE_API = "/alive";

	public static final String NEWS_OBJECT_API = "/newsobjects";

	public static final String NEWSOBJECT_PERDESKS_API = "/newsobjects/perDesk?";

	public static final String CONTENT_DERIVATIVES_API = "/content/belga/derivatives?";

	public static final String CONTENT_ORIGINALS_API = "/content/originals?";

	public static void configBaseURI() {

		reset();
		baseURI = BASE_URI;

		given().config(config().logConfig(LogConfig.logConfig().enableLoggingOfRequestAndResponseIfValidationFails())).baseUri(BASE_URI)

				.when().get(BASE_URI + DOCS_API)

				.then().assertThat().spec(expectedResponse)

				.and().log().ifValidationFails();
	}

	public static void configSchemaValidator() {

		JsonSchemaValidator.reset();
		JsonSchemaFactory factory = JsonSchemaFactory.newBuilder()
				.setValidationConfiguration(ValidationConfiguration.newBuilder().setDefaultVersion(SchemaVersion.DRAFTV3).freeze()).freeze();

		JsonSchemaValidator.settings = JsonSchemaValidatorSettings.settings().with().jsonSchemaFactory(factory).and().with().checkedValidation(false);
	}

}
