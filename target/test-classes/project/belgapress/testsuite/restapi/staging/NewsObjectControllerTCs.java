package project.belgapress.testsuite.restapi.staging;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import project.belgapress.center.SubTransferStator;
import project.belgapress.object.restapi.StagingAPI;

public class NewsObjectControllerTCs extends SubTransferStator {

	@BeforeTest
	public void setBaseAPI() {

		StagingAPI.configBaseURI();
	}

	// NewsObjects_ByID

	@Test
	public void GET_Keywords_Size_GreaterThan_2_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and()
				.body("keywords.size()", greaterThan(2));
	}

	@Test
	public void GET_Tags_Size_GreaterThan_1_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("tags.size()",
				greaterThan(1));
	}

	@Test
	public void GET_Size_Items_Equal_14_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("size()",
				equalTo(14));
	}

	@Test
	public void GET_Id_Notnull_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("id",
				notNullValue());
	}

	@Test
	public void GET_CorrelationId_Notnull_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("correlationId",
				notNullValue());
	}

	@Test
	public void GET_Title_Notnull_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("title",
				notNullValue());
	}

	@Test
	public void GET_Lead_Notnull_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("lead",
				notNullValue());
	}

	@Test
	public void GET_Body_Notnull_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("body",
				notNullValue());
	}

	@Test
	public void GET_PublishDate_Notnull_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("publishDate",
				notNullValue());
	}

	@Test
	public void GET_Source_Notnull_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("source",
				notNullValue());
	}

	@Test
	public void GET_Publisher_Nullable_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("publisher",
				nullValue());
	}

	@Test
	public void GET_SourceLogoUrn_Nullable_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and().body("sourceLogoUrn",
				nullValue());
	}

	@Test
	public void GET_MediumTypeGroup_isOneOf_3_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and()
				.body("mediumTypeGroup", isOneOf("PRINT", "BELGA", "ONLINE"));
	}

	@Test
	public void GET_Language_Notnull_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and()
				.body("mediumTypeGroup", notNullValue());
	}

	@Test
	public void GET_Categories_Array_NotEmpty_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and()
				.body("mediumTypeGroup", not(empty()));
	}

	@Test
	public void GET_Tags_Array_NotEmpty_NewsObjects_ByID() {

		given().request(StagingAPI.NEWS_OBJECT_API);
		when().get(StagingAPI.NEWS_OBJECT_API + "/urn:www.belga.be:ingest:opoint:24395_867923").then().assertThat().statusCode(200).and()
				.body("mediumTypeGroup", not(empty()));
	}

	// NewsObjects_PerDesk

	@Test
	public void GET_Notnull_NewsObjects_PerDesk() {

		given().request(StagingAPI.NEWSOBJECT_PERDESKS_API);
		when().get(StagingAPI.NEWSOBJECT_PERDESKS_API + "from=2020-07-01&to=2020-08-31").then().assertThat().statusCode(200).and().body(notNullValue());
	}

	@Test
	public void GET_Size_Equal_6_NewsObjects_PerDesk() {

		given().request(StagingAPI.NEWSOBJECT_PERDESKS_API);
		when().get(StagingAPI.NEWSOBJECT_PERDESKS_API + "from=2020-07-01&to=2020-08-31").then().assertThat().statusCode(200).and().body("size()", equalTo(6));
	}

	@Test
	public void GET_SPF_Notnull_Value_NewsObjects_PerDesk() {

		given().request(StagingAPI.NEWSOBJECT_PERDESKS_API);
		when().get(StagingAPI.NEWSOBJECT_PERDESKS_API + "from=2020-07-01&to=2020-08-31").then().assertThat().statusCode(200).and().body("SPF", notNullValue());
	}

	@Test
	public void GET_INT_Notnull_Value_NewsObjects_PerDesk() {

		given().request(StagingAPI.NEWSOBJECT_PERDESKS_API);
		when().get(StagingAPI.NEWSOBJECT_PERDESKS_API + "from=2020-07-01&to=2020-08-31").then().assertThat().statusCode(200).and().body("INT", notNullValue());
	}

	@Test
	public void GET_BTL_Notnull_Value_NewsObjects_PerDesk() {

		given().request(StagingAPI.NEWSOBJECT_PERDESKS_API);
		when().get(StagingAPI.NEWSOBJECT_PERDESKS_API + "from=2020-07-01&to=2020-08-31").then().assertThat().statusCode(200).and().body("BTL", notNullValue());
	}

	@Test
	public void GET_SPN_Notnull_Value_NewsObjects_PerDesk() {

		given().request(StagingAPI.NEWSOBJECT_PERDESKS_API);
		when().get(StagingAPI.NEWSOBJECT_PERDESKS_API + "from=2020-07-01&to=2020-08-31").then().assertThat().statusCode(200).and().body("SPN", notNullValue());
	}

	@Test
	public void GET_EXT_Notnull_Value_NewsObjects_PerDesk() {

		given().request(StagingAPI.NEWSOBJECT_PERDESKS_API);
		when().get(StagingAPI.NEWSOBJECT_PERDESKS_API + "from=2020-07-01&to=2020-08-31").then().assertThat().statusCode(200).and().body("EXT", notNullValue());
	}

	@Test
	public void GET_BIN_Notnull_Value_NewsObjects_PerDesk() {

		given().request(StagingAPI.NEWSOBJECT_PERDESKS_API);
		when().get(StagingAPI.NEWSOBJECT_PERDESKS_API + "from=2020-07-01&to=2020-08-31").then().assertThat().statusCode(200).and().body("BIN", notNullValue());
	}

}