package project.belgapress.testsuite.restapi;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import project.belgapress.object.model.POJOModel;

public class JsonMappingTCs {

	public List<POJOModel> getNewsObjectsFromApi(String urlStr) {

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		mapper.configure(JsonReadFeature.ALLOW_UNESCAPED_CONTROL_CHARS.mappedFeature(), true);
		List<POJOModel> newsObjects = new ArrayList<>();
		try {
			// if (!StringUtils.isEmpty(appConfiguration.getBelgaApiUrl()) &&
			// urlStr.contains("originals")) {
			
			if (!StringUtils.isEmpty("")) {
				
				URL url = new URL(urlStr);
				System.out.println(urlStr);
				newsObjects = mapper.readValue(url, new TypeReference<List<POJOModel>>() {
				});
				
			} else {
				
				String filePath = "";
				if (urlStr.contains("originals")) {
					filePath = "json/publisher-usage-2020-08-11(23343_items).json";
					
				} else {
					filePath = "json/belga-usage-2020-08-11(24514_items)-generated.json";
				}
				
				File file = new ClassPathResource(filePath).getFile();
				newsObjects = mapper.readValue(file, new TypeReference<List<POJOModel>>() {
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newsObjects.stream().filter(e -> !CollectionUtils.isEmpty(e.getChildKeys())).collect(Collectors.toList());
	}

}
