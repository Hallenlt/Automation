package project.belgapress.testsuite.restapi.local;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.hasItem;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import project.belgapress.center.SubTransferStator;
import project.belgapress.object.restapi.LocalAPI;

public class LanguagesControllerTCs extends SubTransferStator {

	@BeforeTest
	public void setBaseURI() {

		LocalAPI.configBaseURI();
	}

	@Test
	public void request_GET() {

		given().request(LocalAPI.LANGUAGE_API);
		when().get(LocalAPI.LANGUAGE_API).then().assertThat().statusCode(200).and().body("value",
				hasItem("EN"), "label", hasItem("EN"));
	}

}