package project.belgapress.testsuite.restapi.local;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import java.io.File;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import net.minidev.json.JSONObject;
import project.belgapress.center.SubTransferStator;
import project.belgapress.object.restapi.LocalAPI;

public class StatisticControllerTCs extends SubTransferStator {

	@BeforeTest
	public void setBaseURI() {

		LocalAPI.configBaseURI();
	}

	@Test
	public void request_GET() {

		given().request("");
		when().get("").then().assertThat().statusCode(200).and().body("Title", hasItem("Activity 1"));
	}

	@Test
	public void POST_JsonData() {

		File jsonData = new File("");

		given().contentType(ContentType.JSON).body(jsonData).when().post().then().assertThat().statusCode(200).and().body("Title", is("Singh"));
	}

	@Test
	public void request_POST() {

		JSONObject objectParams = new JSONObject();
		objectParams.put("ID", "0");
		objectParams.put("Title", "Singh");
		objectParams.put("DueDate", "2020-07-01T07:30:07.146Z");
		objectParams.put("Completed", true);

		given().contentType(ContentType.JSON).body(objectParams).when().post("").then().assertThat().statusCode(200).and().body("Title", is("Singh"));
	}

	@Test
	public void request_PUT() {

		// JSONObject is a class that represents a Simple JSON.
		// We can add Key - Value pairs using the put method
		JSONObject objectParams = new JSONObject();
		objectParams.put("ID", "1");
		objectParams.put("Title", "Singh");
		objectParams.put("DueDate", "2020-07-01T07:30:07.146Z");
		objectParams.put("Completed", false);

		given().contentType(ContentType.JSON).body(objectParams).when().put("" + 1).then().assertThat().statusCode(200).and().body("Title",
				equalTo("Singh"));
	}

	@Test
	public void request_DELETE() {

		given().header("Content-Type", ContentType.JSON);

		// Delete the request and check the response
		given().contentType(ContentType.JSON).formParam("ID", "1").when().delete("" + 1).then().assertThat().statusCode(200);
	}

}