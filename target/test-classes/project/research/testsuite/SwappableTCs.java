package project.research.testsuite;

import java.util.Arrays;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class SwappableTCs extends SubTransferStator {

	@Test(dataProvider = "number-inputs")
	public void intMove(int fromIndex, int toIndex, List<String> expectedOrder) {
		swappable.goTo();
		swappable.getswappables().reorder(fromIndex, toIndex);
		Assert.assertEquals(expectedOrder, swappable.getswappables().getItems());
	}

	@Test(dataProvider = "string-inputs")
	public void stringMove(String fromItem, String toItem, List<String> expectedOrder) {
		swappable.goTo();
		swappable.getswappables().reorder(fromItem, toItem);
		Assert.assertEquals(expectedOrder, swappable.getswappables().getItems());
	}

	@DataProvider(name = "number-inputs")
	public static Object[][] getNumberInputs() {
		return new Object[][] { { 0, 2, Arrays.asList("Item 2", "Item 3", "Item 1", "Item 4", "Item 5", "Item 6", "Item 7") },
				{ 1, 3, Arrays.asList("Item 1", "Item 3", "Item 4", "Item 2", "Item 5", "Item 6", "Item 7") } };
	}

	@DataProvider(name = "string-inputs") 
	public static Object[][] getStringInputs() {
		return new Object[][] { { "Item 3", "Item 4", Arrays.asList("Item 1", "Item 2", "Item 4", "Item 3", "Item 5", "Item 6", "Item 7") },
				{ "Item 1", "Item 7", Arrays.asList("Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7", "Item 1") } };
	}

}