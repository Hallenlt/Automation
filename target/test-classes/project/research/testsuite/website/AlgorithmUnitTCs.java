package project.research.testsuite.website;

import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class AlgorithmUnitTCs extends SubTransferStator {

	protected int num = 9999;
	protected int[] arrNum = { 1, 2, 3, 4, 5, 6, 7, 8 };
	protected String str = "a B c";
	protected String str1 = "c B a";
	protected String[] arrStr = { "Stability", "Recovery", "Usability", "Supportability", "Scalability", "Reliability",
			"Accessibility", "Interoperability", "Adaptability", "Reusability", "Durability", "Affordability",
			"Portability", "Security" };

	@Test
	public void StringReverse() {
		
		algorithm.stringReverse(str);
	}

	@Test
	public void stringInvert() {
		
		algorithm.stringInvert(str);
	}

	@Test
	public void numberReverse() {

		algorithm.numberReverse(arrNum);
	}

	@Test
	public void reverseWords() {

		algorithm.ReverseWords(str);
	}

	@Test
	public void stringPalindrome() {

		algorithm.stringPalindrome(str);
	}

	@Test
	public void numberPalindrome() {

		algorithm.numberPalindrome(num);
	}

	@Test
	public void maxArray() {

		algorithm.maxArray(arrNum);
	}

	@Test
	public void secondMax() {

		algorithm.minArray(arrNum);
	}

	@Test
	public void secondMin() {

		algorithm.secondMin(arrNum);
	}

	@Test
	public void twoStringAnagram() {

		algorithm.twoStringAnagram(str, str1);
	}

	@Test
	public void removeDuplicates() {

		algorithm.removeDuplicates(str);
	}

	@Test
	public void removeDuplicated() {

		algorithm.removeDuplicated(str);
	}

	@Test
	public void removeDuplicatesArray() {

		algorithm.removeDuplicatesArray(arrStr);
	}

	@Test
	public void countLetters() {

		algorithm.countLetters(str);
	}

	@Test
	public void fizzBuzz() {

		algorithm.fizzBuzz();
	}

	@Test
	public void evenOrOdd() {

		algorithm.evenOrOdd(num);
	}

	@Test
	public void sumOfOnePair() {

		algorithm.sumOfOnePair(arrNum, 8);
	}

	@Test
	public void permutation() {

		algorithm.permutation(str, str1);
	}

	@Test
	public void moveZerosToEnd() {

		algorithm.moveZerosToEnd(arrNum);
	}

	@Test
	public void countCharacterInString() {

		algorithm.countCharacterInString(256, str);
	}

	@Test
	public void replaceSubstring() {

		algorithm.replaceSubstring(str, "a", "z");
	}

	@Test
	public void largestValueArray() {

		algorithm.largestValueArray(arrNum);
	}

	@Test
	public void dictionaryOrder() {

		algorithm.dictionaryOrder(arrStr);
	}

	@Test
	public void frequencyOfCharacter() {

		algorithm.frequencyOfCharacter(str, 'c');
	}

}