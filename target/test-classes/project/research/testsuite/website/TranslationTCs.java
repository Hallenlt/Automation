package project.research.testsuite.website;

import java.io.IOException;

import org.testng.annotations.Test;

import data.ProviderData;
import infrastructure.LocalizationReader;

public class TranslationTCs {

	@Test(dataProvider = "languages", dataProviderClass = ProviderData.class)
	public void readTesting(String language) throws IOException {

		System.out.println("Translation for key 'testing' in language '" + language + "' is : " + LocalizationReader.translateText("testing", language));
	}

	@Test(dataProvider = "languages", dataProviderClass = ProviderData.class)
	public void readSuperheroes(String language) throws IOException {

		System.out.println("For language '" + language + "' - superhero firstname and lastname: "
				+ LocalizationReader.translateText("superhero.firstname", language) + " " + LocalizationReader.translateText("superhero.lastname", language));
	}

}