package project.research.testsuite.website;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import infrastructure.selenium.ObjectMapCore;
import infrastructure.utility.DirectoryUtils;
import project.pim.center.SubTransferStator;

public class MapObjectTCs extends SubTransferStator {

	@Test
	public void mapObject() throws IOException {

		selenium.navigateToURL("https://www.google.com");

		// Get object map file
		objmap = new ObjectMapCore(DirectoryUtils.RESOURCE_PATH + "\\object-properties\\objectmap.properties");

		WebElement textbox = driver().findElement(objmap.getLocator("Textbox"));
		textbox.sendKeys("admin@idsolutions.com.vn");
	}

}