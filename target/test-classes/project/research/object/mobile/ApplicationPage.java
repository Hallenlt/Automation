package project.research.object.mobile;

import java.util.List;

import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;

import infrastructure.appium.AppiumBaseCore;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class ApplicationPage extends AppiumBaseCore {

	public ApplicationPage(AppiumDriver<MobileElement> appDriver) {
		super(appDriver);
		PageFactory.initElements(new AppiumFieldDecorator(appDriver), this);
	}

	@CacheLookup
	@iOSXCUITFindBy()
	@AndroidFindBy(xpath = "//div[@class='_highlighter-box_lf61n_125']/[@id='']")
	List<MobileElement> elements;

	@CacheLookup
	@iOSXCUITFindBy()
	@AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"Notifications\"]")
	MobileElement NotificationsLink;

	@CacheLookup
	@iOSXCUITFindBy()
	@AndroidFindBy(id = "com.android.vending:id/main_nav_item")
	MobileElement mainNavigationBar;

	@CacheLookup
	@iOSXCUITFindBy()
	@AndroidFindBy(id = "com.android.vending:id/search_bar_text_input")
	MobileElement searchBar;

	@CacheLookup
	@iOSXCUITFindBy()
	@AndroidFindBy(tagName = "com.android.vending:id/bucket_items")
	MobileElement parentCategoriesList;

	@CacheLookup
	@iOSXCUITFindBy()
	@AndroidFindBy(id = "com.android.vending:id/category_item_title")
	MobileElement childCategoriesList;

	@CacheLookup
	@iOSXCUITFindBy()
	@AndroidFindBy(id = "com.android.vending:id/category_item_title")
	List<MobileElement> categoriesList;

	@CacheLookup
	@iOSXCUITFindBy(xpath = "")
	@AndroidFindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.TextView[4]")
	MobileElement categoriesLink;

	public ApplicationPage clickFirstItemInList() {
		clickFirstItemInList(categoriesList);
		return this;
	}

	public ApplicationPage assertIsFirstItemInDataRowTable() {
		assertIsFirstItemInDataRowTable(parentCategoriesList, childCategoriesList, "Action");
		return this;
	}

	public ApplicationPage assertIsListItemsInDataTable() {
		assertIsListItemsInDataTable(categoriesList, "Adventure");
		return this;
	}

	public ApplicationPage navigateTo_Account() {
		click(NotificationsLink);
		return this;
	}

	public ApplicationPage clickOn_MainNavigationBar() {
		click(mainNavigationBar);
		return this;
	}

	public ApplicationPage input_Search() {
		sendKeys(searchBar, "content");
		return this;
	}

	public ApplicationPage getTextSearchBox() {
		getTextAttribute(searchBar);
		return this;
	}

	public ApplicationPage navigateTo_Categories() {
		click(categoriesLink);
		return this;
	}

}