package project.pim.testsuite.integration;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class InformationManagementE2E extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

	}

//	@Test(priority = 550)
//	public void Add_New_Product_Family_Then_Retrieve_Data_In_New_Product() {
//
//		url.navigateTo_ProductFamilies();
//
//		String[] componentArray = families.getComponentArray();
//
//		families.clickOn_Create();
//
//		selenium.sendKeys(componentArray[0], faker.team().name());
//		selenium.sendKeys(componentArray[1], faker.book().title());
//		selenium.sendKeysRandomUUID(componentArray[2]);
//
//		String title = selenium.getTextAttribute(componentArray[0]);
//		String code = selenium.getTextAttribute(componentArray[2]);
//		String item = title + "(" + code + ")";
//
//		families.clickOn_Save();
//		families.savedMessageIsDisplayed();
//
//		url.navigateTo_NewProduct();
//		products.assert_HasItemInListProductFamily(item);
//	}
//
//	@Test(priority = 551)
//	public void Add_New_Attribute_Then_Retrieve_Data_In_New_Product_Family() {
//
//		url.navigateTo_SettingsAttributes();
//
//		attributes.clickOn_Create();
//		attributes.clickOn_MultiSelectType();
//
//		String[] componentArray = attributes.getComponentArray();
//
//		selenium.sendKeysRandomUUID(componentArray[0]);
//		selenium.sendKeys(componentArray[1], faker.leagueOfLegends().masteries());
//		attributes.selectList_Group();
//		String name = selenium.getTextAttribute(componentArray[1]);
//
//		attributes.clickOn_Save();
//		attributes.savedMessageIsDisplayed();
//
//		url.navigateTo_ProductFamilies();
//		families.clickOn_Create();
//		url.navigateTo_FamilyAttributes();
//		families.assert_HasItemInListAttributes(name);
//	}
//
//	@Test(priority = 552)
//	public void Create_New_Category_Then_Retrieve_Data_In_New_Product() {
//
//		url.navigateTo_Categories();
//		categories.clickOn_Create();
//
//		String[] componentArray = categories.getComponentArray();
//
//		selenium.sendKeys(componentArray[0], faker.artist().name());
//		String name = selenium.getTextAttribute(componentArray[0]);
//
//		categories.clickOn_Save();
//		categories.savedMessageIsDisplayed();
//
//		url.navigateTo_NewProduct();
//		products.assert_HasItemInListCategory(name);
//	}
//
//	@Test(priority = 553)
//	public void Create_New_Brand_Then_Retrieve_Data_In_New_Product() {
//
//	}
//
//	@Test(priority = 554)
//	public void Modify_Present_Product_Family_Then_Verify_Data_In_Product() {
//
//		url.navigateTo_ProductFamilies();
//
//		String[] componentArray = families.getComponentArray();
//
//		families.clickOn_FirstItemInRowTable();
//
//		selenium.sendKeys(componentArray[0], faker.team().name());
//
//		String title = selenium.getTextAttribute(componentArray[0]);
//		String code = selenium.getTextAttribute(componentArray[2]);
//		String item = title + "(" + code + ")";
//
//		families.clickOn_Save();
//		families.savedMessageIsDisplayed();
//
//		url.navigateTo_NewProduct();
//		products.assert_HasItemInListProductFamily(item);
//	}
//
//	@Test(priority = 555)
//	public void Modify_Present_Attribute_Then_Verify_Data_In_Product_Family() {
//
//		url.redirectTo_SettingsAttributes();
//
//		attributes.clickOn_FirstItemInRowTable();
//
//		String[] componentArray = attributes.getComponentArray();
//
//		selenium.sendKeys(componentArray[1], faker.leagueOfLegends().masteries());
//		String name = selenium.getTextAttribute(componentArray[1]);
//
//		attributes.clickOn_Update();
//		attributes.updatedMessageIsDisplayed();
//
//		url.navigateTo_ProductFamilies();
//		families.clickOn_Create();
//		url.navigateTo_FamilyAttributes();
//		families.assert_HasItemInListAttributes(name);
//	}
//
//	@Test(priority = 556)
//	public void Edit_Existing_Category_Then_Verify_Data_In_Product() {
//
//		url.navigateTo_Categories();
//
//		categories.clickOn_EditFirstItemInTree();
//
//		String[] componentArray = categories.getComponentArray();
//
//		selenium.sendKeys(componentArray[0], faker.artist().name());
//		String name = selenium.getTextAttribute(componentArray[0]);
//
//		categories.clickOn_Save();
//		categories.updatedMessageIsDisplayed();
//
//		url.navigateTo_NewProduct();
//		products.assert_HasItemInListCategory(name);
//	}
//
//	@Test(priority = 557)
//	public void Edit_Existing_Brand_Then_Verify_Data_In_Product() {
//
//	}
//
//	@Test(priority = 558)
//	public void Remove_Product_Family_Then_Verify_Data_In_Product() {
//
//		url.navigateTo_ProductFamilies();
//
//		String[] componentArray = families.getComponentArray();
//
//		families.clickOn_FirstItemInRowTable();
//
//		String title = selenium.getTextAttribute(componentArray[0]);
//		String code = selenium.getTextAttribute(componentArray[2]);
//		String item = title + "(" + code + ")";
//
//		families.clickOn_Delete();
//		families.deletedMessageIsDisplayed();
//
//		url.navigateTo_NewProduct();
//		products.assert_HasNotItemInListProductFamily(item);
//	}
//
//	@Test(priority = 559)
//	public void Remove_Attribute_Then_Verify_Data_In_Product_Family() {
//
//		url.redirectTo_SettingsAttributes();
//
//		String[] componentArray = attributes.getComponentArray();
//
//		attributes.clickOn_FirstItemInRowTable();
//
//		String name = selenium.getTextAttribute(componentArray[1]);
//
//		attributes.clickOn_Delete();
//		attributes.deletedMessageIsDisplayed();
//
//		url.navigateTo_ProductFamilies();
//		families.clickOn_Create();
//		url.navigateTo_FamilyAttributes();
//		families.assert_HasNotItemInListAttributes(name);
//	}
//
//	@Test(priority = 560)
//	public void Delete_Category_Then_Assert_Data_In_Product() {
//
//		url.navigateTo_Categories();
//
//		String[] componentArray = categories.getComponentArray();
//
//		categories.clickOn_EditFirstItemInTree();
//
//		String name = selenium.getTextAttribute(componentArray[0]);
//
//		categories.clickOn_Delete();
//		categories.deletedMessageIsDisplayed();
//
//		url.navigateTo_NewProduct();
//		products.assert_HasNotItemInListCategory(name);
//	}
//
//	@Test(priority = 561)
//	public void Delete_Brand_Then_Verify_Data_In_Product() {
//
//	}
//
//	@Test(priority = 562)
//	public void Inactive_Product_Family_Then_Verify_Data_In_Product() {
//
//		url.navigateTo_ProductFamilies();
//
//		String[] componentArray = families.getComponentArray();
//
//		families.clickOn_FirstItemInRowTable();
//
//		families.check_StatusCheckbox();
//		String title = selenium.getTextAttribute(componentArray[0]);
//		String code = selenium.getTextAttribute(componentArray[2]);
//		String item = title + "(" + code + ")";
//
//		families.clickOn_Save();
//		families.savedMessageIsDisplayed();
//
//		url.navigateTo_NewProduct();
//		products.assert_HasNotItemInListProductFamily(item);
//	}
//
//	@Test(priority = 563)
//	public void Inactive_Attribute_Then_Verify_Data_In_Product_Family() {
//
//		url.navigateTo_SettingsAttributes();
//
//		String[] componentArray = attributes.getComponentArray();
//
//		attributes.clickOn_FirstItemInRowTable();
//
//		attributes.check_StatusCheckbox();
//		String name = selenium.getTextAttribute(componentArray[1]);
//
//		attributes.clickOn_Update();
//		attributes.updatedMessageIsDisplayed();
//
//		url.navigateTo_ProductFamilies();
//		families.clickOn_Create();
//		url.navigateTo_FamilyAttributes();
//		families.assert_HasNotItemInListAttributes(name);
//	}

	@Test(priority = 564)
	public void Inactive_Category_Then_Verify_Data_In_Product() {

		url.redirectTo_Categories();

		String[] componentArray = categories.getComponentArray();

		categories.clickOn_EditFirstItemInTree();

		categories.check_StatusCheckbox();
		String name = selenium.getTextAttribute(componentArray[0]);

		categories.clickOn_Save();
		categories.updatedMessageIsDisplayed();

		url.navigateTo_NewProduct();
		products.assert_HasNotItemInListCategory(name);
	}

	@Test(priority = 565)
	public void Inactive_Brand_Then_Verify_Data_In_Product() {

	}

}