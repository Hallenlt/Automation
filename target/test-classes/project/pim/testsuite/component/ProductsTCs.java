package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import data.ProviderData;
import infrastructure.root.CommonRandomCore;
import project.pim.center.SubTransferStator;

public class ProductsTCs extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

		url.navigateTo_Products();
	}

	// [0]Name [1]Description [2]LongDescription [3]Status [4]ProductFamily
	// [5]Brand [6]Categories [7]Tags

	@Test(priority = 451)
	public void Create_New_Product_Item_With_Basic_Info() {

		products.clickOn_Create();

		String[] componentArray = products.getComponentArray();

		selenium.sendKeys(componentArray[0], faker.book().title());
		products.selectList_Status();
		products.selectList_ProductFamily();
		products.selectList_Brand();
		selenium.sendKeys(componentArray[1], faker.commerce().material());
		selenium.sendKeys(componentArray[2], faker.commerce().department());
		products.selectFirstList_Categories();
		selenium.sendKeysSelectInput(componentArray[7], faker.commerce().color());
		selenium.click(componentArray[7]);

		validate.getInputData(componentArray);
		products.clickOn_Save();

		products.savedMessageIsDisplayed();
		products.clickOn_FirstItemInRowTable();
		products.statusCheckboxIsActive();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

	@Test(priority = 452)
	public void Edit_Existing_Product_Item_With_Basic_Info() {

		String[] componentArray = products.getComponentArray();

		products.clickOn_FirstItemInRowTable();

		products.check_StatusCheckbox();
		selenium.sendKeys(componentArray[0], faker.book().title());
		products.selectList_Status();
		products.selectList_ProductFamily();
		products.selectList_Brand();
		selenium.sendKeys(componentArray[1], faker.commerce().material());
		selenium.sendKeys(componentArray[2], faker.commerce().department());
		products.selectSecondList_Categories();
		selenium.sendKeysSelectInput(componentArray[7], faker.commerce().color());
		selenium.click(componentArray[7]);

		validate.getInputData(componentArray);
		products.clickOn_Save();

		products.savedMessageIsDisplayed();
		products.clickOn_FirstItemInRowTable();
		products.statuscheckboxIsInactive();

		
		validate.getOutputData(componentArray);
		validate.assertData();
	}

	@Test(priority = 453)
	public void Add_Pricing_When_Creating_New_Product_Item() {

		products.clickOn_Create();

		String[] componentArray = products.getComponentArray();

		selenium.sendKeys(componentArray[0], faker.book().title());
		products.selectList_ProductFamily();
		products.selectList_Brand();
		products.selectFirstList_Categories();

		url.navigateTo_Pricing();

		// [0]PriceExcludedTax [1]PriceIncludedTax
		String[] pricingArray = products.getPricingArray();

		selenium.sendKeys(pricingArray[0], CommonRandomCore.randomNumericNotZero(7));
		selenium.sendKeys(pricingArray[1], CommonRandomCore.randomNumericNotZero(7));

		validate.getInputData(pricingArray);
		products.clickOn_Save();

		products.savedMessageIsDisplayed();
		products.clickOn_FirstItemInRowTable();
		url.navigateTo_Pricing();

		validate.getOutputData(pricingArray);
		validate.assertData();
	}

	@Test(priority = 454)
	public void Modify_Pricing_In_Existing_Product_Item() {

		products.clickOn_FirstItemInRowTable();

		// [0]PriceExcludedTax [1]PriceIncludedTax
		String[] pricingArray = products.getPricingArray();

		url.navigateTo_Pricing();

		selenium.sendKeys(pricingArray[0], CommonRandomCore.randomNumericNotZero(6));
		selenium.sendKeys(pricingArray[1], CommonRandomCore.randomNumericNotZero(6));

		validate.getInputData(pricingArray);
		products.clickOn_Save();

		products.savedMessageIsDisplayed();
		products.clickOn_FirstItemInRowTable();
		url.navigateTo_Pricing();

		validate.getOutputData(pricingArray);
		validate.assertData();
	}

	@Test(priority = 455)
	public void Delete_Existing_Product_Item() {

		String[] componentArray = products.getComponentArray();

		products.clickOn_FirstItemInRowTable();

		String code = selenium.getTextAttribute(componentArray[0]);
		products.clickOn_Delete();

		products.deletedMessageIsDisplayed();
		products.assert_IsNotFirstItemInRowTable(code);
	}

//	@Test(priority = 500, dataProvider = "getDataTypeNotNull", dataProviderClass = ProviderData.class)
	public void Create_New_Product_Item_With_Different_DataTypes(String value) {

		products.clickOn_Create();

		String[] componentArray = products.getComponentArray();

		selenium.sendKeys(componentArray[0], value);
		products.selectList_Status();
		products.selectList_ProductFamily();
		products.selectList_Brand();
		selenium.sendKeys(componentArray[1], value);
		selenium.sendKeys(componentArray[2], value);
		products.selectFirstList_Categories();
		selenium.sendKeysSelectInput(componentArray[7], value);
		selenium.click(componentArray[7]);

		validate.getInputData(componentArray);
		products.clickOn_Save();

		products.savedMessageIsDisplayed();
		products.clickOn_FirstItemInRowTable();
		products.statusCheckboxIsActive();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

}