package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class CategoriesTCs extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

		url.navigateTo_Categories();
	}

	// [0]Name [1]Description

	@Test(priority = 351)
	public void Create_New_Category_Item() {

		categories.clickOn_Create();

		String[] componentArray = categories.getComponentArray();
		String name = faker.beer().style();

		selenium.sendKeys(componentArray[0], name);
		selenium.sendKeys(componentArray[1], faker.beer().yeast());

		validate.getInputData(componentArray);
		categories.clickOn_Save();

		categories.savedMessageIsDisplayed();
		categories.clickOn_EditLastItemInTree(name);
		categories.statusCheckboxIsActive();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

	@Test(priority = 352)
	public void Edit_Present_First_Category_Item() {

		categories.clickOn_EditFirstItemInTree();

		String[] componentArray = categories.getComponentArray();

		selenium.sendKeys(componentArray[0], faker.beer().style());
		selenium.sendKeys(componentArray[1], faker.beer().yeast());
		categories.check_StatusCheckbox();

		validate.getInputData(componentArray);
		categories.clickOn_Save();

		categories.updatedMessageIsDisplayed();
		categories.clickOn_EditFirstItemInTree();
		categories.statuscheckboxIsInactive();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

	@Test(priority = 353)
	public void Delete_Present_First_Category_Item() {

		categories.clickOn_EditFirstItemInTree();

		String[] componentArray = categories.getComponentArray();

		String name = selenium.getTextAttribute(componentArray[0]);
		categories.clickOn_Delete();

		categories.deletedMessageIsDisplayed();
		categories.assert_HasNotItemInTree(name);
	}

//	@Test(priority = 400, dataProvider = "getDataTypeNotNull", dataProviderClass = ProviderData.class)
	public void Create_New_Category_Item_With_Different_DataTypes(String value) {

		categories.clickOn_Create();

		String[] componentArray = categories.getComponentArray();

		selenium.sendKeys(componentArray[0], value);
		selenium.sendKeys(componentArray[1], value);

		validate.getInputData(componentArray);
		categories.clickOn_Save();

		categories.savedMessageIsDisplayed();
		categories.clickOn_EditLastItemInTree(value);
		categories.statusCheckboxIsActive();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

}