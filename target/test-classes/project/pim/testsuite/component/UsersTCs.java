package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class UsersTCs extends SubTransferStator {

	static final String PASSWORD = "012345678";

	@BeforeMethod
	public void beforeMethod() {

		clearWebStorage();

		login.validLogin();
		url.navigateTo_UsersAll();
	}

	// [0]Email [1]Phone [2]First Name [3]Last Name [4]Gender [5]Role
	// [6]Country [7]City [8]Address

	@Test(priority = 101)
	public void Create_New_Admin_Account_Then_Perform_Login() {

		users.clickOn_Create();

		String[] componentArray = users.getComponentArray();
		String email = faker.internet().emailAddress();

		users.check_StatusCheckbox();
		selenium.sendKeys(componentArray[0], email);
		users.insert_NewPassword(PASSWORD);
		selenium.sendKeys(componentArray[1], faker.number().digits(10));
		selenium.sendKeys(componentArray[2], faker.name().firstName());
		selenium.sendKeys(componentArray[3], faker.name().lastName());
		users.selectList_Gender(1);
		users.selectList_Role(1);
		users.selectList_Country();
		selenium.sendKeys(componentArray[7], faker.address().city());
		selenium.sendKeys(componentArray[8], faker.address().streetAddress());

		validate.getInputData(componentArray);
		users.clickOn_Save();

		users.successfulMessageIsDisplayed();
		url.logout();

		login.validLogin(email, PASSWORD);
		url.navigateTo_UsersAll();
		users.clickOn_FirstItemInRowTable();
		users.statusCheckboxIsActive();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

	@Test(priority = 102)
	public void Failed_Login_By_Inactived_Account() {

		users.clickOn_FirstItemInRowTable();

		String[] componentArray = users.getComponentArray();

		String email = selenium.getTextAttribute(componentArray[0]);
		users.check_StatusCheckbox();
		users.statusCheckboxIsInactive();
		users.clickOn_Update();

		users.successfulMessageIsDisplayed();
		url.logout();

		login.invalidLogin(email, PASSWORD);
	}

	@Test(priority = 103)
	public void Create_New_Admin_Account_With_Sensitive_Email_Then_Perform_Login() {

		users.clickOn_Create();

		String[] componentArray = users.getComponentArray();
		String email = faker.internet().emailAddress();

		users.check_StatusCheckbox();
		selenium.sendKeys(componentArray[0], email.toUpperCase());
		users.insert_NewPassword(PASSWORD);
		users.selectList_Role(1);

		users.clickOn_Save();
		users.successfulMessageIsDisplayed();
		url.logout();

		login.validLogin(email.toLowerCase(), PASSWORD);
		url.navigateTo_UsersAll();

		users.clickOn_FirstItemInRowTable();
		users.statusCheckboxIsActive();
	}

	@Test(priority = 104)
	public void Failed_Login_By_Deleted_Account() {

		users.clickOn_FirstItemInRowTable();

		String[] componentArray = users.getComponentArray();

		String email = selenium.getTextAttribute(componentArray[0]);
		users.clickOn_Delete();
		users.successfulMessageIsDisplayed();
		url.logout();

		login.invalidLogin(email, PASSWORD);
	}

	@Test(priority = 105)
	public void Create_New_SuperUser_Account_Then_Perform_Login() {

		users.clickOn_Create();

		String[] componentArray = users.getComponentArray();
		String email = faker.internet().emailAddress();

		users.check_StatusCheckbox();
		selenium.sendKeys(componentArray[0], email);
		users.insert_NewPassword(PASSWORD);
		users.selectList_Role(2);

		users.clickOn_Save();
		users.successfulMessageIsDisplayed();
		url.logout();

		login.validLogin(email, PASSWORD);
		url.navigateTo_UsersAll();
	}

	@Test(priority = 106)
	public void Create_New_User_Account_Then_Perform_Login() {

		users.clickOn_Create();

		String[] componentArray = users.getComponentArray();
		String email = faker.internet().emailAddress();

		users.check_StatusCheckbox();
		selenium.sendKeys(componentArray[0], email);
		users.insert_NewPassword(PASSWORD);
		users.selectList_Role(1);

		users.clickOn_Save();
		users.successfulMessageIsDisplayed();
		url.logout();

		login.validLogin(email, PASSWORD);
		url.navigateTo_UsersAll();
	}

	@Test(priority = 107)
	public void Edit_Current_First_Account_Item() {

		users.clickOn_FirstItemInRowTable();

		String[] componentArray = users.getComponentArray();

		users.check_StatusCheckbox();
		selenium.sendKeys(componentArray[1], faker.number().digits(10));
		selenium.sendKeys(componentArray[2], faker.name().firstName());
		selenium.sendKeys(componentArray[3], faker.name().lastName());
		users.selectList_Gender(2);
		users.selectList_Role(2);
		users.selectList_Country();
		selenium.sendKeys(componentArray[7], faker.address().city());
		selenium.sendKeys(componentArray[8], faker.address().streetAddress());

		validate.getInputData(componentArray);
		users.clickOn_Update();

		users.successfulMessageIsDisplayed();
		selenium.refreshPage();
		users.clickOn_FirstItemInRowTable();
		users.statusCheckboxIsInactive();

		validate.getOutputData(componentArray);
		validate.assertData();
	}

//	@Test(priority = 108)
//	public void Change_Account_Password_Then_Perform_Login() {
//
//		users.clickOn_FirstItemInRowTable();
//
//		String[] componentArray = users.getComponentArray();
//
//		String email = selenium.getTextAttribute(componentArray[0]);
//		String password = faker.internet().password();
//
//		users.clickOn_ChangePassword();
//		users.insert_ChangePassword(password, password);
//		
//		users.clickOn_Update();
//		users.successfulMessageIsDisplayed();
//		url.logout();
//
//		login.validLogin(email, password);
//
//	}

//	@Test(priority = 150, dataProvider = "getDataPassword", dataProviderClass = ProviderData.class)
	public void Create_New_User_Account_By_Different_Password_DataTypes(String value) {

		users.clickOn_Create();

		String[] componentArray = users.getComponentArray();
		String email = faker.internet().emailAddress();
		String password = value;

		users.check_StatusCheckbox();
		selenium.sendKeys(componentArray[0], email);
		users.insert_NewPassword(value);
		users.selectList_Role(1);

		users.clickOn_Save();
		users.successfulMessageIsDisplayed();
		url.logout();

		login.validLogin(email, password);
		url.navigateTo_UsersAll();
	}

}