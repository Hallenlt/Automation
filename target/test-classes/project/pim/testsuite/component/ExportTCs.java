package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import data.ProviderData;
import project.pim.center.SubTransferStator;

public class ExportTCs extends SubTransferStator {

    @BeforeMethod
    public void beforeMethod() {

	url.navigateTo_Export();
    }

    // [0]ProviderName [1]ConnectionService [2]URL [3]ExecutionInterval
    // [4]StartExecutionAt

    @Test(priority = 651)
    public void Create_New_Cart_Export_Provider_Then_Verify_General_Information() {

	imexport.clickOn_ExportCreate("WORDPRESS");

	String[] componentArray = imexport.getComponentArray();
	String name = faker.leagueOfLegends().rank();

	selenium.sendKeys(componentArray[0], name);
//		imexport.select_ConnectionService();
	selenium.sendKeys(componentArray[2], "http://" + faker.internet().url());
	imexport.insert_Account(faker.name().username(), faker.internet().password());
	imexport.selectList_ExecutionInterval();

	validate.getInputData(componentArray);

	url.navigateTo_MappingConfiguration();
	imexport.selectList_MappingTemplate(2);
	imexport.selectList_ProductFamily();
	imexport.clickOn_Save();

	imexport.savedMessageIsDisplayed();
	imexport.assert_HasItemInListCart(name);
	imexport.clickOn_Edit(name);

	validate.getOutputData(componentArray);
	validate.assertData();
    }

    @Test(priority = 652)
    public void Edit_Current_Cart_Export_Provider_Then_Verify_Genral_Information() {

	imexport.clickOn_EditFirstItem();

	String[] componentArray = imexport.getComponentArray();
	String name = faker.leagueOfLegends().rank();

	selenium.sendKeys(componentArray[0], name);
	selenium.sendKeys(componentArray[2], "http://" + faker.internet().url());
	imexport.insert_Account(faker.name().username(), faker.internet().password());
	imexport.selectList_ExecutionInterval();

	validate.getInputData(componentArray);
	imexport.clickOn_Update();

	imexport.savedMessageIsDisplayed();
	imexport.clickOn_EditFirstItem();

	validate.getOutputData(componentArray);
	validate.assertData();
    }

    @Test(priority = 653)
    public void Create_New_Cart_Export_Provider_Then_Verify_Mapping_Configuration() {

	imexport.clickOn_ExportCreate("WORDPRESS");

	String[] componentArray = imexport.getComponentArray();
	String name = faker.leagueOfLegends().rank();

	selenium.sendKeys(componentArray[0], name);
//		imexport.select_ConnectionService();
	selenium.sendKeys(componentArray[2], "http://" + faker.internet().url());
	imexport.insert_Account(faker.name().username(), faker.internet().password());
	imexport.selectList_ExecutionInterval();

	String[] mappingArray = imexport.getMappingArray();

	url.navigateTo_MappingConfiguration();
	imexport.selectList_MappingTemplate(2);
	imexport.selectList_ProductFamily();

	validate.getInputData(mappingArray);
	imexport.clickOn_Save();

	imexport.savedMessageIsDisplayed();
	imexport.assert_HasItemInListCart(name);
	imexport.clickOn_Edit(name);
	url.navigateTo_MappingConfiguration();

	validate.getOutputData(mappingArray);
	validate.assertData();
    }

    @Test(priority = 654)
    public void Edit_Current_Cart_Export_Provider_Then_Verify_Mapping_Configuration() {

	imexport.clickOn_EditFirstItem();
	url.navigateTo_MappingConfiguration();

	String[] mappingArray = imexport.getMappingArray();

	imexport.selectList_MappingTemplate(1);
	validate.getInputData(mappingArray);
	imexport.clickOn_Update();

	imexport.savedMessageIsDisplayed();
	imexport.clickOn_EditFirstItem();
	url.navigateTo_MappingConfiguration();

	validate.getOutputData(mappingArray);
	validate.assertData();
    }

    @Test(priority = 655)
    public void Delete_Current_Cart_Export_Provider() {

	imexport.clickOn_EditFirstItem();

	String[] componentArray = imexport.getComponentArray();

	String name = selenium.getTextAttribute(componentArray[0]);

	imexport.clickOn_Close();
	imexport.clickOn_Delete(name);

	imexport.deletedMessageIsDisplayed();
	imexport.assert_HasNotItemInListCart(name);
    }

    @Test(priority = 656)
    public void Change_Start_Execution_At_And_Meansure_The_Time() {

	imexport.clickOn_EditFirstItem();

	String[] componentArray = imexport.getComponentArray();

	imexport.pickDatetime_StartExecutionAt();

	validate.getInputData(componentArray);
	imexport.clickOn_Update();

	imexport.savedMessageIsDisplayed();
	imexport.clickOn_EditFirstItem();

	validate.getOutputData(componentArray);
	validate.assertData();
    }

//	@Test(priority = 700, dataProvider = "getDataTypeNotNull", dataProviderClass = ProviderData.class)
    public void Create_New_Cart_Export_Provider_With_Different_DataTypes(String value) {

	imexport.clickOn_ExportCreate("WORDPRESS");

	String[] componentArray = imexport.getComponentArray();
	String name = value;

	selenium.sendKeys(componentArray[0], name);
//		imexport.select_ConnectionService();
	selenium.sendKeys(componentArray[2], "http://" + faker.internet().url());
	imexport.insert_Account(faker.name().username(), faker.internet().password());
	imexport.selectList_ExecutionInterval();

	validate.getInputData(componentArray);
	imexport.clickOn_Save();

	imexport.savedMessageIsDisplayed();
	imexport.clickOn_Edit(name);

	validate.getOutputData(componentArray);
	validate.assertData();
    }

}