package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class RegisterTCs extends SubTransferStator {

	static final String PASSWORD = "012345678";

	@BeforeMethod
	public void beforeMethod() {

		clearWebStorage();

		home.redirectTo_HomePage();
		url.navigateTo_Register();
	}
	
	// [0]Display Name [1]Phone [2]Email [3]Password [4]Confirm Password

	@Test(priority = 51)
	public void NavigateTo_Login_Screen_From_Register_Page() {

		url.titleUrlAssert_Register();
		url.navigateTo_Login();
		home.assert_HomeTitleURL();
	}

	@Test(priority = 53)
	public void Register_New_Account_With_Sensitive_Email() {

		String[] componentArray = register.getComponentArray();
		String email = faker.internet().emailAddress();

		selenium.sendKeys(componentArray[0], faker.name().name());
		selenium.sendKeys(componentArray[1], faker.number().digits(10));
		selenium.sendKeys(componentArray[2], email);
		selenium.sendKeys(componentArray[3], PASSWORD);
		selenium.sendKeys(componentArray[4], PASSWORD);

		register.submit();
		register.successfulMessageIsDisplayed();
		home.assert_HomeTitleURL();

		login.validLogin(email.toUpperCase(), PASSWORD);
	}
	
//	@Test(priority = 100, dataProvider = "getDataPassword", dataProviderClass = ProviderData.class)
	public void Register_New_Account_With_Different_Password_DataTypes(String value) {

		String[] componentArray = register.getComponentArray();

		selenium.sendKeys(componentArray[0], faker.leagueOfLegends().champion());
		selenium.sendKeys(componentArray[1], faker.number().digits(10));
		selenium.sendKeys(componentArray[2], faker.internet().emailAddress());
		selenium.sendKeys(componentArray[3], value);
		selenium.sendKeys(componentArray[4], value);

		register.submit();
		register.successfulMessageIsDisplayed();
		home.assert_HomeTitleURL();
	}

}