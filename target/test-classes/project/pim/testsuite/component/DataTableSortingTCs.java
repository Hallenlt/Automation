package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import project.pim.ElementUtils;
import project.pim.center.SubTransferStator;
import project.pim.object.UsersPage;

public class DataTableSortingTCs extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

	}

	@Test(priority = 801)
	public void Sort_By_Column_The_Product_Data_Table() {

		url.redirectTo_Products();
		sort.ArrangeDynamicTable(ElementUtils.SORT_BUTTON_ELEMENT, ElementUtils.FIRST_COLUMN_ELEMENT);
	}

	@Test(priority = 802)
	public void Sort_By_Column_The_ProductFamilies_Data_Table() {

		url.redirectTo_ProductFamilies();
		sort.ArrangeDynamicTable(ElementUtils.SORT_BUTTON_ELEMENT, ElementUtils.FIRST_COLUMN_ELEMENT);
	}

	@Test(priority = 803)
	public void Sort_By_Column_The_UsersAll_Data_Table() {

		url.redirectTo_UsersAll();
		sort.ArrangeDynamicTable(UsersPage.SORT_BUTTON_ELEMENT, UsersPage.ROWS_ELEMENT);
	}

	@Test(priority = 804)
	public void Sort_By_Column_The_Users_RoleAdmin_Data_Table() {

		url.redirectTo_UsersAdmin();
		sort.ArrangeDynamicTable(UsersPage.SORT_BUTTON_ELEMENT, UsersPage.ROWS_ELEMENT);
	}

	@Test(priority = 805)
	public void Sort_By_Column_The_Users_RoleSuper_Data_Table() {

		url.redirectTo_UsersSuper();
		sort.ArrangeDynamicTable(UsersPage.SORT_BUTTON_ELEMENT, UsersPage.ROWS_ELEMENT);
	}

	@Test(priority = 806)
	public void Sort_By_Column_The_Users_RoleUser_Data_Table() {

		url.redirectTo_UsersUser();
		sort.ArrangeDynamicTable(UsersPage.SORT_BUTTON_ELEMENT, UsersPage.ROWS_ELEMENT);
	}

	@Test(priority = 850)
	public void Sort_By_Column_The_SettingAttributes_Data_Table() {

		url.redirectTo_SettingsAttributes();
		sort.ArrangeDynamicTable(ElementUtils.SORT_BUTTON_ELEMENT, ElementUtils.FIRST_COLUMN_ELEMENT);
	}

}