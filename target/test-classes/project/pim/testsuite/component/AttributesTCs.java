package project.pim.testsuite.component;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import data.ProviderData;
import project.pim.center.SubTransferStator;

public class AttributesTCs extends SubTransferStator {

	@BeforeMethod
	public void beforeMethod() {

		url.redirectTo_SettingsAttributes();
	}

	// [0]Code [1]Name [2]Metadata [3]Group

	@Test(priority = 301)
	public void Create_New_Attribute_Item_As_MultiSelect_Type() {

		attributes.clickOn_Create()

				.clickOn_MultiSelectType();
		String[] componentArray = attributes.getComponentArray();

		selenium.sendKeysRandomUUID(componentArray[0])

				.sendKeys(componentArray[1], faker.leagueOfLegends().masteries());
		attributes.selectList_Group();
		validate.getInputData(componentArray);
		attributes.clickOn_Save()

				.savedMessageIsDisplayed()

				.clickOn_FirstItemInRowTable()

				.statusCheckboxIsActive();
		validate.getOutputData(componentArray);
		validate.assertData();
	}

	@Test(priority = 302)
	public void Edit_Current_Attribute_Item() {

		attributes.clickOn_FirstItemInRowTable();
		String[] componentArray = attributes.getComponentArray();

		selenium.sendKeys(componentArray[1], faker.leagueOfLegends().masteries());
		attributes.check_StatusCheckbox()

				.selectList_Group();
		validate.getInputData(componentArray);
		attributes.clickOn_Update()

				.updatedMessageIsDisplayed()

				.clickOn_FirstItemInRowTable()

				.statuscheckboxIsInactive();
		validate.getOutputData(componentArray);
		validate.assertData();
	}

	@Test(priority = 303)
	public void Delete_Current_Attribute_Item() {

		String[] componentArray = attributes.getComponentArray();
		attributes.clickOn_FirstItemInRowTable();

		String code = selenium.getTextAttribute(componentArray[0]);
		attributes.clickOn_Delete()

				.deletedMessageIsDisplayed()

				.assert_IsNotFirstItemInRowTable(code);
	}

//	@Test(priority = 304, dataProvider = "getDataTypeNotNull", dataProviderClass = ProviderData.class)
//	public void Add_New_Attribute_Item_As_MultiSelect_Type(String value) {
//
//		attributes.clickOn_Create().clickOn_MultiSelectType();
//		String[] componentArray = attributes.getComponentArray();
//
//		selenium.sendKeys(componentArray[0], value);
//		selenium.sendKeys(componentArray[1], value);
//		attributes.selectList_Group();
//		validate.getInputData(componentArray);
//		attributes.clickOn_Save()
//
//				.savedMessageIsDisplayed().clickOn_FirstItemInRowTable().statusCheckboxIsActive();
//		validate.getOutputData(componentArray);
//		validate.assertData();
//	}

}