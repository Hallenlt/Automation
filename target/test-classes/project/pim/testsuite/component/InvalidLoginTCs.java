package project.pim.testsuite.component;

import org.testng.Reporter;
import org.testng.annotations.Test;

import project.pim.center.SubTransferStator;

public class InvalidLoginTCs extends SubTransferStator {

	static final String USERNAME_ARRAY[] = { "", "", "admin@idsolutions.com.vn", "a' or '1'='1", "admin@idsolutions.com.vn", "'=' 'or'", "",
			"admin@idsolutions.com.vn" };
	static final String PASSWORD_ARRAY[] = { "123456", "", "admin@idsolutions.com.vn", "a' or '1'='1", "", "'=' 'or'", "123456" };

	@Test(alwaysRun = true)
	public void Invalid_Login_With_Wrong_Username_Or_Password() {

		home.redirectTo_HomePage();

		int userlenght = USERNAME_ARRAY.length;
		int passlenght = PASSWORD_ARRAY.length;
		for (int i = 0; i < userlenght && i < passlenght; i++) {

			url.logout();
			home.input_username(USERNAME_ARRAY[i]).input_password(USERNAME_ARRAY[i]).submit();
			home.assert_HomeTitleURL();
			Reporter.log("\n\t [INFO] - Executed | Login Unsuccessful. =>> " + i + " |");
		}
	}

}