package project.pim.center;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import infrastructure.selenium.SeleniumBaseCore;
import project.pim.object.HomePage;

public class Login extends HomePage {

//    static final String USERNAME = "admin@idsolutions.com.vn";
//    static final String PASSWORD = "123456";

	public final String USERNAME = "0977739579";
	public final String PASSWORD = "trilocvt1612";

	PageURL url;

	public Login(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public Login validLogin() {

		login(USERNAME, PASSWORD);
		return this;
	}

	public Login validLogin(String username, String password) {

		login(username, password);
		return this;
	}

	public Login invalidLogin(String username, String password) {

		login(username, password);
		failedMessageIsDisplayed().assert_HomeTitleURL();
		validLogin();
		return this;
	}

	protected Login login(String username, String password) {

		selenium = new SeleniumBaseCore(driver);
		url = new PageURL(driver);

		Reporter.log("\n\t [INFO] - Executed | Performing Login Action. | \n", true);
		redirectTo_HomePage().assert_HomeTitleURL();
		input_username(username).input_password(password);
		submit();
		return this;
	}

	public Login failedMessageIsDisplayed() {

		checkIfElementIsDisplayed("//div[contains(.,'Login Failed')]");
		return this;
	}

}