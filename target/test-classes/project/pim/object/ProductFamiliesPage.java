package project.pim.object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import infrastructure.selenium.SeleniumBaseCore;
import project.pim.ElementUtils;

public class ProductFamiliesPage extends SeleniumBaseCore {

	public ProductFamiliesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	final String MENU_ATTRIBUTE_ELEMENT = "//*[@id=\"menu-attributes\"]";

	// [0]Title [1]Description [2]Code [3]AttributeLabel
	String componentArray[] = { "//*[@id=\"name.en.value\"]", "//*[@id=\"description\"]", "//*[@id=\"code\"]",
			"//*[@aria-labelledby=\"attributeUsedAsLabel\"]" };

	String tableDataArray[] = { "" };

	public String[] getTableDataArray() {
		return tableDataArray.clone();
	}

	public String[] getComponentArray() {
		return componentArray.clone();
	}

	public ProductFamiliesPage input_Search(String value) {
		sendKeys(ElementUtils.SEARCH_BOX_ELEMENT, value);
		click(ElementUtils.SEARCH_BUTTON_ELEMENT);
		return this;
	}

	public ProductFamiliesPage assert_HasNotItemInListAttributes(String expected) {
		openListbox_Attributes();
		assertHasNotDataInPageSource(MENU_ATTRIBUTE_ELEMENT + "/div[3]", expected);
		return this;
	}

	public ProductFamiliesPage assert_HasItemInListAttributes(String expected) {
		openListbox_Attributes();
		assertHasDataInPageSource(MENU_ATTRIBUTE_ELEMENT + "/div[3]", expected);
		return this;
	}

	public ProductFamiliesPage assert_IsFirstItemInRowTable(String expected) {
		assertIsFirstItemInDataRowTable(ElementUtils.FIRST_ROW_ELEMENT, ElementUtils.FIRST_CELL_ELEMENT,
				expected);
		return this;
	}

	public ProductFamiliesPage assert_IsNotFirstItemInRowTable(String expected) {
		assertIsNotFirstItemInDataRowTable(ElementUtils.FIRST_ROW_ELEMENT,
				ElementUtils.FIRST_CELL_ELEMENT, expected);
		return this;
	}

	public ProductFamiliesPage clickOn_FirstItemInRowTable() {
		clickFirstItemInList(ElementUtils.FIRST_ROW_ELEMENT);
		return this;
	}

	public ProductFamiliesPage selectList_AttributeLabel() {
		selectDropdownElective(componentArray[3], ElementUtils.DROPDOWN_MENU_ELEMENT, "li");
		return this;
	}

	public ProductFamiliesPage selectMultiList_Attributes() {
		openListbox_Attributes();
		checkAllCheckbox(MENU_ATTRIBUTE_ELEMENT + "/div[3]/ul", "li");
		closeListbox_Attributes();
		return this;
	}

	public ProductFamiliesPage openListbox_Attributes() {
		click("//*[text()='Add Attributes']");
		click("//*[@aria-labelledby=' mui-component-select-attributes']");
		return this;
	}

	public ProductFamiliesPage closeListbox_Attributes() {
		try {
			driver().findElement(By.xpath(MENU_ATTRIBUTE_ELEMENT + "/div[1]")).click();
			Thread.sleep(500);
			driver().findElement(By.xpath("//*[@id=\"simple-popover\"]/div[1]")).click();
			Thread.sleep(500);
		} catch (WebDriverException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	public ProductFamiliesPage statusCheckboxIsInactive() {
		checkboxIsUnselected(ElementUtils.CHECKBOX_ELEMENT);
		return this;
	}

	public ProductFamiliesPage statusCheckboxIsActive() {
		checkboxIsSelected(ElementUtils.CHECKBOX_ELEMENT);
		return this;
	}

	public ProductFamiliesPage check_StatusCheckbox() {
		check("//form/div/div[2]/div/div[2]/div[2]/div[1]/div/div[2]/label/span[1]");
		return this;
	}

	public ProductFamiliesPage clickOn_Delete() {
		click("//*[text()='Delete']");
		return this;
	}

	public ProductFamiliesPage clickOn_Save() {
		click("//*[text()='Save']");
		return this;
	}

	public ProductFamiliesPage clickOn_Create() {
		click("//*[text()='add new']");
		return this;
	}

	public ProductFamiliesPage deletedMessageIsDisplayed() {
		checkIfElementIsDisplayed("//div[contains(.,'Product Family Is Deleted')]");
		return this;
	}

	public ProductFamiliesPage savedMessageIsDisplayed() {
		checkIfElementIsDisplayed("//div[contains(.,'Product Family Saved')]");
		return this;
	}
}