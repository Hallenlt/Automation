package project.pim.object;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;
import infrastructure.utility.DirectoryUtils;
import project.pim.ElementUtils;

public class ProductsPage extends SeleniumBaseCore {

    public ProductsPage(WebDriver driver) {
	super(driver);
	// TODO Auto-generated constructor stub
    }

    // [0]Name [1]Description [2]LongDescription [3]Status [4]ProductFamily
    // [5]Brand [6]Categories [7]Tags
    String componentArray[] = { "//*[@id=\"name\"]", "//*[@id=\"description\"]", "//*[@id=\"longDescription\"]",
	    "//*[@aria-labelledby=\" mui-component-select-productStatus\"]", "//*[@aria-labelledby=\" mui-component-select-family\"]",
	    "//*[@aria-labelledby=\" mui-component-select-brand\"]", "//*[@class='selected-list']", "//input[@aria-autocomplete='list']" };

    // [0]PriceExcludedTax [1]PriceIncludedTax
    String pricingArray[] = { "//*[@id='priceTaxExcl']", "//*[@id='priceTaxIncl']" };

    public String[] getPricingArray() {
	return pricingArray.clone();
    }

    public String[] getComponentArray() {
	return componentArray.clone();
    }

    public ProductsPage input_Search(String value) {
	sendKeys(ElementUtils.SEARCH_BOX_ELEMENT, value);
	click(ElementUtils.SEARCH_BUTTON_ELEMENT);
	return this;
    }

    public ProductsPage assert_HasNotItemInListCategory(String expected) {
	click(componentArray[6]);
	assertHasNotDataInPageSource("//div[@class='option-custom ']", expected);
	return this;
    }

    public ProductsPage assert_HasItemInListCategory(String expected) {
	click(componentArray[6]);
	assertHasDataInPageSource("//div[@class='option-custom ']", expected);
	return this;
    }

    public ProductsPage assert_HasNotItemInListProductFamily(String expected) {
	click(componentArray[4]);
	assertHasNotDataInPageSource(ElementUtils.DROPDOWN_MENU_ELEMENT, expected);
	return this;
    }

    public ProductsPage assert_HasItemInListProductFamily(String expected) {
	click(componentArray[4]);
	assertHasDataInPageSource(ElementUtils.DROPDOWN_MENU_ELEMENT, expected);
	return this;
    }

    public ProductsPage assert_IsFirstItemInRowTable(String expected) {
	assertIsFirstItemInDataRowTable(ElementUtils.FIRST_ROW_ELEMENT, ElementUtils.FIRST_CELL_ELEMENT, expected);
	return this;
    }

    public ProductsPage assert_IsNotFirstItemInRowTable(String expected) {
	assertIsNotFirstItemInDataRowTable(ElementUtils.FIRST_ROW_ELEMENT, ElementUtils.FIRST_CELL_ELEMENT, expected);
	return this;
    }

    public ProductsPage clickOn_FirstItemInRowTable() {
	clickFirstItemInList(ElementUtils.FIRST_ROW_ELEMENT);
	return this;
    }

//	public ProductsPage upload_Attachments(String filename) {
//		uploadFile2("//label/span", UtilitiesDataPath.RESOURCE_PATH + "\\testdata\\" + filename);
//    return this;
//	}

    public ProductsPage selectList_Status() {
	selectDropdownElective(componentArray[3], ElementUtils.DROPDOWN_MENU_ELEMENT, "li");
	return this;
    }

    public ProductsPage selectList_ProductFamily() {
	selectDropdownElective(componentArray[4], ElementUtils.DROPDOWN_MENU_ELEMENT, "li");
	return this;
    }

    public ProductsPage selectList_Brand() {
	selectDropdownElective(componentArray[5], ElementUtils.DROPDOWN_MENU_ELEMENT, "li");
	return this;
    }

    public ProductsPage selectSecondList_Categories() {
	click(componentArray[6]);
	click("//div[2][@class='select-item rounded mb-2']");
	click(componentArray[6]);
	return this;
    }

    public ProductsPage selectFirstList_Categories() {
	click(componentArray[6]);
	click("//div[1][@class='select-item rounded mb-2']");
	click(componentArray[6]);
	return this;
    }

    public ProductsPage statuscheckboxIsInactive() {
	checkboxIsUnselected(ElementUtils.CHECKBOX_ELEMENT);
	return this;
    }

    public ProductsPage statusCheckboxIsActive() {
	checkboxIsSelected(ElementUtils.CHECKBOX_ELEMENT);
	return this;
    }

    public ProductsPage check_StatusCheckbox() {
	check("//div[3]/label");
	return this;
    }

    public ProductsPage clickOn_Delete() {
	click("//*[text()='Delete']");
	return this;
    }

    public ProductsPage clickOn_Save() {
	click("//*[text()='Save']");
	return this;
    }

    public ProductsPage clickOn_Create() {
	click("//*[text()='add new']");
	return this;
    }

    public ProductsPage deletedMessageIsDisplayed() {
	checkIfElementIsDisplayed("//div[contains(.,'Process Successful !')]");
	return this;
    }

    public ProductsPage savedMessageIsDisplayed() {
	checkIfElementIsDisplayed("//div[contains(.,'Process Successful !')]");
	return this;
    }

}