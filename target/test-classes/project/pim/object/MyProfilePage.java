package project.pim.object;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;
import infrastructure.utility.DirectoryUtils;

public class MyProfilePage extends SeleniumBaseCore {

    public MyProfilePage(WebDriver driver) {
	super(driver);
	// TODO Auto-generated constructor stub
    }

    // [0]DisplayName [1]Email [2]Phone [3]Address [4]Role
    String componentArray[] = { "//*[@id=\"displayName\"]", "//*[@id=\"email\"]", "//*[@id=\"phone\"]", "//*[@id=\"address\"]",
	    "//*[@id=\"roleProfileModel.profileName\"]" };

    String integrationArray[] = { componentArray[1], componentArray[2] };

    public String[] getIntegrationArray() {
	return integrationArray.clone();
    }

    public String[] getComponentArray() {
	return componentArray.clone();
    }

//	public MyProfilePage upload_AvatarImage(String filename) {
//		uploadFile2("//div/label", UtilitiesDataPath.RESOURCE_PATH + "\\test-data\\" + filename);
//		return this;
//	}

    public MyProfilePage clickOn_Save() {
	click("//*[text()='Save']");
	return this;
    }

    public MyProfilePage errorMessageIsDisplayed() {
	checkIfElementIsDisplayed("//div[contains(.,'')]");
	return this;
    }

    public MyProfilePage successMessageIsDisplayed() {
	checkIfElementIsDisplayed("//div[contains(.,'Profile Saved')]");
	return this;
    }

}
