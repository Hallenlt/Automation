package project.pim.object;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;

import infrastructure.selenium.SeleniumBaseCore;
import project.pim.center.PageURL;

public class HomePage extends SeleniumBaseCore {

	public HomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public HomePage assert_HomeTitleURL() {
		assertTitleURL("Login Page", PageURL.IP_CONFIG + "/login");
		return this;
	}

	public HomePage redirectTo_HomePage() {
		getURL(PageURL.IP_CONFIG + "/login");
		return this;
	}

	public HomePage switchLanguage() {
		return this;
	}

	public HomePage submit() {
		click("//*[text()='Login']");
		return this;
	}

	public HomePage input_username(String value) {
		sendKeys("//input[@name='username']", value);
		return this;
	}

	public HomePage input_password(String value) {
		sendKeys("//input[@name='password']", value);
		return this;
	}

	public void getURL(String url) {

		Reporter.log("[INFO] - Executed | Opening URL Path Navigation To: " + url + " |");
		try {
			driver.get(url);
		} catch (WebDriverException e) {
			// TODO: handle exception
			fail(null, e);
		}
	}

	public void click(String locator) {

		Reporter.log("[INFO] - Executed | Clicking On Locator: " + locator + " |");
		waitForPresenceOfElement(locator);
		try {
			WebElement element = driver.findElement(By.xpath(locator));
//			wait.until(ExpectedConditions.elementToBeClickable(element = driver.findElement(By.xpath(locator))));
			if (element.isEnabled()) {
				element.click();
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	public void assertTitleURL(String expectTitle, String expectUrl) {

		Reporter.log("[INFO] - Executed | Asserting Page Title: " + expectTitle + " |");
		Reporter.log("[INFO] - Executed | Asserting URL Navigation: " + expectUrl + " |");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			fail(null, e);
		}
//		(new WebDriverWait(driver, ELEMENT_WAIT_TIMEOUT_IN_SECONDS)).until(ExpectedConditions.titleIs(expectTitle));
		String actualTitle = "";
		actualTitle = driver.getTitle();
		status = expectTitle.equals(actualTitle);
		if (status == true) {

		}
//		(new WebDriverWait(driver, ELEMENT_WAIT_TIMEOUT_IN_SECONDS)).until(ExpectedConditions.urlToBe(expectUrl));
		String actualUrl = "";
		actualUrl = driver.getCurrentUrl();
		status = expectUrl.equals(actualUrl);
		if (status == true) {

		} else {
			assertEquals(actualTitle, expectTitle);
			assertEquals(actualUrl, expectUrl);
		}
	}

	public HomePage sendKeys(String locator, String value) {

		Reporter.log("[INFO] - Executed | Clearing And Sendkeys Text Input: " + value + " ==>> At Locator: " + locator
				+ " |");
		waitForPresenceOfElement(locator);
		try {
			WebElement element = driver.findElement(By.xpath(locator));
			element.sendKeys(Keys.chord(Keys.CONTROL + "a"));
			element.sendKeys(Keys.chord(Keys.DELETE));
			element.sendKeys(value);
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
		return this;
	}

}