package infrastructure;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.text.diff.CommandVisitor;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.utility.DirectoryUtils;

public class CommandVisitorImpl extends CommonLibrariesCore implements CommandVisitor<Character> {

	// Spans with red & green highlights to put highlighted characters in HTML
	static final String DELETION = "<span style=\"background-color: #FB504B\">${text}</span>";
	static final String INSERTION = "<span style=\"background-color: #45EA85\">${text}</span>";

	String left = "";
	String right = "";

	@Override
	public void visitKeepCommand(Character c) {

		// For new line use <br/> so that in HTML also it shows on next line.
		String toAppend = "\n".equals("" + c) ? "<br/>" : "" + c;
		// KeepCommand means c present in both left & right. So add this to both without
		// any
		// highlight.
		left = left + toAppend;
		right = right + toAppend;
	}

	@Override
	public void visitInsertCommand(Character c) {

		// For new line use <br/> so that in HTML also it shows on next line.
		String toAppend = "\n".equals("" + c) ? "<br/>" : "" + c;
		// InsertCommand means character is present in right file but not in left. Show
		// with green highlight on right.
		right = right + INSERTION.replace("${text}", "" + toAppend);
	}

	@Override
	public void visitDeleteCommand(Character c) {

		// For new line use <br/> so that in HTML also it shows on next line.
		String toAppend = "\n".equals("" + c) ? "<br/>" : "" + c;
		// DeleteCommand means character is present in left file but not in right. Show
		// with red highlight on left.
		left = left + DELETION.replace("${text}", "" + toAppend);
	}

	public void generateHTML() {

		try {
			String template = null;
			template = FileUtils.readFileToString(
					new File(DirectoryUtils.RESOURCE_PATH + "\\config\\diff-template.html"), "utf-8");
			String out1 = template.replace("${left}", left);
			String output = out1.replace("${right}", right);
			FileUtils.write(new File(".\\DiffReport.html"), output, "utf-8");
			APP_LOGS("HTML Diff Report Generated.");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			APP_LOGS(e.getMessage());
		}

	}
}