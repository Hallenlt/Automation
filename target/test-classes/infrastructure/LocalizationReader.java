package infrastructure;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

import infrastructure.utility.DirectoryUtils;

public class LocalizationReader {

	static final String FILE_NAME = DirectoryUtils.RESOURCE_PATH + "\\test-data\\languages\\";

	public static String translateText(String key, String language) throws IOException {

		Properties prop = new Properties();
		FileInputStream input = new FileInputStream(FILE_NAME + language + ".properties");
		prop.load(new InputStreamReader(input, Charset.forName("UTF-8")));
		input.close();
		return prop.getProperty(key);
	}

}