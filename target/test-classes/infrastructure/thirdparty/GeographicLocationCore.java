package infrastructure.thirdparty;

import java.util.HashMap;
import java.util.Map;

import infrastructure.thread.CentralBaseProcessor;

public class GeographicLocationCore extends CentralBaseProcessor {

	public void locateCoordinates(float latitude, float longitude) {

		Map<String, Object> coordinates = new HashMap<String, Object>();
		coordinates.put("latitude", latitude);
		coordinates.put("longtitude", longitude);
		coordinates.put("accuracy", 1);
//		  driver().executeCdpCommand("Emulation.setGeolocationOverride", coordinates);
	}
	
}