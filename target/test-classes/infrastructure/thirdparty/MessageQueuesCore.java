package infrastructure.thirdparty;

import java.io.File;
import java.io.IOException;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.commons.io.FileUtils;
import org.testng.Reporter;

import infrastructure.thread.CentralBaseProcessor;

public class MessageQueuesCore extends CentralBaseProcessor {

	// Creating Active Client instance
	ActiveMQConnectionFactory factory = null;
	Destination destination;
	ActiveMQConnection connection = null;
	TextMessage text;
	MessageProducer producer;
	MessageConsumer consumer;
	Message message;
	Session session;

	public MessageQueuesCore(String username, String password, String port, String queue) {

		try {
			Reporter.log("\n [INFO] - Executed | Initialize Message Queues: " + port + " :: " + queue + " |", true);
			factory = new ActiveMQConnectionFactory(port);
			factory.setUserName(username);
			factory.setPassword(password);
			connection = (ActiveMQConnection) factory.createConnection();
			connection.start();
			session = (ActiveMQSession) connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(queue);
			producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			APP_LOGS("Initialized Message Queues: " + port + " :: " + queue);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			APP_LOGS_FAIL(e.getMessage());
		}
	}

	public MessageQueuesCore sendMessage(String path) {

		try {
			Reporter.log("\n [INFO] - Executed | Sending File Message To Queue: " + path + " |", true);
			String file = FileUtils.readFileToString(new File(path), "utf-8");
			this.text = this.session.createTextMessage(file);
			this.producer.send(text);
			APP_LOGS("Sent File Message To Queue: " + path);
		} catch (JMSException | IOException e) {
			// TODO Auto-generated catch block
			APP_LOGS_FAIL(e.getMessage());
		}
		return this;
	}

	public MessageQueuesCore removeMessages(String queue) {

		try {
			this.connection.destroyDestination(new ActiveMQQueue(queue));
		} catch (Exception e) {
			// TODO: handle exception
			APP_LOGS_FAIL(e.getMessage());
		}
		return this;
	}

	public MessageQueuesCore disconnect() {

		if (this.session != null && this.connection != null) {
			try {
				this.session.close();
				this.connection.close();
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				APP_LOGS_FAIL(e.getMessage());
			}
		}
		return this;
	}

}