package infrastructure.thirdparty;

import static org.testng.Assert.fail;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;

import infrastructure.root.CommonLibrariesCore;

public class SMSReaderCore extends CommonLibrariesCore {

	static final String SMS_RECEIVE_FREE_URL = "https://smsreceivefree.com/country/usa";
	static final String SMS_MESSAGE_LIST_URL = "https://smsreceivefree.com/info";
	static final String NUMBER_CSS_CLASS = "numbutton";
	static final String TABLE_CSS_SELECTOR = "table.msgTable > tbody > tr";
	static final String HREF = "href";
	static final String HTML_TD = "td";
	static final String FORWARD_SLASH = "/";
	static final Pattern PATTERN = Pattern.compile("/info/(\\d+)/");

	public String getNumber() {

		try {
			return Jsoup.connect(SMS_RECEIVE_FREE_URL) // access the site
					.get().body() // get html body
					.getElementsByClass(NUMBER_CSS_CLASS) // get all number elements
					.stream().map(e -> e.attr(HREF)) // get href attr value
					.map(PATTERN::matcher) // regex to find the number in the url
					.filter(Matcher::find) // if there is number
					.findAny() // pick any
					.map(m -> m.group(1)) // get the number
					.orElseThrow(IllegalStateException::new);

		} catch (IllegalStateException | IOException e) {
			// TODO Auto-generated catch block
			APP_LOGS_FAIL(e.getMessage());
			fail(null, e);
		}
		return null;
	}

	public String getMessage(final String phoneNumber, final String fromPhoneNumber) {

		try {
			Jsoup.connect(SMS_MESSAGE_LIST_URL.concat(FORWARD_SLASH).concat(phoneNumber).concat(FORWARD_SLASH)).get().body() // get html body
					.select(TABLE_CSS_SELECTOR).stream().map(tr -> tr.getElementsByTag(HTML_TD)) // get all cells
					.filter(tds -> tds.get(0).text().trim().startsWith(fromPhoneNumber)).findFirst() // find first match
					.map(tds -> tds.get(2)) // pick 3rd cell
					.map(td -> td.text().trim()); // get cell value

			APP_LOGS("SMS Reader Get Message With Phone Number: " + phoneNumber + " | " + fromPhoneNumber);
		} catch (Exception e) {
			// TODO: handle exception
			APP_LOGS_FAIL(e.getMessage());
			fail(null, e);
		}
		return null;
	}

}