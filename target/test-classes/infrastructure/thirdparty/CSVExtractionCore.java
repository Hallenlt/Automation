package infrastructure.thirdparty;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import org.testng.Reporter;

public class CSVExtractionCore {

	public void assertFileCSV(String downloadedPath, int noOfEntries) {

		File file = getLatesFilefromDir(downloadedPath);
		String csvFile = file.getName();

		Reporter.log("CSV File Downloaded Is: " + csvFile);
		Reporter.log("Verify Number Of Entries With Number Of Entries In CSV");
		assertEquals(getRecordsCountInCSV(downloadedPath, csvFile), noOfEntries);
	}

	public int getRecordsCountInCSV(String downloadPath, String csvFileName) {

		int lineNumberCount = 0;
		try {
			if (!csvFileName.isEmpty() || csvFileName != null) {

				String filePath = downloadPath + System.getProperty("file.separator") + csvFileName;
				Reporter.log(filePath, true);
				File file = new File(filePath);
				if (file.exists()) {

					Reporter.log("File found :" + csvFileName, true);
					FileReader fr = new FileReader(file);
					LineNumberReader linenumberreader = new LineNumberReader(fr);

					while (linenumberreader.readLine() != null) {
						lineNumberCount++;
					}
					// To remove the header
					lineNumberCount = lineNumberCount - 1;
					Reporter.log("Total number of lines found in csv : " + (lineNumberCount), true);
					linenumberreader.close();

				} else {
					Reporter.log("File does not exists", true);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return lineNumberCount;
	}

	protected File getLatesFilefromDir(String dirpath) {

		File dir = new File(dirpath);
		File[] files = dir.listFiles();

		if (files == null || files.length == 0)
			return null;
		File lastModifiedFile = files[0];

		for (int i = 1; i < files.length; i++) {
			if (lastModifiedFile.lastModified() < files[i].lastModified())
				lastModifiedFile = files[i];
		}
		return lastModifiedFile;
	}

}
