package infrastructure.interactor;

import org.openqa.selenium.WebDriver;

import infrastructure.selenium.SeleniumBaseCore;
import infrastructure.thread.CentralBaseProcessor;
import infrastructure.validation.BoundaryProcessValidator;
import infrastructure.validation.ProcessValidator;
import io.github.bonigarcia.wdm.config.WebDriverManagerException;
import project.pim.center.PageURL;
import project.pim.object.AttributesPage;
import project.pim.object.BrandsPage;
import project.pim.object.CategoriesPage;
import project.pim.object.HomePage;
import project.pim.object.ImportExportPage;
import project.pim.object.MyProfilePage;
import project.pim.object.ProductFamiliesPage;
import project.pim.object.ProductsPage;
import project.pim.object.RegisterPage;
import project.pim.object.UsersPage;

public class BehaviorDrivenPresentor extends CentralBaseProcessor {

	public BehaviorDrivenPresentor(WebDriver driver) {
		this.driver = driver;
		// TODO Auto-generated constructor stub
	}

	// ===================================================
	// Code part used to handle BDD - Scenario below
	// ===================================================

	protected synchronized void Type(Type behaviorDrivenType, String description) {

		if (behaviorDrivenType != null) {

			if (behaviorDrivenType == Type.SCENARIO) {
				APP_LOGS_INFO("| SCENARIO | : " + description);
			} else if (behaviorDrivenType == Type.GIVEN) {
				APP_LOGS_INFO("GIVEN - " + description);
			} else if (behaviorDrivenType == Type.WHEN) {
				APP_LOGS_INFO("WHEN - " + description);
			} else if (behaviorDrivenType == Type.THEN) {
				APP_LOGS_INFO("THEN - " + description);
			} else if (behaviorDrivenType == Type.AND) {
				APP_LOGS_INFO("AND - " + description);
			}
		} else {
			throw new WebDriverManagerException("Invalid Behavior Driven Type: " + behaviorDrivenType);
		}
	}

	// ===================================================
	// Code part used to handle BDD - User Story below
	// ===================================================

	public BehaviorDrivenPresentor USER_STORY(String description) {

		APP_LOGS_INFO("| USER STORY | : " + description);
		return this;
	}

	public BehaviorDrivenPresentor AS_A(String description) {

		APP_LOGS_INFO("AS A - " + description);
		return this;
	}

	public BehaviorDrivenPresentor I_WANT(String description) {

		APP_LOGS_INFO("I WANT - " + description);
		return this;
	}

	public BehaviorDrivenPresentor SO_THAT(String description) {

		APP_LOGS_INFO("SO THAT - " + description);
		return this;
	}

	// ===================================================
	// Code part used to handle flow redirection below
	// ===================================================

	public synchronized ProcessValidator Validate(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new ProcessValidator(driver());
	}

	public synchronized BoundaryProcessValidator ValidateBoundary(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new BoundaryProcessValidator(driver());
	}

	public synchronized SeleniumBaseCore Selenium(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new SeleniumBaseCore(driver());
	}

	public synchronized PageURL Url(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new PageURL(driver());
	}

	public synchronized AttributesPage Attributes(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new AttributesPage(driver());
	}

	public synchronized BrandsPage Brands(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new BrandsPage(driver());
	}

	public synchronized CategoriesPage Categories(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new CategoriesPage(driver());
	}

	public synchronized HomePage Home(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new HomePage(driver());
	}

	public synchronized ImportExportPage ImportExport(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new ImportExportPage(driver());
	}

	public synchronized MyProfilePage MyProfile(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new MyProfilePage(driver());
	}

	public synchronized ProductFamiliesPage ProductFamilies(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new ProductFamiliesPage(driver());
	}

	public synchronized ProductsPage Products(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new ProductsPage(driver());
	}

	public synchronized RegisterPage Register(Type behaviorDrivenType, String description) {
		Type(behaviorDrivenType, description);
		return new RegisterPage(driver());
	}

	public synchronized UsersPage Users(Type behaviorDrivenType, String description) {

		Type(behaviorDrivenType, description);
		return new UsersPage(driver());
	}

}