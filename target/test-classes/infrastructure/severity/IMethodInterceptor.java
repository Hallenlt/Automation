package infrastructure.severity;

import java.util.List;

import org.testng.IMethodInstance;
import org.testng.ITestContext;

public interface IMethodInterceptor {

	List<IMethodInstance> intercept(List<IMethodInstance> methods, ITestContext context);

}
