package infrastructure.root;

import java.security.SecureRandom;

public class CommonRandomCore {

	static SecureRandom seran = new SecureRandom();

	static String UTF8_ENCODING_CHARS = "áâơôêă";

	static String INVALID_SPECIAL_CHARS = ":/<>*?|";

	static String VALID_SPECIAL_CHARS = "`~!@#$%^&()-_=+{}[];'.,";

	static String NUMERIC = "0123456789";

	static String NUMERIC_NOT_ZERO = "123456789";

	static String ALPHA_NUMERIC = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	static String DOWN_THE_LINE_STRING = "\n01234\n56789\nABCDEFG\nHIJKLMN\nOPQRST\nUVWXYZ\nabcdefgh\nijklmnop\nqrstuvwxyz\n";

	// ===========================================================================
	// Code part used to generate random dummy dynamic input data below
	// ===========================================================================

	public static String randomInvalidSpecialChars(int length) {
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(INVALID_SPECIAL_CHARS.charAt(seran.nextInt(INVALID_SPECIAL_CHARS.length())));
		return sb.toString();
	}

	public static String randomUtf8EncodingChars(int length) {
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(UTF8_ENCODING_CHARS.charAt(seran.nextInt(UTF8_ENCODING_CHARS.length())));
		return sb.toString();
	}

	public static String randomValidSpecialChars(int length) {
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(VALID_SPECIAL_CHARS.charAt(seran.nextInt(VALID_SPECIAL_CHARS.length())));
		return sb.toString();
	}

	public static String randomAlphaNumeric(int length) {
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(ALPHA_NUMERIC.charAt(seran.nextInt(ALPHA_NUMERIC.length())));
		return sb.toString();
	}

	public static String randomNumeric(int length) {
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(NUMERIC.charAt(seran.nextInt(NUMERIC.length())));
		return sb.toString();
	}

	public static String randomNumericNotZero(int length) {
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(NUMERIC_NOT_ZERO.charAt(seran.nextInt(NUMERIC_NOT_ZERO.length())));
		return sb.toString();
	}

	public static String randomDownTheLineString(int length) {
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(DOWN_THE_LINE_STRING.charAt(seran.nextInt(DOWN_THE_LINE_STRING.length())));
		return sb.toString();
	}

}
