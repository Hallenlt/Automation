package infrastructure.root;

import java.util.Calendar;
import java.util.TimeZone;

import org.testng.Reporter;

public class CommonDatetimeCore extends CommonLibrariesCore {

	// ===================================================
	// Code part used to get date-time below
	// ===================================================

	public static String getDatetime(int day, int month, int year) {

		Reporter.log("\n [INFO] - Executed | Getting Datetime: " + day + "/" + month + "/" + year + " |", true);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, day);
		calendar.add(Calendar.MONTH, month);
		calendar.add(Calendar.YEAR, year);

		date = calendar.getTime();
		APP_LOGS("Get Datetime: " + date + " |");

		String dateStr = dateformat.format(date);
		APP_LOGS("Date Format (DD/MM/YYYY): " + dateStr);

		return dateStr;
	}

	public static String getCurrentDay() {

		// Create a Calendar Object
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

		// Get Current Day as a number
		int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
		APP_LOGS("Today Int: " + todayInt + "\n");

		// Integer to String Conversion
		String todayStr = Integer.toString(todayInt);
		APP_LOGS("Today Str: " + todayStr + "\n");

		return todayStr;
	}

	public static String getDatetime(String day) {

		Reporter.log("\n [INFO] - Executed | Getting Datetime: " + day + " |", true);
		Calendar calendar = Calendar.getInstance();
		dateformat.format(calendar.getTime());

		if (day == "today") {
			calendar.add(Calendar.DAY_OF_YEAR, 0);

		} else if (day == "yesterday") {
			calendar.add(Calendar.DAY_OF_YEAR, -1);

		} else if (day == "tomorrow") {
			calendar.add(Calendar.DAY_OF_YEAR, 1);
		}
		String date = dateformat.format(calendar.getTime());
		APP_LOGS("Get Datetime: " + date + " |");

		return date;
	}
	
}
