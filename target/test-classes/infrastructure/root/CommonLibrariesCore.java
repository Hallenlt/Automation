package infrastructure.root;

import java.awt.Robot;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.fluttercode.datafactory.impl.DataFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.aventstack.extentreports.Status;
import com.github.javafaker.Faker;
//import com.relevantcodes.extentreports.ExtentReports;
//import com.relevantcodes.extentreports.ExtentTest;
//import com.relevantcodes.extentreports.LogStatus;

import atu.testrecorder.ATUTestRecorder;
import infrastructure.appium.AppiumBaseCore;
import infrastructure.reporter.ExtentTestManager;
import infrastructure.selenium.DatetimePickerCore;
import infrastructure.selenium.SeleniumBaseCore;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public class CommonLibrariesCore {
	
	protected AppiumBaseCore appium = null;

	protected SeleniumBaseCore selenium = null;

//	protected ClassifierClient clsfClient = null;

	protected DatetimePickerCore datetimePicker;

	protected WebDriverWait wait = null;

	protected Boolean status = null;

	protected static ATUTestRecorder recorder = null;

	protected Alert alert = null;

	protected Robot robot = null;

	protected Process process = null;

	protected Actions actions = null;

	protected static SecureRandom securerandom = new SecureRandom();

	protected Faker faker = new Faker();

	protected DataFactory factory = new DataFactory();

	protected static Date date = new Date();

	public static SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

	public static SimpleDateFormat datetimeformat = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

	protected Runtime runtime = Runtime.getRuntime();

	public static Calendar calendar = Calendar.getInstance();

	protected final static int WAIT_TIMEOUT_IN_SECONDS = 10;

	protected static Reporter reporter;

//	protected ExtentTest logger;
//
//	protected ExtentReports extent;

	// Method for adding logs passed from test cases
	protected synchronized static Reporter APP_LOGS(String message) {

//		logger.log(LogStatus.PASS, message);
		ExtentTestManager.get().log(Status.PASS, message);
		Reporter.log(message, true);
		return reporter;
	}

	protected synchronized static Reporter APP_LOGS_FAIL(String message) {

//		logger.log(LogStatus.FAIL, message);
		ExtentTestManager.get().log(Status.FAIL, message);
		Reporter.log(message, true);
		return reporter;
	}

	protected synchronized static Reporter APP_LOGS_WARNING(String message) {

//		logger.log(LogStatus.WARNING, message);
		ExtentTestManager.get().log(Status.WARNING, message);
		Reporter.log(message, true);
		return reporter;
	}

	protected synchronized static Reporter APP_LOGS_INFO(String message) {

//		logger.log(LogStatus.INFO, message);
		ExtentTestManager.get().log(Status.INFO, message);
		Reporter.log(message, true);
		return reporter;
	}

//	protected void clearWebStorage() {
//
//		try {
//			WebStorage webStorage = null;
//			webStorage = (WebStorage) new Augmenter().augment(driver);
//
//			Reporter.log("\n [INFO] - Executed | " + "Deleting Local Storage" + " |");
//			LocalStorage localStorage = webStorage.getLocalStorage();
//			localStorage.clear();
//
////			Reporter.log("\n [INFO] - Executed | " + "Deleting Session Storage" + " |");
////			SessionStorage sessionStorage = webStorage.getSessionStorage();
////			sessionStorage.clear();
////			
////			Reporter.log("\n [INFO] - Executed | " + "Deleting All Cookies" + " |");
////			driver.manage().deleteAllCookies();
//
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//	}

}
