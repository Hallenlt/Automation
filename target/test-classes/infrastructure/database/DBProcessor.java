package infrastructure.database;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.testng.Reporter;

import infrastructure.utility.Environment;

public class DBProcessor extends DBConnector {

	public DBProcessor(Environment env) {
		super(env);
		// TODO Auto-generated constructor stub
	}

	public static String SELECT_QUERY = "SELECT * FROM ACTION_LOG WHERE ACTION_LOG_ID = 2058520359";
	public static String ACTION_LOG_ID = "action_log_sq.nextval";
	public static Long[] USER_ID = { 18047L, 13001L, 14001L, 17014L, 17043L, 19050L };
	public static Long[] ORGANISATION_ID = { 28050L, 1246L, 1081L, 1081L, 1081L, 1L };
	public static String[] TYPE = { "'desktop'", "'mobile'", "'android'", "'ios'", "'desktop'", "'mobile'" };
	public static String DATE_TIME = "(select to_date('30-05-2019', 'dd-MM-yyyy') - dbms_random.value(1,730) from dual)";
	static String[] IP_ADDRESS = { "'192.168.100.21'", "'192.168.100.22'", "'192.168.100.23'", "'192.168.100.24'", "'192.168.100.25'", "'192.168.100.26'" };
	public static String EVENT[] = { "'Choose content TYPE'", "'Choose article'", "'Login'", "'Logout'", "'Create user'", "'Delete user(s)'" };
	public static String[] PARAMETERS = { "'{\"ContentTYPE\":\"Online\"}'", "'{\"ContentTYPE\":\"Now\",\"ID\":77609724}'", "''", "''",
			"'{\"username\":\"admin\"}'", "'{\"username(s)\":\"[doanphungtriloc@]\"}'" };

	public DBConnector verifyListOfRecords() {

		boolean flag = false;
		List<String> listOfDBValues = new ArrayList<String>();
		String expEmployeeName = "Jack";
		String sqlQuery = "select EmpName from employee";
		listOfDBValues = DBConnector.executeQuery3(sqlQuery);

		for (String strName : listOfDBValues) {
			if (strName.equalsIgnoreCase(expEmployeeName)) {
				flag = true;
				break;
			}
		}
		assertTrue(flag, "Retrieved values are not matching with Expected values");
		return this;
	}

	public DBProcessor Import_ActionLog_By_Model() {

		try {
			int count = 1;
			while (count <= 1000000) {

				List<DBModel> actionLogs = new ArrayList<>();
				for (int i = 0; i < 6; i++) {

					String search_text = new String(faker.app().name());
					DBModel dBModel = new DBModel();

					dBModel.setUserId(USER_ID[i]);
					dBModel.setType(TYPE[i]);
					dBModel.setIpAdress(IP_ADDRESS[i]);
					dBModel.setEvent(EVENT[i]);
					if (EVENT[i].contains("Choose article") || EVENT[i].contains("Choose content TYPE")) {
						dBModel.setSearchText(search_text);
					} else {
						dBModel.setSearchText("");
					}
					dBModel.setOrganisationId(ORGANISATION_ID[i]);
					dBModel.setParameters(PARAMETERS[i]);

					actionLogs.add(dBModel);
				}

				List<String> queries = new ArrayList<>();
				for (DBModel dBModel : actionLogs) {

					String sql = "insert into ACTION_LOG (ACTION_LOG_ID, USER_ID, TYPE, DATE_TIME, IP_ADDRESS, EVENT, SEARCH_TEXT, ORGANISATION_ID, PARAMETERS) values ("
							+ ACTION_LOG_ID + "," + dBModel.getUserId() + "," + dBModel.getType() + "," + DATE_TIME + "," + dBModel.getIpAdress() + ","
							+ dBModel.getEvent() + "," + "'" + dBModel.getSearchText() + "'" + "," + dBModel.getOrganisationId() + "," + dBModel.getParameters()
							+ ")";

//					String builder = String.format("insert into user (id, name) values (%d, '%s')", 1, "abc");

					queries.add(sql);
				}

				for (String i : queries) {

					int queryResult = getState().executeUpdate(i);
					if (queryResult == 1) {
						Reporter.log(i, true);
						Reporter.log("[INFO] - Executed | " + count * 4 + " Row Is Inserted.", true);
					} else {
						Reporter.log("[INFO] - Executed | Error Insert.", true);
					}
				}
				count++;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	public DBProcessor Import_ActionLog_By_Algorithm() {

		try {
			int count = 1;
			while (count <= 1000000) {

				int repeat = 6;
				Long getUserID[] = new Long[repeat];
				String getType[] = new String[repeat];
				String getIpAddress[] = new String[repeat];
				String getEvent[] = new String[repeat];
				Long getOrganisationId[] = new Long[repeat];
				String getParameters[] = new String[repeat];

				int i1, j1 = 0;
				int i2, j2 = 0;
				int i3, j3 = 0;
				int i4, j4 = 0;
				int i5, j5 = 0;
				int i6, j6 = 0;

				for (i1 = 0, j1 = 0, i2 = 0, j2 = 0, i3 = 0, j3 = 0, i4 = 0, j4 = 0, i5 = 0, j5 = 0, i6 = 0, j6 = 0; i1 < repeat; i1++, j1++, i2++, j2++, i3++, j3++, i4++, j4++, i5++, j5++, i6++, j6++) {

					getUserID[i1] = USER_ID[j1];
					if (j1 == USER_ID.length - 1)
						j1 = -1;
					ArrayList<Long> userIdList = new ArrayList<>();
					userIdList.add(getUserID[i1]);

					getType[i2] = TYPE[j2];
					if (j2 == TYPE.length - 1)
						j2 = -1;
					ArrayList<String> TYPEList = new ArrayList<>();
					TYPEList.add(getType[i2]);

					getIpAddress[i3] = IP_ADDRESS[j3];
					if (j3 == IP_ADDRESS.length - 1)
						j3 = -1;
					ArrayList<String> ipAddressList = new ArrayList<>();
					ipAddressList.add(getIpAddress[i3]);

					getEvent[i4] = EVENT[j4];
					if (j4 == EVENT.length - 1)
						j4 = -1;
					ArrayList<String> EVENTList = new ArrayList<>();
					EVENTList.add(getEvent[i4]);

					getOrganisationId[i5] = ORGANISATION_ID[j5];
					if (j5 == ORGANISATION_ID.length - 1)
						j5 = -1;
					ArrayList<Long> organisationIdList = new ArrayList<>();
					organisationIdList.add(getOrganisationId[i5]);

					getParameters[i6] = PARAMETERS[j6];
					if (j6 == PARAMETERS.length - 1)
						j6 = -1;
					ArrayList<String> PARAMETERSList = new ArrayList<>();
					PARAMETERSList.add(getParameters[i6]);

					String search_text;
					if (getEvent[i4].contains("Choose article") || getEvent[i4].contains("Choose content TYPE")) {
						search_text = new String(factory.getCity());
					} else {
						search_text = null;
					}

					String sql = "insert into ACTION_LOG (ACTION_LOG_ID, USER_ID, TYPE, DATE_TIME, IP_ADDRESS, EVENT, SEARCH_TEXT, ORGANISATION_ID, PARAMETERS) values ("
							+ ACTION_LOG_ID + "," + getUserID[i1] + "," + getType[i2] + "," + DATE_TIME + "," + getIpAddress[i3] + "," + getEvent[i4] + ","
							+ "'" + search_text + "'" + "," + getOrganisationId[i5] + "," + getParameters[i6] + ")";

					int queryResult = getState().executeUpdate(sql);
					if (queryResult == 1) {
						Reporter.log(sql, true);
						Reporter.log("[INFO] - Executed | " + count * 6 + " Row Is Inserted.", true);
					} else {
						Reporter.log("[INFO] - Executed | Error Insert.", true);
					}
				}
				count++;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return this;
	}

}
