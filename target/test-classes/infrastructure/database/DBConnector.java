package infrastructure.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.testng.Reporter;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.utility.Environment;
import infrastructure.utility.EnvironmentUtils;

public class DBConnector extends CommonLibrariesCore {

	static Statement state;
	static Connection connect;
	static String jdbcDriver = "oracle.jdbc.driver.OracleDriver";
	static String usernameDB = "";
	static String passwordDB = "";
	static String urlDB = "";

	public static Statement getState() {
		return state;
	}

	public static void setState(Statement state) {
		DBConnector.state = state;
	}

	public static Connection getConnect() {
		return connect;
	}

	public static void setConnect(Connection connect) {
		DBConnector.connect = connect;
	}

	public DBConnector(Environment env) {

		if (env == Environment.TEST) {
			urlDB = EnvironmentUtils.DB_TEST_URL;
			usernameDB = EnvironmentUtils.USERNAME_TEST_DB;
			passwordDB = EnvironmentUtils.PASSWORD_TEST_DB;

		} else if (env == Environment.STAGING) {
			urlDB = EnvironmentUtils.DB_STAG_URL;
			usernameDB = EnvironmentUtils.USERNAME_STAG_DB;
			passwordDB = EnvironmentUtils.PASSWORD_STAG_DB;
		}

		try {
			Class.forName(jdbcDriver);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
//			connect = DriverManager.getConnection(urlDB, usernameDB, passwordDB);
//			state = connect.createStatement();
			setConnect(DriverManager.getConnection(urlDB, usernameDB, passwordDB));
			setState(getConnect().createStatement());
			if (getConnect() != null)
				Reporter.log("[INFO] - Executed | Connect Database Successfully.");

			if (getState() != null)
				Reporter.log("[INFO] - Executed | Connect Statement Successfully.");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static ArrayList<String> executeQuery3(String query) {

		ArrayList<String> resultValues = new ArrayList<String>();
		ResultSet resultSet;
		try {
			resultSet = getState().executeQuery(query);
			while (resultSet.next()) {

				int columnCount = resultSet.getMetaData().getColumnCount();
				StringBuilder stringBuilder = new StringBuilder();

				for (int iCounter = 1; iCounter <= columnCount; iCounter++) {

					stringBuilder.append(resultSet.getString(iCounter).trim() + " ");
				}
				String reqValue = stringBuilder.substring(0, stringBuilder.length() - 1);
				resultValues.add(reqValue);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NullPointerException ex) {
			Reporter.log("No Records found for this specific query" + ex.getStackTrace());
		}
		return resultValues;
	}

	public DBProcessor executeQuery2(String query) {

		try {
//			ResultSet resultSet = state.executeQuery(query);
			ResultSet resultSet = getState().executeQuery(query);
			while (resultSet.next()) {
				String c1 = resultSet.getString(1);
				String c2 = resultSet.getString(2);
				String c3 = resultSet.getString(3);
				String c4 = resultSet.getString(4);
				String c5 = resultSet.getString(5);
				String c6 = resultSet.getString(6);
				String c7 = resultSet.getString(7);
				String c8 = resultSet.getString(8);
				String c9 = resultSet.getString(9);
				Reporter.log("[INFO] - Executed | " + c1 + " || " + c2 + " || " + c3 + " || " + c4 + " || " + c5 + " || " + c6 + " || " + c7 + " || " + c8
						+ " || " + c9);
			}

		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
//			disconnect();
		}
		return (DBProcessor) this;
	}

	public DBProcessor executeQuery(String query) {

		try {
//			ResultSet resultSet = state.executeQuery(query);
			ResultSet resultSet = getState().executeQuery(query);
			resultSet.last();
			int rows = resultSet.getRow();
			ResultSetMetaData metaData = (ResultSetMetaData) resultSet.getMetaData();
			int cols = metaData.getColumnCount();
			Reporter.log("[INFO] - Executed | " + rows + "--" + cols);
			String[][] inputArr = new String[rows][cols];
			int i = 0;
			resultSet.beforeFirst();

			while (resultSet.next()) {
				for (int j = 0; j < cols; j++) {

					inputArr[i][j] = resultSet.getString(j + 1);
					Reporter.log(inputArr[i][j]);
				}
			}

		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
//			disconnect();
		}
		return (DBProcessor) this;
	}

	public void disconnectDB() {

		try {
			if (getState() != null) {
				getState().close();
				Reporter.log("[INFO] - Executed | Database Statement Is Closed.");
			}
			if (getConnect() != null) {
				getConnect().close();
				Reporter.log("[INFO] - Executed | Database Connection Is Closed.");
			}
		} catch (SQLException ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
	}

}