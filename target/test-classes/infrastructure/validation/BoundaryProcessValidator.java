package infrastructure.validation;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.selenium.SeleniumBaseCore;

public class BoundaryProcessValidator extends SeleniumBaseCore {

	public BoundaryProcessValidator(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	static ArrayList<Object> inputData = new ArrayList<>();
	static ArrayList<Object> outputData = new ArrayList<>();

	public ArrayList<Object> getInputData(String[] elementInputArray, int length) {

		inputData.clear();

		String[] inputArr = elementInputArray;

		for (int i = 0; i < inputArr.length; i++) {

			inputData.add(length);
			CommonLibrariesCore.APP_LOGS("Get Input Text Attribute Value: " + getTextAttribute(inputArr[i]) + " --> In Locator: " + inputArr[i] + " |");
		}
		Reporter.log("\n [INFO] - Executed | ==================================== \n", true);

		return inputData;
	}

	public ArrayList<Object> getOutputData(String[] elementOutputArray, int boundary) {

		outputData.clear();

		String[] outputArr = elementOutputArray;

		for (int i = 0; i < outputArr.length; i++) {

			if (outputArr[i] != null && boundary == -1)
				outputData.add(getTextAttribute(outputArr[i]).length() + 1);

			else if (outputArr[i] != null && boundary == 0)
				outputData.add(getTextAttribute(outputArr[i]).length());

			else if (outputArr[i] != null && boundary == 1)
				outputData.add(getTextAttribute(outputArr[i]).length());
		}
		Reporter.log("\n [INFO] - Executed | ==================================== \n", true);

		return outputData;
	}

	public void assertData() {

		status = inputData.equals(outputData);
		if (status == true) {
			CommonLibrariesCore.APP_LOGS("\t TEXT STRING INPUT / OUTPUT DATA: \n\t" + inputData + " == \n\t" + outputData);

		} else if (status == false) {
			CommonLibrariesCore.APP_LOGS_FAIL("\t TEXT STRING INPUT / OUTPUT DATA: \n\t" + inputData + " == \n\t" + outputData);
		}
		assertEquals(inputData, outputData);
	}

}
