package infrastructure.appium;

import org.openqa.selenium.support.PageFactory;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.thread.CentralBaseProcessor;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class IOSBaseCore extends CentralBaseProcessor {

	IOSDriver<MobileElement> driver = (IOSDriver<MobileElement>) appDriver();

	public IOSBaseCore(IOSDriver<MobileElement> driver) {
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		// TODO Auto-generated constructor stub
	}

	protected void shake() {
		driver.shake();
	}

	protected void getIOSBatteryInfo() {
		driver.getBatteryInfo();
	}

}