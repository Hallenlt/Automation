package infrastructure.appium;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofSeconds;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.time.DateTimeException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.mobile.NetworkConnection;
import org.openqa.selenium.mobile.NetworkConnection.ConnectionType;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.google.common.io.Files;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.root.CommonRandomCore;
import infrastructure.thread.CentralBaseProcessor;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.ElementOption;

public abstract class AppiumBaseCore extends CentralBaseProcessor {

	NetworkConnection network = (NetworkConnection) appDriver();

	public AppiumBaseCore(AppiumDriver<MobileElement> appDriver) {
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
		this.appDriver = appDriver;
		// TODO Auto-generated constructor stub
	}

	public AppiumDriver<MobileElement> getMobileDriver() {

		if (Platform.getCurrent().equals("Android")) {

		} else if (Platform.getCurrent().equals("iOS")) {

		}
		return appDriver;
	}

	public void detectBaseAI() {

		appDriver.findElement(MobileBy.custom("ai:clock")).click();
	}

	// ===================================================
	// Code part used to handle network connection below
	// ===================================================

	protected void setNoneNetworkConnection() {

		if (network.getNetworkConnection() != ConnectionType.NONE) {
			network.setNetworkConnection(ConnectionType.NONE);
		}
	}

	protected void setAirPlanConnection() {

		if (network.getNetworkConnection().isAirplaneMode()) {
			network.setNetworkConnection(ConnectionType.AIRPLANE_MODE);
		}
	}

	protected void setDataConnection() {

		if (network.getNetworkConnection().isDataEnabled()) {
			network.setNetworkConnection(ConnectionType.DATA);
		}
	}

	protected void setWifiConnection() {

		if (network.getNetworkConnection().isWifiEnabled()) {
			network.setNetworkConnection(ConnectionType.WIFI);
		}
	}

	protected void setAllNetworksConnection() {

		if (network.getNetworkConnection() != ConnectionType.ALL) {
			network.setNetworkConnection(ConnectionType.ALL);
		}
	}

	// ===================================================
	// Code part used to handle touch gestures below
	// ===================================================

	public void tapByElement(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | : " + locator + " |", true);
		try {
			new TouchAction(appDriver).tap(tapOptions().withElement(ElementOption.element(locator)))
					.waitAction(waitOptions(Duration.ofSeconds(WAIT_TIMEOUT_IN_SECONDS))).perform();
			APP_LOGS(" : " + locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception
			APP_LOGS_FAIL(" : " + locator + " |");
			fail(null, e);
		}
	}

	public void tapByCoordinates(int x, int y) {

		new TouchAction(appDriver).tap(point(x, y)).waitAction(waitOptions(Duration.ofMillis(250))).perform();
	}

	public void pressByElement(MobileElement locator) {

		new TouchAction(appDriver).press(ElementOption.element(locator)).waitAction(waitOptions(ofSeconds(WAIT_TIMEOUT_IN_SECONDS))).release().perform();
	}

	public void pressByCoordinates(int x, int y) {

		new TouchAction(appDriver).press(point(x, y)).waitAction(waitOptions(ofSeconds(WAIT_TIMEOUT_IN_SECONDS))).release().perform();
	}

	public void horizontalSwipeByPercentage(double startPercentage, double endPercentage, double anchorPercentage) {

		Dimension size = appDriver.manage().window().getSize();
		int anchor = (int) (size.height * anchorPercentage);
		int startPoint = (int) (size.width * startPercentage);
		int endPoint = (int) (size.width * endPercentage);

		new TouchAction(appDriver).press(point(startPoint, anchor)).waitAction(waitOptions(ofSeconds(WAIT_TIMEOUT_IN_SECONDS))).moveTo(point(endPoint, anchor))
				.release().perform();
	}

	public void verticalSwipeByPercentages(double startPercentage, double endPercentage, double anchorPercentage) {

		Dimension size = appDriver.manage().window().getSize();
		int anchor = (int) (size.width * anchorPercentage);
		int startPoint = (int) (size.height * startPercentage);
		int endPoint = (int) (size.height * endPercentage);

		new TouchAction(appDriver).press(point(anchor, startPoint)).waitAction(waitOptions(ofSeconds(WAIT_TIMEOUT_IN_SECONDS))).moveTo(point(anchor, endPoint))
				.release().perform();
	}

	public void swipeByElements(MobileElement startLocator, MobileElement targetLocator) {

		int startX = startLocator.getLocation().getX() + (startLocator.getSize().getWidth() / 2);
		int startY = startLocator.getLocation().getY() + (startLocator.getSize().getHeight() / 2);

		int endX = targetLocator.getLocation().getX() + (targetLocator.getSize().getWidth() / 2);
		int endY = targetLocator.getLocation().getY() + (targetLocator.getSize().getHeight() / 2);

		new TouchAction(appDriver).press(point(startX, startY)).waitAction(waitOptions(ofSeconds(WAIT_TIMEOUT_IN_SECONDS))).moveTo(point(endX, endY)).release()
				.perform();
	}

	public void swipeByElement(MobileElement locator) {

		int endX = locator.getLocation().getX() + (locator.getSize().getWidth() / 2);
		int endY = locator.getLocation().getY() + (locator.getSize().getHeight() / 2);

		new TouchAction(appDriver).press(point(0, 0)).waitAction(waitOptions(ofSeconds(WAIT_TIMEOUT_IN_SECONDS))).moveTo(point(endX, endY)).release().perform();
	}

	public void multiTouchByElement(MobileElement locator) {

		TouchAction press = new TouchAction(appDriver).press(ElementOption.element(locator)).waitAction(waitOptions(ofSeconds(WAIT_TIMEOUT_IN_SECONDS)))
				.release();
		new MultiTouchAction(appDriver).add(press).perform();
	}

	// ===================================================
	// Code part used to handle searching below
	// ===================================================

	public void findItemByAllInDataTable(MobileElement nextbutton, String expected) {

		Reporter.log("\n [INFO] - Executed | Finding Item By All In Data Table: " + expected + " |", true);

		int pageindex = 0;

		loop: while (true) {
			waitForPresenceOfElement(By.tagName("table"));
			waitForPresenceOfElement(By.tagName("tr"));

			MobileElement tbl = appDriver.findElement(By.tagName("table"));
			List<MobileElement> rows = tbl.findElements(By.tagName("tr"));
			try {
				for (int i = 0; i < rows.size(); i++) {

					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("arguments[0].scrollIntoView();", rows.get(i));
					List<MobileElement> cols = rows.get(i).findElements(By.tagName("th"));

					for (int j = 0; j < cols.size(); j++) {
						jse.executeScript("arguments[0].scrollIntoView();", cols.get(j));
						String actual = cols.get(j).getText();

						if (actual.contains(expected)) {
							APP_LOGS("Item By All Was Found In Data Table At The Position: [" + i + "] " + actual);
							break loop;
						}
					}
				}
			} catch (WebDriverException e) {
				// TODO: handle exception

				APP_LOGS_FAIL(e.getMessage());
				fail(null, e);
			}

			if (nextbutton.isEnabled()) {
				nextbutton.click();
				APP_LOGS("Went To The Next Page Index: " + (pageindex + 1));

			} else {
				APP_LOGS("Went to The Lastest Page Index: " + (pageindex));
				APP_LOGS_FAIL("Item By All Not Found In Data Table: " + expected);
				break loop;
			}
			pageindex++;
		}
	}

	public void findItemBySpecificInDataTable(String nextbutton, String column, String expected) {

		Reporter.log("\n [INFO] - Executed | Finding Item By Specific On Data Table: " + expected + " |", true);

		int pageindex = 0;

		loop: while (true) {
			waitForPresenceOfElement(By.xpath(column));
			List<MobileElement> cols = appDriver.findElements(By.xpath(column));
			try {
				Thread.sleep(500);
				for (int i = 0; i < cols.size(); i++) {

					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("arguments[0].scrollIntoView();", cols.get(i));
					String actual = cols.get(i).getText();

					if (actual.contains(expected)) {
						APP_LOGS("Item By Specific Was Found On Data Table At The Position: [" + i + "] " + actual);
						break loop;
					}
				}
			} catch (WebDriverException | InterruptedException e) {
				// TODO: handle exception

				APP_LOGS_FAIL(e.getMessage());
				fail(null, e);
			}
			MobileElement nextButton = appDriver.findElement(By.xpath(nextbutton));

			if (nextButton.isEnabled()) {
				nextButton.click();
				APP_LOGS("Went To The Next Page: " + (pageindex + 1));
			} else {
				APP_LOGS("Went to The Lastest Page Index: " + pageindex);
				APP_LOGS_FAIL("Item By Specific Not Found On Data Table: " + expected);
				break loop;
			}
			pageindex++;
		}
	}

	// ===================================================
	// Code part used to handle file attachment below
	// ===================================================

	public void assertInvalidBrokenImage() {

		Reporter.log("\n [INFO] - Executed | Assert Invalid Broken Image" + " |", true);
		int invalidImageCount = 0;
		List<MobileElement> imagesList = appDriver.findElements(By.tagName("img"));
		APP_LOGS("Total No. Of Available Images: " + imagesList.size());

		for (MobileElement imglocator : imagesList) {
			if (imglocator != null) {
				try {
					HttpClient client = HttpClientBuilder.create().build();
					HttpGet request = new HttpGet(imglocator.getAttribute("src"));
					HttpResponse response = client.execute(request);
					if (response.getStatusLine().getStatusCode() != 200)
						invalidImageCount++;
				} catch (IOException e) {
					Reporter.log(e.getMessage(), true);
				}
			}
		}
		if (invalidImageCount > 0)
			APP_LOGS_FAIL("Total No. Of Invalid Broken Images: " + invalidImageCount);
		else
			APP_LOGS("Total No. Of Invalid Broken Images: " + invalidImageCount);
	}

	protected void uploadFile(MobileElement locator, String filepath) {

		Reporter.log("\n [INFO] - Executed | File Uploading" + " |", true);
		locator.sendKeys(System.getProperty("user.dir") + filepath);
		APP_LOGS("File Is Uploaded" + " |");
	}

	protected void uploadFile2(MobileElement locator, String filepath, String filename) {

		Reporter.log("\n [INFO] - Executed | File Uploading" + " |", true);
		try {
			click(locator);
			Runtime.getRuntime().exec("Script.exe");
		} catch (IOException e) {
			// TODO Auto-generated catch block

			APP_LOGS_FAIL("File Is Uploaded" + " |");
			fail(null, e);
		}
		StringSelection select;
		select = new StringSelection(filepath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			// TODO Auto-generated catch block

			fail(null, e);
		}
		isFileExisting(filepath, filename);
		APP_LOGS("File Is Uploaded" + " |");
	}

	public void downloadFile(MobileElement locator, String filename, String filepath) {

		Reporter.log("\n [INFO] - Executed | File Downloading" + " |", true);
		try {
			click(locator);
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyPress(KeyEvent.KEY_RELEASED);
		} catch (AWTException e) {
			// TODO Auto-generated catch block

			fail(null, e);
		}
		isFileExisting(filepath, filename);
		APP_LOGS("File Is Downloaded" + " |");
	}
	
	public boolean isFileExisting(String filepath, String filename) {

		boolean flag = false;
		File dir = new File(filepath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {

			if (dir_contents[i].equals(filename))
				return flag = true;
		}
		return flag;
	}

	// ===================================================
	// Code part used to declaring variables above
	// ===================================================

	public void refreshPage() {

		Reporter.log("\n [INFO] - Executed | Refreshing Present Page |", true);
		APP_LOGS("Refreshed Present Page |");
		try {
			Thread.sleep(1000);
			appDriver.navigate().refresh();
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void switchingHandleTabs2(Method method) {

		try {
//			(new WebDriverWait(appDriver, WAIT_TIMEOUT_IN_SECONDS)).until(ExpectedConditions.numberOfWindowsToBe(2));
			String winHandleBefore = appDriver.getWindowHandle();
			for (String winHandle : appDriver.getWindowHandles()) {
				appDriver.switchTo().window(winHandle);
			}
			String getTitle = "";
			getTitle = appDriver.getTitle();
			Reporter.log("\n [INFO] - Executed | Switching Handle New Tab For: " + getTitle + " |", true);

			method.getName();

			appDriver.close();
			appDriver.switchTo().window(winHandleBefore);
			APP_LOGS("Switching Handle New Tab For: " + getTitle + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Switching Handle New Tab |");
			fail(null, e);
		}
	}

	public void switchingHandleTabs(Method method) {

		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.open()");
			List<String> browserTabs = new ArrayList<String>(appDriver.getWindowHandles());
			appDriver.switchTo().window(browserTabs.get(1));
			String getTitle = "";
			getTitle = appDriver.getTitle();
			Reporter.log("\n [INFO] - Executed | Switching Handle New Tab For: " + getTitle + " |", true);

			method.getName();

			appDriver.close();
			appDriver.switchTo().window(browserTabs.get(0));
			APP_LOGS("Switching Handle New Tab For: " + getTitle + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Switching Handle New Tab |");
			fail(null, e);
		}
	}

	protected void switchingHandleWindows(Method method) {

		try {
//			(new WebDriverWait(appDriver, WAIT_TIMEOUT_IN_SECONDS)).until(ExpectedConditions.numberOfWindowsToBe(2));
			String MainWindow = appDriver.getWindowHandle();
			Set<String> set = appDriver.getWindowHandles();
			Iterator<String> iterator = set.iterator();
			while (iterator.hasNext()) {
				String ChildWindow = iterator.next();
				if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
					appDriver.switchTo().window(ChildWindow);
					String getTitle = "";
					getTitle = appDriver.getTitle();
					Reporter.log("\n [INFO] - Executed | Switching And Handling All Opened Window For: " + getTitle + " |", true);

					method.getName();

					APP_LOGS("Switching And Handling All Opened Window For: " + getTitle + " |");
				}
			}
			appDriver.switchTo().window(MainWindow);
		} catch (WebDriverException e) {

			APP_LOGS_FAIL("Switching And Handling All Opened Window |");
			fail(null, e);
		}
	}

//		public void switchTabs() {
	//
//			selenium.switchingHandleTabs(override_switchingHandleTabs());
//		}
//		public Method override_switchingHandleTabs() {
	//
//			return null;
//		}

	protected void mediaPlayer(MobileElement tagname) {

		Reporter.log("\n [INFO] - Executed | Playing Media: " + tagname + " |", true);
		waitForPresenceOfElement(tagname);
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].play();", tagname);
//				jse.executeScript("arguments[0].setVolume(50);", locator);
//				jse.executeScript("arguments[0].setMute(true);", locator);
//				jse.executeScript("arguments[0].setMute(false);", locator);
			jse.executeScript("arguments[0].pause()", tagname);
			APP_LOGS("Played Media: " + tagname + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Played Media: " + tagname + " |");
			fail(null, e);
		}
	}

	// =========================================================
	// Code part used to indicate screenshot / capture below
	// =========================================================

	public String captureElement(MobileElement locator, String destination, String name) {

		String dest = destination + name;
		File file = new File(dest);
		try {
			File elementShot = locator.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(elementShot, file);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			fail(null, e);
		}
		return dest;
	}

	public String capture(String destination, String name) {

		TakesScreenshot ts = (TakesScreenshot) appDriver;
		File screenshot = null;
		String dest = destination + name;
		File file = null;
		file = new File(dest);
		try {
			screenshot = ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshot, file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			fail(null, e);
		}
		return dest;
	}

	public String capture(String name) {

		TakesScreenshot ts = (TakesScreenshot) appDriver;
		String dest = ".\\evidences\\" + name + ".png";
		File file = new File(dest);
		try {
			File screenshot = ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshot, file);
		} catch (Exception e) {
			// TODO: handle exception
			fail(null, e);
		}
		return dest;
	}

	protected void captureScreenshot() {

		Reporter.log("\n [INFO] - Executed | Screenshot Capture Entire Screen: " + appDriver.getTitle() + " |", true);
		BufferedImage image = null;
		try {
			image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
		} catch (HeadlessException | AWTException e) {
			// TODO Auto-generated catch block
		}
		String title = appDriver.getTitle().replaceAll("\\W", "");
		Random random = new Random();
		try {
			status = ImageIO.write(image, "jpg", new File(".\\evidences\\" + title + random.nextInt() + ".jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Reporter.log(e.getMessage(), true);
		}
		Reporter.log("\n [INFO] - Executed | Screenshot Captured Entire Screen: " + appDriver.getTitle() + " == " + status + " |", true);
	}

	// =========================================================
	// Code part used to get text attribute below
	// =========================================================

	public String getTextAttribute(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Getting Text / Attribute In Attribute: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			String text = "";
			text = locator.getText().toString().trim();
			return text;
		} catch (NullPointerException e) {
			// TODO: handle exception
			String attribute = "";
			attribute = locator.getAttribute("text").toString().trim();
			return attribute;
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Get Text / Attribute In Locator: " + locator + " |");
		}
		return null;
	}

	protected void alertDismiss() {

		Reporter.log("\n [INFO] - Executed | Dissmissing Alert |", true);
		try {
			alert = wait.until((ExpectedConditions.alertIsPresent()));
			alert = appDriver.switchTo().alert();
			alert.dismiss();
			APP_LOGS("Dismissed Alert" + alert.getText() + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Dismissed Alert" + alert.getText() + " |");
			fail(null, e);
		}
	}

	protected void alertAccept() {

		Reporter.log("\n [INFO] - Executed | Accepting Alert |", true);
		try {
			alert = wait.until((ExpectedConditions.alertIsPresent()));
			alert = appDriver.switchTo().alert();
			alert.accept();
			APP_LOGS("Accepted Alert: " + alert.getText() + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Accepted Alert" + alert.getText() + " |");
			fail(null, e);
		}
	}

	public void getURL(String url) {

		Reporter.log("\n [INFO] - Executed | Opening URL Path Navigation To: " + url + " |", true);
		APP_LOGS("Opening URL Path Navigation To: " + url + " |");
		try {
			appDriver.get(url);
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Opened URL Path Navigation To: " + url + " |");
			fail(null, e);
		}
	}

	public void navigateToURL(String url) {

		Reporter.log("\n [INFO] - Executed | Opening URL Path Navigation To: " + url + " |", true);
		APP_LOGS("Opening URL Path Navigation To: " + url + " |");
		try {
			appDriver.navigate().to(url);
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Opening URL Path Navigation To: " + url + " |");
			fail(null, e);
		}
	}

	protected void selectAllRadioButton(List<MobileElement> locator) {

		Reporter.log("\n [INFO] - Executed | Selecting Radio Button: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			// wait.until((ExpectedConditions.elementToBeClickable((By) locator));
			status = locator.get(0).isSelected();
			if (status == true) {
				locator.get(1).click();
				APP_LOGS("The First Radio Button Is Selected ==> Select The Second: " + locator + " |");
			} else {
				locator.get(0).click();
				APP_LOGS("The Second Radio Button Is Selected ==> Select The First: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Selecting Radio Button: " + locator + " |");
			fail(null, e);
		}
	}

	protected void selectDropdownElective(MobileElement locator, MobileElement parent_locator, String child_tagname) {

		Reporter.log("\n [INFO] - Executed | Selecting Dropdown In Locator: " + locator + " --> " + child_tagname + " |", true);
		waitForPresenceOfElement(locator);
		try {
			Thread.sleep(500);
			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			if (locator.isEnabled()) {
				locator.click();
			}
			Thread.sleep(500);
			List<MobileElement> list = parent_locator.findElements(By.tagName(child_tagname));
			int size = list.size();
			int randomValue = ThreadLocalRandom.current().nextInt(size - 1) + 1;
			if (list.get(randomValue).isEnabled()) {
				list.get(randomValue).click();
			}
			APP_LOGS("Selected Dropdown In Locator: " + locator + " --> " + child_tagname + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Selected Dropdown In Locator: " + locator + " --> " + child_tagname + " |");
			fail(null, e);
		}
	}

	protected void selectDropdownButton(MobileElement locator, List<MobileElement> tagname, int index) {

		Reporter.log("\n [INFO] - Executed | Selected Dropdown In Index [" + index + "] At Locator: " + locator + " --> " + tagname + " |", true);
		waitForPresenceOfElement(locator);
		try {
			Thread.sleep(500);
			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			if (locator.isEnabled()) {
				locator.click();
			}
			waitForPresenceOfElement((MobileElement) tagname);
			tagname.get(index).click();
			APP_LOGS("Selected Dropdown In Index [" + index + "] At Locator: " + locator + " --> " + tagname + " |");
		} catch (InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Selected Dropdown In Index [" + index + "] At Locator: " + locator + " --> " + tagname + " |");
			fail(null, e);
		}
	}

	protected void selectDropdownButton(MobileElement locator, MobileElement child_locator) {

		Reporter.log("\n [INFO] - Executed | Selecting Dropdown In Locator: " + locator + " --> " + child_locator + " |", true);
		try {

			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			if (locator.isEnabled()) {
				locator.click();
			}
			waitForPresenceOfElement(locator);
			// wait.until((ExpectedConditions.elementToBeClickable(locator =
			// child_locator));
			if (locator.isEnabled()) {
				locator.click();
				APP_LOGS("Selected Dropdown In Locator: " + locator + " --> " + child_locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Selected Dropdown In Locator: " + locator + " --> " + child_locator + " |");
			fail(null, e);
		}
	}

	protected void selectDropdown(MobileElement locator, int index) {

		Reporter.log("\n [INFO] - Executed | Selecting Dropdown In Index [" + index + "] At Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			// wait.until((ExpectedConditions.elementSelectionStateToBe(locator, false));
			Select dropDown = new Select(locator);
			dropDown.selectByIndex(index);
			APP_LOGS("Selected Dropdown In Index [" + index + "] At Locator: " + locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("[INFO] - Executed | Selected Dropdown In Index [" + index + "] At Locator: " + locator + " |");
			fail(null, e);
		}
	}

	// ===================================================
	// Code part used to handle check-box below
	// ===================================================

	protected void check(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Selecting Checkbox: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {

			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			if (locator.isEnabled()) {
				locator.click();
			}
			APP_LOGS("Checkbox Is selected: " + locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Checkbox Is selected: " + locator + " |");
			fail(null, e);
		}
	}

	protected void checkAllCheckbox(MobileElement locator, String child_tagname) {

		Reporter.log("\n [INFO] - Executed | Selecting All Checkbox In Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {

			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			List<MobileElement> locators = locator.findElements(By.tagName(child_tagname));
			for (MobileElement el : locators) {
				el.click();
			}
			APP_LOGS("All Checkbox Is Selected: " + locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("All Checkbox Is Selected: " + locator + " |");
			fail(null, e);
		}
	}

	protected void checkboxIsUnselected(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Checking If Checkbox Is Inactive: " + locator + " |", true);
		waitForPresenceOfElement(locator);

		String attribute = "";
		attribute = locator.getAttribute("checked");
		if (attribute != null) {
			APP_LOGS_FAIL("Expect Checkbox Is Inactive [Unselected] But Found The Opposite: " + locator + " |");
			fail();
		} else {
			APP_LOGS("Expect Checkbox Is Inactive [UnSelected]: " + locator + " |");
		}
	}

	protected void checkboxIsSelected(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Checking If Checkbox Is Active: " + locator + " |", true);
		waitForPresenceOfElement(locator);

		String attribute = "";
		attribute = locator.getAttribute("checked");
		if (attribute != null) {
			APP_LOGS("Expect Checkbox Is Active [Selected]: " + locator + " |");
		} else {
			APP_LOGS_FAIL("Expect Checkbox Is Active [Selected] But Found The Opposite: " + locator + " |");
			fail();
		}
	}

	// ===================================================
	// Code part used to handle event on click below
	// ===================================================

	public void submit(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Submitting On Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {

			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			if (locator.isEnabled()) {
				locator.submit();
				APP_LOGS("Submitted On Locator: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exceptionF

			APP_LOGS_FAIL("Submitted On Locator: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickAndHold(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Clicking And Holding On Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			actions = new Actions(appDriver);
			if (locator.isEnabled()) {
				actions.moveToElement(locator).clickAndHold().build().perform();
				APP_LOGS("Clicked And Holded On Locator: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked And Holded On Locator: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickInvisibleHidden(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On Invisible Hidden Locator: " + locator + " |", true);
		try {
			if (locator.isEnabled()) {
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("return arguments[0].click();", locator);
				locator.click();
				APP_LOGS("Clicked On Invisible Hidden Locator: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On Invisible Hidden Locator: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickHref(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On Href Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			Thread.sleep(500);
			locator.click();
			APP_LOGS("Clicked On Href Locator: " + locator + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			// Ignore locator Click Intercepted Exception
		}
	}

	public void clickIgnoreScroll(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On Locator: " + locator + " |", true);
		try {
			Thread.sleep(500);

			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			if (locator.isEnabled()) {
				locator.click();
				APP_LOGS("Clicked On Locator: " + locator + " |");
			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On Locator: " + locator + " |");
			fail(null, e);
		}
	}

	public void click(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			Thread.sleep(500);
			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			if (locator.isEnabled()) {
				locator.click();
//		APP_LOGS("Clicked On Locator: " + locator + " |");
			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception
//	    APP_LOGS_FAIL("Clicked On Locator: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickItemInList(List<MobileElement> locator, int index) {

		Reporter.log("\n [INFO] - Executed | Clicking On Item In List: " + locator + " |", true);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		waitForPresenceOfElement(locator);
		try {
			if (locator.get(index).isEnabled()) {
				locator.get(index).click();
				APP_LOGS("Clicked On Item In List: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On Item In List: " + locator + " |");
			fail(null, e);
		}
	}

	protected void clickLastItemInList(List<MobileElement> locator) {

		Reporter.log("\n [INFO] - Executed | Clicking On Last Item In List: " + locator + " |", true);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		waitForPresenceOfElement((MobileElement) locator);
		try {
			int size = locator.size();
			if (locator.get(size - 1).isEnabled()) {
				locator.get(size - 1).click();
				APP_LOGS("Clicked On Last Item In List: " + locator + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On Last Item In List: " + locator + " |");
			fail(null, e);
		}
	}

	public void clickFirstItemInList(List<MobileElement> listItem) {

		Reporter.log("\n [INFO] - Executed | Clicking On First Item In List: " + listItem + " |", true);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		waitForPresenceOfElement(listItem);
		try {
			if (listItem.get(0).isEnabled()) {
				listItem.get(0).click();
				APP_LOGS("Clicked On First Item List: " + listItem + " |");
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Clicked On First Item In Data Row Table: " + listItem + " |");
			fail(null, e);
		}
	}

	// ===================================================
	// Code part used to move on locator below
	// ===================================================

	public void scrollIntoViewTop(By by) {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", by);
	}

	protected void scrollIntoViewBottom(By by) {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(false);", by);
	}

	public void scrollIntoViewTop(MobileElement locator) {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", locator);
	}

	protected void scrollIntoViewBottom(MobileElement locator) {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(false);", locator);
	}

	protected void dragVerticalSlider(MobileElement locator, int yOffset) {

		Reporter.log("\n [INFO] - Executed | Dragging The Horizontal Slider: " + locator + " --> With xOffset: " + yOffset + " |", true);
		waitForPresenceOfElement(locator);
		try {
			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			appDriver.switchTo().frame(0);
			actions = new Actions(appDriver);
			actions.dragAndDropBy(locator, 0, yOffset).build().perform();
			actions.release();
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Dragged The Horizontal Slider: " + locator + " --> With xOffset: " + yOffset + " |");
			fail(null, e);
		}
		APP_LOGS("Dragged The Horizontal Slider: " + locator + " --> With xOffset: " + yOffset + " |");
	}

	protected void dragHorizontalSlider(MobileElement locator, int xOffset) {

		Reporter.log("\n [INFO] - Executed | Dragging The Horizontal Slider: " + locator + " --> With xOffset: " + xOffset + " |", true);
		waitForPresenceOfElement(locator);
		try {

			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			appDriver.switchTo().frame(0);
			actions = new Actions(appDriver);
			actions.dragAndDropBy(locator, xOffset, 0).build().perform();
			actions.release();
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Dragged The Horizontal Slider: " + locator + " --> With xOffset: " + xOffset + " |");
			fail(null, e);
		}
		APP_LOGS("Dragged The Horizontal Slider: " + locator + " --> With xOffset: " + xOffset + " |");
	}

	protected void dragAndDrop(MobileElement locator, int xOffset, int yOffset) {

		Reporter.log("\n [INFO] - Executed | Dragging And Dropping: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {

			// wait.until((ExpectedConditions.elementToBeClickable(locator));
			actions = new Actions(appDriver);
			actions.clickAndHold(locator).moveByOffset(xOffset, yOffset).release().build().perform();
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Dragged And Dropped: " + locator + " |");
			fail(null, e);
		}
		APP_LOGS("Dragged And Dropped: " + locator + " |");
	}

	protected void hoverMoveOn(int xOffset, int yOffset) {

		Reporter.log("\n [INFO] - Executed | Hover Move On: " + xOffset + " - " + yOffset + " |", true);
		try {
			Thread.sleep(500);
			actions = new Actions(appDriver);
			actions.moveByOffset(xOffset, yOffset).build().perform();
			APP_LOGS("Hover Move On: " + xOffset + "," + yOffset + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Hover Move On: " + xOffset + "," + yOffset + " |");
			fail(null, e);
		}
	}

	protected void hoverMoveOnLastItem(List<MobileElement> locator) {

		Reporter.log("\n [INFO] - Executed | Hover Move On: " + locator + " |", true);
		try {
			Thread.sleep(500);
			int size = locator.size();
			actions = new Actions(appDriver);
			if (locator.get(size - 1).isEnabled()) {
				actions.moveToElement(locator.get(size - 1)).build().perform();
				APP_LOGS("Hove Move On: " + locator + " |");
			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Hover Move On: " + locator + " |");
			fail(null, e);
		}
	}

	protected void hoverMoveOnFirstItem(List<MobileElement> locator) {

		Reporter.log("\n [INFO] - Executed | Hover Move On: " + locator + " |", true);
		try {
			Thread.sleep(500);
			actions = new Actions(appDriver);
			if (locator.get(0).isEnabled()) {
				actions.moveToElement(locator.get(0)).build().perform();
				APP_LOGS("Hove Move On: " + locator + " |");
			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Hover Move On: " + locator + " |");
			fail(null, e);
		}
	}

	protected void hoverMoveOn(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Hover Move On: " + locator + " |", true);
		try {
			Thread.sleep(500);

			actions = new Actions(appDriver);
			if (locator.isEnabled()) {
				actions.moveToElement(locator).build().perform();
				APP_LOGS("Hove Move On: " + locator + " |");
			}
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Hover Move On: " + locator + " |");
			fail(null, e);
		}
	}

	// ===================================================
	// Code part used to handle checking component below
	// ===================================================

	protected void checkIfElementIsDisabled(MobileElement locator, String cssValue, String expected) {

		Reporter.log("\n [INFO] - Executed | Checking If locator Is Disabled: " + locator + " --> Via CSS Value Attribute: " + cssValue + " |", true);
		try {
			Thread.sleep(500);

			String value = "";
			value = locator.getCssValue(cssValue);
			Reporter.log("\n [INFO] - Executed | Get CSS Value: " + value, true);
			if (value.contains(expected)) {
				APP_LOGS("CSS Attribute Is Disabled: " + cssValue + " --> With Value: " + value + " == " + expected + " |");
			} else {
				APP_LOGS_FAIL("CSS Attribute Is Enabled: " + cssValue + " --> With Value: " + value + " == " + expected + " |");
				assertEquals(value, expected);
			}
			APP_LOGS("Checked locator Is Disabled: " + locator + " --> Via CSS Value Attribute: " + cssValue + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Checked locator Is Disabled: " + locator + " --> Via CSS Value Attribute: " + cssValue + " |");
			fail(null, e);
		}
	}

	protected void checkIfButtonCursorIsDisabled(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Checking If locator Is Disabled: " + locator + " --> Via CSS Value Attribute: cursor" + " |", true);
		try {
			Thread.sleep(500);

			String value = "";
			value = locator.getCssValue("cursor");
			if (value.contains("not-allowed")) {
				APP_LOGS("CSS Attribute Is Disabled: cursor" + " --> With Value: " + value + " |");
			} else {
				APP_LOGS_FAIL("[INFO] - Executed | CSS Attribute Is Enabled: cursor" + " --> With Value: " + value + " |");
				assertTrue(value.contains("not-allowed"));
			}
			APP_LOGS("Checked locator Is Disabled: " + locator + " --> Via CSS Value Attribute: cursor" + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Checked locator Is Disabled: " + locator + " --> Via CSS Value Attribute: cursor" + " |");
			fail(null, e);
		}
	}

	protected void checkIfElementColourIsDisabled(MobileElement locator, String expected) {

		Reporter.log("\n [INFO] - Executed | Checking If locator Is Disabled By Colour: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			Thread.sleep(500);

			String color = "";
			color = locator.getCssValue("background-color");
			APP_LOGS_FAIL("CSS Value By Background Color: " + color + " |");
			String hex = Color.fromString(color).asHex();
			if (hex.contains(expected)) {
				APP_LOGS("Asserted / Validated CSS Value As Hex. locator Is Disabled By Colour: \n" + hex + " == " + expected + " |");
			} else {
				APP_LOGS_FAIL("[INFO] - Executed | Asserted / Validated CSS Value As Hex. locator Is Enabled By Colour: \n" + hex + " == " + expected + " |");
				assertEquals(hex, expected);
			}
			APP_LOGS_FAIL("Checked locator Is Disabled By Colour: " + locator + " |");
		} catch (WebDriverException | InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Checked locator Is Disabled By Colour: " + locator + " |");
			fail(null, e);
		}
	}

	protected void checkIfElementIsUndisplayed(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Checking If locator Is Undisplayed: " + locator + " |", true);
		String returnActual = "locator Is Displayed (False): ";
		String returnExpected = "locator Is Not Displayed (True): ";
		try {
			Thread.sleep(500);
			status = locator.isDisplayed() && ((List<MobileElement>) locator).size() > 0;
			if (status != null && status == true) {
				APP_LOGS_FAIL("Checked If locator Is Undisplayed: " + locator + " |");
				assertEquals(returnActual, returnExpected);
			}
		} catch (InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("" + "Checked If locator Is Undisplayed: " + locator + " |");
		}
//			APP_LOGS("" + returnExpected + locator + " |");
	}

	protected void checkIfElementIsDisplayed(MobileElement locator) {

		Reporter.log("\n [INFO] - Executed | Checking If locator Is Displayed: " + locator + " |", true);
		String returnExpected = "locator Is Displayed (False): ";
		String returnActual = "locator Is Not Displayed (True): ";
		try {
			Thread.sleep(500);
			status = locator.isDisplayed() && ((List<MobileElement>) locator).size() != 0;
			if (status != null && status == true) {
				APP_LOGS("Checked If locator Is Displayed: " + locator + " |");
			}
		} catch (InterruptedException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("" + "Checked If locator Is Displayed: " + locator + " |");
			assertEquals(returnActual, returnExpected);
		}
	}

	// ===============================================================
	// Code part used to handle assertion / validation data below
	// ===============================================================

	protected void assertIsFirstItemInCustomTable(MobileElement table, String body, String rows, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is First Item In Data Row Table: " + rows + " |", true);
		waitForPresenceOfElement(table);
		waitForPresenceOfElement(By.xpath(body));
		try {
			MobileElement tableBody = table.findElement(By.xpath(body));
			List<MobileElement> tableRows = tableBody.findElements(By.xpath(rows));
			String actual = "";
			actual = tableRows.get(0).getText();
			if (actual.contains(expected) == true) {
				APP_LOGS("Asserted Is First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + rows + " |");
			} else {
				APP_LOGS_FAIL("Asserted Is First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + rows + " |");
				assertEquals(actual, expected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void assertIsNotFirstItemInCustomTable(MobileElement table, String body, String rows, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is Not First Item In Data Row Table: " + rows + " |", true);
		String returnExpected = "Item Was Not The First In Data Table (True): ";
		String returnActual = "Item Was The First In Data Table (False): ";
		waitForPresenceOfElement(table);
		waitForPresenceOfElement(By.xpath(body));
		try {
			MobileElement tableBody = table.findElement(By.xpath(body));
			List<MobileElement> tableRows = tableBody.findElements(By.xpath(rows));
			String actual = "";
			actual = tableRows.get(0).getText();
			if (!actual.contains(expected) == true) {
				APP_LOGS("Asserted Is Not First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + rows + " |");
			} else {
				APP_LOGS_FAIL("Asserted Is Not First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + rows + " |");
				assertEquals(returnActual, returnExpected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void assertIsNotFirstItemInDataRowTable(MobileElement parent_row, String child_column, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is Not First Item In Data Row Table: " + child_column + " |", true);
		String returnExpected = "Item Was Not The First In Data Table (True): ";
		String returnActual = "Item Was The First In Data Table (False): ";
		waitForPresenceOfElement(parent_row);
		waitForPresenceOfElement(By.xpath(child_column));
		try {
			MobileElement row_list = parent_row.findElement(By.xpath(child_column));
			String actual = "";
			actual = row_list.getText();
			if (!actual.contains(expected) == true) {
				APP_LOGS("Asserted Is Not First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + child_column + " |");
			} else {
				APP_LOGS_FAIL("Asserted Is Not First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + child_column + " |");
				assertEquals(returnActual, returnExpected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void assertIsNotListItemsInDataTable(MobileElement locator, String tagname, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is Not List Items In Data Table: " + locator + " |", true);
		String returnExpected = "Item Was Not The First In Data Table (True): ";
		String returnActual = "Item Was The First In Data Table (False): ";
		waitForPresenceOfElement(locator);
		waitForPresenceOfElement(By.tagName(tagname));
		try {

			List<MobileElement> rows_list = locator.findElements(By.xpath(tagname));
			int rows_count = rows_list.size();

//				ArrayList<Object> listArray = new ArrayList<>();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < rows_count; i++) {
				sb.append("\n\t" + rows_list.get(i).getText());
//					listArray.add(rows_list.get(i).getText());
			}
			String actual = "";
			actual = sb.toString();
			if (!actual.contains(expected)) {
				APP_LOGS("Asserted Is Not List Item In Data Table: \n" + actual + " == " + expected + " --> At Locator: " + locator + " |");
			} else {
				APP_LOGS_FAIL("Asserted Is Not List Item In Data Table: \n" + actual + " == " + expected + " --> At Locator: " + locator + " |");
				assertEquals(returnActual, returnExpected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void assertIsFirstItemInDataRowTable(MobileElement parent_row, MobileElement child_column, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is First Item In Data Row Table: " + child_column + " |", true);
//	waitForPresenceOfElement(parent_row);
//	waitForPresenceOfElement(child_column);
		try {
			MobileElement row_list = parent_row;
			row_list = child_column;
			String actual = "";
			actual = row_list.getText();
			if (actual.contains(expected)) {
				APP_LOGS("Asserted Is First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + child_column + " |");
			} else {
				APP_LOGS_FAIL("Asserted Is First Item In Data Row Table: \n" + actual + " == " + expected + " --> At Locator: " + child_column + " |");
				assertEquals(actual, expected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL(e.getMessage());
			fail(null, e);
		}
	}

	protected void assertIsListItemsInDataTable(List<MobileElement> locator, String expected) {

		Reporter.log("\n [INFO] - Executed | Assert Is List Items In Data Table: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			int rows_count = locator.size();

//				ArrayList<Object> listArray = new ArrayList<>();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < rows_count; i++) {
				sb.append("\n\t" + locator.get(i).getText());
//					listArray.add(rows_list.get(i).getText());
			}
			String actual = "";
			actual = sb.toString();
			if (actual.contains(expected)) {
				APP_LOGS("Assert Is List Items In Data Table: \n" + actual + " == " + expected + " --> At Locator: " + locator + " |");
			} else {
				APP_LOGS_FAIL("Assert Is List Items In Data Table: \n" + actual + " == " + expected + " --> At Locator: " + locator + " |");
				assertEquals(actual, expected);
			}
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
	}

	protected void assertTitleURL(String expectTitle, String expectUrl) {

		Reporter.log("\n [INFO] - Executed | Asserting Page Title: " + expectTitle + " |", true);
		Reporter.log("\n [INFO] - Executed | Asserting URL Navigation: " + expectUrl + " |", true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			fail(null, e);
		}
//			(new WebDriverWait(appDriver, ELEMENT_WAIT_TIMEOUT_IN_SECONDS)).until(ExpectedConditions.titleIs(expectTitle));
		String actualTitle = "";
		actualTitle = appDriver.getTitle();
		status = expectTitle.equals(actualTitle);
		if (status == true) {
			APP_LOGS("Asserted Page Title: " + actualTitle + " == " + expectTitle + " |");
		}
//			(new WebDriverWait(appDriver, ELEMENT_WAIT_TIMEOUT_IN_SECONDS)).until(ExpectedConditions.urlToBe(expectUrl));
		String actualUrl = "";
		actualUrl = appDriver.getCurrentUrl();
		status = expectUrl.equals(actualUrl);
		if (status == true) {
			APP_LOGS("Asserted URL Navigation: " + actualUrl + " == " + expectUrl + " |");
		} else {
			APP_LOGS_FAIL("Asserted Page Title: " + actualTitle + " == " + expectTitle + " |");
			APP_LOGS_FAIL("Asserted URL Navigation: " + actualUrl + " == " + expectUrl + " |");
			assertEquals(actualTitle, expectTitle);
			assertEquals(actualUrl, expectUrl);
		}
	}

	protected void assertHasNotDataInPageSource(MobileElement locator, String expected) {

		Reporter.log("\n [INFO] - Executed | Asserting Has Not Data: " + expected + " --> In Page Source: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		String pageSrc = "";
		pageSrc = appDriver.getPageSource();
		if (!pageSrc.contains(expected)) {
			APP_LOGS("Asserted List: " + locator + " --> There Is Not Contains: " + expected);
		} else {
			APP_LOGS_FAIL("Asserted Data List: " + locator + " --> There Is Not Contains: " + expected);
			assertFalse(pageSrc.contains(expected));
		}
	}

	protected void assertHasDataInPageSource(MobileElement locator, String expected) {

		Reporter.log("\n [INFO] - Executed | Asserting Has Data: " + expected + " --> In Page Source: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		String pageSrc = "";
		pageSrc = appDriver.getPageSource();
		if (pageSrc.contains(expected)) {
			APP_LOGS("Asserted Data List: " + locator + " --> There Is Contains: " + expected);
		} else {
			APP_LOGS_FAIL("Asserted Data List: " + locator + " --> There Is Contains: " + expected);
			assertTrue(pageSrc.contains(expected));
		}
	}

	// ===================================================
	// Code part used to handle waiting below
	// ===================================================

	protected void waitForPresenceOfElement(By by) {

		scrollIntoViewBottom(by);
//		status = // wait.until((ExpectedConditions.presenceOfElementLocated(by)) != null;
//	status = // wait.until((ExpectedConditions.visibilityOflocatorLocated(By.xpath(locator))) != null;
		if (status == true) {

		} else {
			APP_LOGS_FAIL("Can Not To Determine Presence Of locator Located: " + by + " |");
		}
	}

	protected void waitForPresenceOfElement(List<MobileElement> locator) {

//	swipeByElement(locator);
//		status = // wait.until((ExpectedConditions.visibilityOf(locator.get(0))) != null;
		if (status == true) {

		} else {
			APP_LOGS_FAIL("Can Not To Determine Presence Of locator Located: " + locator + " |");
		}
	}

	protected void waitForPresenceOfElement(MobileElement locator) {

//	swipeByElement(locator);
//		status = // wait.until((ExpectedConditions.visibilityOf(locator)) != null;
		if (status == true) {

		} else {
			APP_LOGS_FAIL("Can Not To Determine Presence Of locator Located: " + locator + " |");
		}
	}

	protected boolean waitForElementToInvisible(By by) {

		try {
			// wait.until((ExpectedConditions.invisibilityOfElementLocated(by));
			return true;
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Can Not Determine Invisibility Of locator Located: " + by + " |");
			fail(null, e);
			return false;
		}
	}

	protected boolean waitForElementToInvisible(List<MobileElement> locator) {

		try {
			// wait.until((ExpectedConditions.invisibilityOf(locator.get(0)));
			return true;
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Can Not Determine Invisibility Of locator Located: " + locator + " |");
			fail(null, e);
			return false;
		}
	}

	protected boolean waitForElementToInvisible(MobileElement locator) {

		try {
			// wait.until((ExpectedConditions.invisibilityOf(locator));
			return true;
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Can Not Determine Invisibility Of locator Located: " + locator + " |");
			fail(null, e);
			return false;
		}
	}

	// =========================================================
	// Code part used to handle text field component below
	// =========================================================

	protected int sendKeysNumericBoundaryMaxLength(MobileElement locator, int boundary) {

		Reporter.log("\n [INFO] - Executed | Boundary Max Length Analysis: " + boundary + " |", true);
		try {

			String limit = "";
			limit = locator.getAttribute("maxlength").toString();
			int maxLength = Integer.parseInt(limit);
			sendKeys(locator, CommonRandomCore.randomNumeric(maxLength + boundary));
			APP_LOGS("Boundary Max Length Analysed: " + boundary + "--> Get Value: " + maxLength + " |");
			return maxLength;
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
		return boundary;
	}

	public int sendKeysAlphaBoundaryMaxLength(MobileElement locator, int boundary) {

		Reporter.log("\n [INFO] - Executed | Boundary Max Length Analysis: " + boundary + " |", true);
		try {

			String limit = "";
			limit = locator.getAttribute("maxlength").toString();
			int maxLength = Integer.parseInt(limit);
			sendKeys(locator, CommonRandomCore.randomAlphaNumeric(maxLength + boundary));
			APP_LOGS("Boundary Max Length Analysed: " + boundary + "--> Get Value: " + maxLength + " |");
			return maxLength;
		} catch (WebDriverException e) {
			// TODO: handle exception

			fail(null, e);
		}
		return boundary;
	}

	public AppiumBaseCore sendKeysRandomUUID(MobileElement locator) {

		String uuid = UUID.randomUUID().toString();
		sendKeys(locator, uuid);
		return this;
	}

	public AppiumBaseCore sendKeys(MobileElement locator, String value) {

		Reporter.log("\n [INFO] - Executed | Entering Input Text: " + value + " --> At Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {

			if (!locator.isEnabled()) {
				APP_LOGS_FAIL("Textbox Field Is Disabled Instead Of Enabled: " + locator);
			}
			locator.sendKeys(Keys.chord(Keys.CONTROL + "a"));
			locator.sendKeys(Keys.chord(Keys.DELETE));
			locator.sendKeys(value);
			APP_LOGS("Entered Input Text: " + value + " --> At Locator: " + locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Entered Input Text: " + value + " --> At Locator: " + locator + " |");
			fail(null, e);
		}
		return this;
	}

	public String sendKeysSelectInput(MobileElement locator, String value) {

		Reporter.log("\n [INFO] - Executed | Entering Input Text: " + value + " --> At Locator: " + locator + " |", true);
		waitForPresenceOfElement(locator);
		try {
			if (!locator.isEnabled()) {
				APP_LOGS_FAIL("Textbox Field Is Disabled Instead Of Enabled: " + locator);
			}
			actions = new Actions(appDriver);
			actions.moveToElement(locator).click();
			actions.sendKeys(value).build().perform();
			try {
				robot = new Robot();
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
			} catch (AWTException e) {
				// TODO Auto-generated catch block

				fail(null, e);
			}
			APP_LOGS("Entered Input Text: " + value + " --> At Locator: " + locator + " |");
		} catch (WebDriverException e) {
			// TODO: handle exception

			APP_LOGS_FAIL("Entered Input Text: " + value + " --> At Locator: " + locator + " |");
			fail(null, e);
		}
		return value;
	}

}
