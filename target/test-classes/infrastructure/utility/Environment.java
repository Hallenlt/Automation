package infrastructure.utility;

public enum Environment {

	TEST, STAGING, PRODUCTION
}