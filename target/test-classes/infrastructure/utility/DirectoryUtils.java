package infrastructure.utility;

public class DirectoryUtils {

	public static final String JAVA_PATH = System.getProperty("user.dir") + "\\src\\main\\java";

	public static final String RESOURCE_PATH = System.getProperty("user.dir") + "\\src\\main\\resources";

//    public static final String JAVA_PATH = System.getProperty("user.dir") + ".\\src\\main\\java";
//
//    public static final String RESOURCE_PATH = System.getProperty("user.dir") + ".\\src\\main\\resources";
	
	public static final String DATA_DRIVEN_PATH = RESOURCE_PATH + "\\test-data\\data-driven\\";

	public static final String MEDIA_PATH = RESOURCE_PATH + "\\test-data\\media\\";

	public static final String DOCUMENT_PATH = RESOURCE_PATH + "test-data\\documents\\";

	public static final String PDF_FILE_1 = RESOURCE_PATH + "\\test-data\\documents\\pdf-file-1.pdf";

	public static final String PDF_FILE_2 = RESOURCE_PATH + "\\test-data\\documents\\pdf-file-2.pdf";

	public static final String XML_FILE = RESOURCE_PATH + "\\test-data\\documents\\remote-20201501-151229-548.xml";

}