package infrastructure.utility;

import java.net.URL;

public class EnvironmentUtils {
	
	public static final String DB_TEST_URL = "jdbc:oracle:thin:@192.168.100.9:1521:IDS";
	public static final String USERNAME_TEST_DB = "BELGABOX_QC";
	public static final String PASSWORD_TEST_DB = "BELGABOX_QC";
	
	public static final String DB_STAG_URL = "";
	public static final String USERNAME_STAG_DB = "";
	public static final String PASSWORD_STAG_DB = "";

	public static final String ANDROID_APP_PACKAGE = "com.android.vending";

	public static final String ANDROID_APP_ACTIVITY = "com.google.android.finsky.activities.MainActivity";

	public static final String IOS_APP = "/abs/path/to/my.apk";
	
//	public static final String GRID_SERVER = "http://172.17.0.2:";

	public static final String GRID_SERVER = "http://localhost:";

	public static final String USER_AGENT = "Mozilla/5.0 (iPhone; CPU iPhone OS 13_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/80.0.3987.95 Mobile/15E148 Safari/604.1";

}