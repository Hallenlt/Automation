package infrastructure.selenium;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import infrastructure.thread.CentralBaseProcessor;

public class HttpResponseCore extends CentralBaseProcessor {

	public HttpResponseCore(WebDriver driver) {
		this.driver = driver;
		// TODO Auto-generated constructor stub
	}

	public void detectHttpResponseAllLinkURLs(String url) {

		Reporter.log("[INFO] - Executed | Checking HTTP Status Response Of Link URL |", true);

		String path = "";
		HttpURLConnection httpConnect = null;
		int response = 200;

		List<WebElement> links = driver().findElements(By.tagName("a"));
		Iterator<WebElement> iterator = links.iterator();
		APP_LOGS("Total Links In Present: " + links.size());

		while (iterator.hasNext()) {
			path = iterator.next().getAttribute("href");

			try {
//				if (path.contains("tel")) {
//					APP_LOGS("URL Is Either Not Configured For Anchor Tag:: " + path);
//					continue;
//				}
				if (path == null || path.isEmpty()) {
					APP_LOGS("URL Is Either Not Configured For Anchor Tag Or It Is Empty:: " + path);
					continue;
				}
				if (!path.startsWith(url)) {
					APP_LOGS("URL Belongs To Another Domain, Skipping It:: " + path);
					continue;
				}
				httpConnect = (HttpURLConnection) (new URL(path).openConnection());
				httpConnect.setRequestMethod("HEAD");
				httpConnect.connect();
				response = httpConnect.getResponseCode();

				if (response >= 400) {
					APP_LOGS_FAIL(path + "\n :: Status: " + response + " ERROR");
					continue;
				} else {
					APP_LOGS(path + "\n :: Status: " + response + " OK");
					continue;
				}
			} catch (IOException e) {
				// TODO: handle exception
				APP_LOGS_WARNING(e.getMessage());
			}
		}
	}

}