package infrastructure.selenium;

import static org.testng.Assert.fail;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Reporter;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.thread.CentralBaseProcessor;

public class ClassifierClientCore extends CentralBaseProcessor {

	public ClassifierClientCore(RemoteWebDriver remoteDriver) {
		this.remoteDriver = remoteDriver();
		// TODO Auto-generated constructor stub
	}

	public void click(String label) {

		Reporter.log("\n [INFO] - Executed | Matching Label: " + label, true);
		try {
//			List<WebElement> els = classifier.findElementsMatchingLabel(remoteDriver, label);
//			APP_LOGS("Matching Label: " + label + " --> " + els.get(0).toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			APP_LOGS_FAIL(e.getMessage());
			fail(null, e);
		}
	}

}