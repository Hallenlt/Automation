package infrastructure.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class SwappableCore {

	static final int OFFSET = 5;

	@FindBy(css = ".ui-sortable-handle")
	List<WebElement> swappableHandles;

	Map<String, WebElement> map;
	Actions action;

	public SwappableCore(final WebDriver driver) {
		this.action = new Actions(driver);
		PageFactory.initElements(driver, this);
		// TODO Auto-generated constructor stub
	}

	public List<String> getItems() {
		return this.swappableHandles.stream().map(WebElement::getText).map(String::trim).collect(Collectors.toList());
	}

	public void reorder(int from, int to) {
		this.reorder(swappableHandles.get(from), swappableHandles.get(to));
	}

	public void reorder(String from, String to) {

		if (Objects.isNull(map)) {
			map = swappableHandles.stream().collect(Collectors.toMap(ele -> ele.getText(), // key
					ele -> ele // value
			));
		}
		this.reorder(map.get(from), map.get(to));
	}

	private void reorder(WebElement source, WebElement target) {
		this.action.clickAndHold(source).dragAndDropBy(target, OFFSET, OFFSET).build().perform();
	}

}