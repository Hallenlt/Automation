package infrastructure.selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import infrastructure.thread.CentralBaseProcessor;

public class DatetimePickerCore extends CentralBaseProcessor {

	public DatetimePickerCore(WebDriver driver) {
		this.driver = driver;
	}

	public DatetimePickerCore selectYear(String yearfield, String parent_locator, String child_locator) {

		selenium.waitForPresenceOfElement(yearfield);
		try {
			WebElement yearField = driver().findElement(By.xpath(yearfield));
			yearField.click();
			WebElement parent = driver().findElement(By.xpath(parent_locator));
			WebElement children = parent.findElement(By.xpath(child_locator));
			children.click();

		} catch (WebDriverException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return this;
	}

	public DatetimePickerCore selectMonth(String monthfield, String parent_locator, String child_locator) {

		selenium.waitForPresenceOfElement(monthfield);
		try {
			WebElement monthField = driver().findElement(By.xpath(monthfield));
			monthField.click();
			WebElement parent = driver().findElement(By.xpath(parent_locator));
			WebElement children = parent.findElement(By.xpath(child_locator));
			children.click();

		} catch (WebDriverException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return this;
	}

	public DatetimePickerCore selectDay(String parent_locator, String child_date) {

		selenium.waitForPresenceOfElement(parent_locator);
		try {
			WebElement cal = driver().findElement(By.xpath(parent_locator));
			List<WebElement> childNode;
			childNode = cal.findElements(By.tagName(child_date));

			for (int i = 1; i < childNode.size(); i++) {
				childNode.get(i).click();
			}

		} catch (WebDriverException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return this;
	}

	public DatetimePickerCore selectDayInTable(String locator) {

		selenium.waitForPresenceOfElement(locator);
		try {
			WebElement cal = driver().findElement(By.xpath(locator));
			List<WebElement> rows, cols;
			rows = cal.findElements(By.tagName("tr"));

			for (int i = 1; i < rows.size(); i++) {
				cols = rows.get(i).findElements(By.tagName("td"));

				for (int j = 0; j < cols.size(); j++) {
					cols.get(j).click();
				}
			}

		} catch (WebDriverException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return this;
	}

}
