package infrastructure.selenium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import infrastructure.root.CommonLibrariesCore;
import infrastructure.thread.CentralBaseProcessor;

public class JSWaitCore extends CentralBaseProcessor {

	public JSWaitCore(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public void waitAllRequest() {

		ajaxComplete();
		waitUntilJSReady();
		waitUntilJQueryReady();
		waitAjaxControls();
		waitUntilAngularReady();
		waitUntilAngular5Ready();
	}

	private void waitAjaxControls() {

//		try {
//			wait = new WebDriverWait(driver, WAIT_TIMEOUT_IN_SECONDS);
//			wait.until(new Function<WebDriver, WebElement>() {
//
//				public WebElement apply(WebDriver driver) {
//
//					JavascriptExecutor jse = (JavascriptExecutor) driver;
//					return (WebElement) jse.executeScript("(window.jQuery != null) && $('.spinner').is(':visible') == false");
//				}
//			});
//		} catch (WebDriverException e) {
//			e.printStackTrace();
//		}
	}

	private void ajaxComplete() {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("var callback = arguments[arguments.length - 1];" + "var xhr = new XMLHttpRequest();" + "xhr.open('GET', '/Ajax_call', true);"
				+ "xhr.onreadystatechange = function() {" + "  if (xhr.readyState == 4) {" + "    callback(xhr.responseText);" + "  }" + "};" + "xhr.send();");
	}

	private void waitForJQueryLoad() {

		try {
			ExpectedCondition<Boolean> jQueryLoad = driver -> ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			boolean jqueryReady = (Boolean) jse.executeScript("return jQuery.active==0");

			wait = new WebDriverWait(driver, WAIT_TIMEOUT_IN_SECONDS);
			if (!jqueryReady) {
				wait.until(jQueryLoad);
			}
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
	}

	private void waitUntilJSReady() {

		try {
			ExpectedCondition<Boolean> jsLoad = driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
					.equals("complete");

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			boolean jsReady = jse.executeScript("return document.readyState").toString().equals("complete");

			wait = new WebDriverWait(driver, WAIT_TIMEOUT_IN_SECONDS);
			if (!jsReady) {
				wait.until(jsLoad);
			}
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
	}

	private void waitUntilJQueryReady() {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		Boolean jQueryDefined = (Boolean) jse.executeScript("return typeof jQuery != 'undefined'");

		if (jQueryDefined) {
			poll(20);
			waitForJQueryLoad();
			poll(20);
		}
	}

	private void waitForAngularLoad() {

		String angularReadyScript = "return angular.element(document).injector().get('$http').pendingRequests.length === 0";
		angularLoads(angularReadyScript);
	}

	private void waitUntilAngularReady() {

		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			Boolean angularUnDefined = (Boolean) jse.executeScript("return window.angular === undefined");

			if (!angularUnDefined) {
				Boolean angularInjectorUnDefined = (Boolean) jse.executeScript("return angular.element(document).injector() === undefined");

				if (!angularInjectorUnDefined) {
					poll(20);
					waitForAngularLoad();
					poll(20);
				}
			}
		} catch (WebDriverException ignored) {

		}
	}

	private void waitUntilAngular5Ready() {

		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			Object angular5Check = jse.executeScript("return getAllAngularRootElements()[0].attributes['ng-version']");

			if (angular5Check != null) {
				Boolean angularPageLoaded = (Boolean) jse.executeScript("return window.getAllAngularTestabilities().findIndex(x=>!x.isStable()) === -1");

				if (!angularPageLoaded) {
					poll(20);
					waitForAngular5Load();
					poll(20);
				}
			}
		} catch (WebDriverException ignored) {

		}
	}

	private void waitForAngular5Load() {

		String angularReadyScript = "return window.getAllAngularTestabilities().findIndex(x=>!x.isStable()) === -1";
		angularLoads(angularReadyScript);
	}

	private void angularLoads(String angularReadyScript) {

		try {
			ExpectedCondition<Boolean> angularLoad = driver -> Boolean.valueOf(((JavascriptExecutor) driver).executeScript(angularReadyScript).toString());

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			boolean angularReady = Boolean.valueOf(jse.executeScript(angularReadyScript).toString());

			wait = new WebDriverWait(driver, WAIT_TIMEOUT_IN_SECONDS);
			if (!angularReady) {
				wait.until(angularLoad);
			}
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
	}

	private void poll(long milis) {

		try {
			Thread.sleep(milis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
