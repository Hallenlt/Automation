package infrastructure.selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import infrastructure.thread.CentralBaseProcessor;

public class DynamicSortingCore extends CentralBaseProcessor {

	public DynamicSortingCore(WebDriver driver) {
		this.driver = driver;
		// TODO Auto-generated constructor stub
	}

	public void ArrangeDynamicTable(String sortbutton, String column) {

		Reporter.log("[INFO] - Executed | Sorting Dynamic Table By Button: " + sortbutton + " --> At Column: " + column + " |", true);
		selenium = new SeleniumBaseCore(driver);

		String temp = null;
		selenium.click(sortbutton);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		APP_LOGS("Printing the values before sorting in the Array");
		List<WebElement> tdList = driver().findElements(By.xpath(column));
		String strArray[] = null;
		strArray = new String[tdList.size()];

		JavascriptExecutor jse = (JavascriptExecutor) driver();

		for (int i = 0; i < tdList.size(); i++) {

			jse.executeScript("arguments[0].scrollIntoView();", tdList.get(i));
			strArray[i] = tdList.get(i).getText();
		}

		for (int i = 0; i < strArray.length; i++) {

			for (int j = i + 1; j < strArray.length; j++) {

				if (strArray[i].compareTo(strArray[j]) > 0) {
					temp = strArray[i];
					strArray[i] = strArray[j];
					strArray[j] = temp;
				}
			}
		}
		APP_LOGS("Printing the values after sorting in the Array");

		for (int i = 0; i < strArray.length; i++) {

		}
		selenium.click(sortbutton);

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tdList = driver().findElements(By.xpath(column));
		APP_LOGS("After sorting printing the values from the List Data Table");

		for (int i = 0; i < strArray.length; i++) {

			jse.executeScript("arguments[0].scrollIntoView();", tdList.get(i));
			tdList.get(i).getText();
		}

		for (int i = 0; i < strArray.length; i++) {

			jse.executeScript("arguments[0].scrollIntoView();", tdList.get(i));
			APP_LOGS(strArray[i] + " == " + tdList.get(i).getText());

			if (strArray[i].compareTo(tdList.get(i).getText()) != 0) {
				status = false;
				APP_LOGS_FAIL("Elements in the table are not sorted");
				break;
			}
		}
		APP_LOGS("Elements in the dynamic table are sorted: " + status);
	}

}