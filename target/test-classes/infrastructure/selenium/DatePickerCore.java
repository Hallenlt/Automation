package infrastructure.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.LongStream;

public class DatePickerCore {

	static final String DATE_FORMAT = "dd MMMM yyyy";
	static final String DAY_FIRST = "01";
	static final String SPACE = " ";
	static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern(DATE_FORMAT);

	@FindBy(css = "button.react-datepicker__navigation react-datepicker__navigation--previous")
	WebElement prev;

	@FindBy(css = "react-datepicker__navigation react-datepicker__navigation--next")
	WebElement next;

	@FindBy(css = "div.react-datepicker__header__dropdown react-datepicker__header__dropdown--scroll")
	WebElement currentDate;

	@FindBy(css = "div.sc-AxgMl cVmQYF custom-day")
	List<WebElement> datesPoint;

	public DatePickerCore(final WebDriver driver) {
		PageFactory.initElements(driver, this);
		// TODO Auto-generated constructor stub
	}

	public void setDate(String date) {

		long diff = this.getDateDifferenceInMonths(date);
		int day = this.getDay(date);
		WebElement arrow = (diff >= 0 ? next : prev);
		diff = Math.abs(diff);
		// click the arrows
		LongStream.range(0, diff).forEach(i -> arrow.click());
		// select the date
		datesPoint.stream().filter(element -> Integer.parseInt(element.getText()) == day).findFirst().ifPresent(element -> element.click());
	}

	private int getDay(String date) {
		LocalDate dpToDate = LocalDate.parse(date, DTF);
		return dpToDate.getDayOfMonth();
	}

	private long getDateDifferenceInMonths(String date) {
		LocalDate dpCurDate = LocalDate.parse(DAY_FIRST + SPACE + this.getCurrentMonthFromDatePicker(), DTF);
		LocalDate dpToDate = LocalDate.parse(date, DTF);
		return YearMonth.from(dpCurDate).until(dpToDate, ChronoUnit.MONTHS);
	}

	private String getCurrentMonthFromDatePicker() {
		return this.currentDate.getText();
	}

}