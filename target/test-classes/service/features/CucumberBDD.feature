Feature: Reset functionality on login page of Application
  Description: The purpose of these tests are to cover E2E happy flows for customer.

  Background: User generates token for Authorisation
    Given I am an authorized user

  @tag1
  Scenario: Verification of Reset button
    Given Open the Firefox and launch the application
    When Enter the Username and Password
    Then Reset the credential
