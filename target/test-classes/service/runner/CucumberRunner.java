package service.runner;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.intuit.karate.Results;
import com.intuit.karate.Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
//@CucumberOptions(features = "./src/main/java/service/features/CucumberBDD.feature", glue = "definition.CucumberSteps")
@CucumberOptions(plugin = { "pretty", "html:target/cucumber-html-report", "json:target/cucumber-json-report/cucumber-json.json",
		"junit:target/cucumber-junit-report", }, features = "classpath:service/features/CucumberBDD.feature", glue = "definition.CucumberSteps")
//monochrome = true, strict = true

public class CucumberRunner {

	@Test
	public void testParallel() {
		Results results = Runner.parallel(getClass(), 10, "target/surefire-reports");
		assertTrue(results.getErrorMessages(), results.getFailCount() == 0);
	}

}